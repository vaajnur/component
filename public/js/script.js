$(".order_type").on("click", function () {
  if ($(this).find("option:selected").val() == "contract")
    $(".pcb_count").removeClass("d-none");
  else $(".pcb_count").addClass("d-none");
});

/* ************************ активный таб после перезагрузки стр. */
$(".nav.custom-tabs.nav-tabs.d-flex")
  .eq(0)
  .find("li")
  .on("click", function () {
    var id1 = $(this).find(".nav-link").attr("href");
    localStorage.setItem("active-tab", id1);
  });

var activ_tab = localStorage.getItem("active-tab");
if (activ_tab !== null) {
  $(".order-tabs-content .active.show").removeClass("active show");
  $(".order-tabs .active").removeClass("active");
  $(activ_tab).addClass("active show");
  $(".first-tab-content").addClass("active show");
  $('[href="' + activ_tab + '"]').addClass("active");
}

$(".close-modal").on("click", function () {
  $(".modal").modal("hide");
});

/* *************************  добавить ответ поставщика для компонента  */
$(".add-provider-btn1").on("click", function () {
  var data1 = $(this).attr("data-component");
  var order1 = $(this).attr("data-order");
  data1 = JSON.parse(data1);
  order1 = JSON.parse(order1);
  if (data1 != false) {
    $("#add-provider-modal tbody").html("");
    $.get("/provider_offers/" + data1.id).done(function (data) {
      // $('#chose-provider-modal tbody').html(data)
      var trs = $(data);
      trs.find(".checkbox.checkbox-single input").attr("disabled", true);
      $("#add-provider-modal tbody").prepend(trs);
      tableditForControlPage();
    });
    $("#add-provider-modal .modal-title").text(data1.name);
    $("#add-provider-modal h5").text(data1.pn);
    $("#add-provider-modal .component_id").attr("data-component-id", data1.id);
    $("#add-provider-modal .quantity").text(data1.qty*order1.pcb_count);
    $("#add-provider-modal .scored").text(data1.scored);
  }
});

/* *************************** выбор предложений поставщиков */
$(".chose-provider-btn1").on("click", function () {
  var data1 = $(this).attr("data-component");
  data1 = JSON.parse(data1);
  if (data1 != false) {
    $.get("/provider_offers/" + data1.id).done(function (data) {
      $("#chose-provider-modal tbody").html(data);
    });
    $("#chose-provider-modal h4.modal-title").text(data1.name);
    $("#chose-provider-modal h5.modal-title").text(data1.pn);
    $("#chose-provider-modal .quantity").text(data1.quantity);
    $("#chose-provider-modal .scored").text(data1.scored);
  }
});

/* ****************************  выбор предложений   */
$("#chose-provider-modal .btn.btn-choose-offers").on("click", function () {
  $("#chose-provider-modal tr").each(function (idx, val) {
    var id = $(val).find('input[type="checkbox"]').attr("data-id");
    var res1 = $(val).find('input[type="checkbox"]').is(":checked")
      ? $.get("/provider_offers/choose/" + id + "/1")
      : $.get("/provider_offers/choose/" + id + "/0");
  });
  $("#chose-provider-modal").modal("hide");
  setTimeout(function () {
    location.reload();
  }, 2000);
});

/* ***************************  */
$(".create-invoice").on("click", function () {
  // $('.margin_form').submit()
    that = $(this);
    $.post(location.href, { create_invoice: '1'}).done(function () {
      that.attr("disabled", true);
    });

});
// предоплата
$(".send-prepayment").on("click", function () {
  $(".prepayment_form").submit();
});
// счета поставщиков
$(".submit_invoicein").on("click", function () {
  // если пустой № счета
  if ($(this).closest("tr").find('[name="invoicein"]').val() != false) {
    var id = $(this).attr("data-offer-id"),
      that = $(this);
    $.post(location.href, { submit_invoicein: id }).done(function () {
      that.attr("disabled", true);
    });
  } else {
    $(this).closest("tr").find(".tabledit-edit-button").trigger("click");
    $(this)
      .closest("tr")
      .find('[name="invoicein"]')
      .css("border", "1px solid red");
  }
});
//11 добавить приход товара
$(".add_goods_arrival").on("click", function () {
  var name = $(this).attr("data-component"),
    offer = $(this).attr("data-offer");
  offer = JSON.parse(offer);
  if (offer != false) {
    $("#add-coming-modal .add-coming-modal-component").text(name);
    $("#add-coming-modal .offer-id").val(offer.id);
    $("#add-coming-modal .input-date").val(offer.supply_date);
    $("#add-coming-modal .supply_number").val(offer.supply_number);
    $("#add-coming-modal .storage").val(offer.storage);
    if (offer.supply_document) {
      $("#add-coming-modal .supply_document_added").removeClass("d-none");
      $("#add-coming-modal .filter_input").removeAttr("required");
      $("#add-coming-modal .supply_document_added .doc-name a").text(
        offer.supply_document
      );
      $("#add-coming-modal .supply_document_added .doc-name a").attr(
        "href",
        offer.supply_document_path
      );
      // del
      $("#add-coming-modal .del-supply-doc").on("click", function () {
        $.post("/docs", {
          offer_id: offer.id,
          doc_id: offer.supply_document_id,
          "delete-doc": 1,
          type: "supply_document",
        })
          .done(function (data) {
            console.log(data);
          })
          .fail(function (err) {
            console.log(err);
          });
        $(this).parent().remove();
        $("#add-coming-modal .filter_input").attr("required", true);
      });
    } else $("#add-coming-modal .supply_document_added").hide();
    $("#add-coming-modal .filter_input").attr(
      "name",
      "supply_document[" + offer.id + "]"
    );
  }
});

// 11 edit  приход товара
$(".edit-coming-btn").on("click", function () {
  var name = $(this).attr("data-component"),
    offer = $(this).attr("data-offer");
  offer = JSON.parse(offer);
  if (offer != false) {
    $("#edit-coming .coming-offer").text(name);
    $("#edit-coming .offer-id").val(offer.id);
    $("#edit-coming .supply_number").val(offer.supply_number);
    $("#edit-coming .supply_date").val(offer.supply_date);
    $("#edit-coming .store").val(offer.storage);

    // --------------- PROOF
    if (offer.proof_foto) {
      $("#edit-coming .proof_foto").removeClass("d-none");
      $("#edit-coming .proof_foto .doc-name a").text(offer.proof_foto);
      $("#edit-coming .proof_foto .doc-name a").attr(
        "href",
        offer.proof_foto_path
      );
      // del
      $("#edit-coming .del-proof-foto").on("click", function () {
        $.post("/docs", {
          offer_id: offer.id,
          doc_id: offer.proof_foto_id,
          "delete-doc": 1,
          type: "proof_foto",
        })
          .done(function (data) {
            console.log(data);
          })
          .fail(function (err) {
            console.log(err);
          });
        $(this).prev().remove();
      });
    } else {
      $("#edit-coming .proof_foto").addClass("d-none");
    }
    // ---------------
    if (offer.supply_document) {
      $("#edit-coming .supply_doc").show();
      $("#edit-coming .supply_doc").text(offer.supply_document);
      $("#edit-coming .supply_doc").attr("href", offer.supply_document_path);
      // del
      $("#edit-coming .supply_doc_del").on("click", function () {
        $.post("/docs", {
          offer_id: offer.id,
          doc_id: offer.supply_document_id,
          "delete-doc": 1,
          type: "supply_document",
        })
          .done(function (data) {
            console.log(data);
          })
          .fail(function (err) {
            console.log(err);
          });
        $(this).prev().remove();
      });
    } else $("#edit-coming .supply_doc, #edit-coming .supply_doc_del").hide();
    $("#edit-coming .filter_input_proof").attr(
      "name",
      "proof_foto[" + offer.id + "]"
    );
  }
});

// 12
$(".send_to_qc").on("click", function () {
  var id = $(this).attr("data-id"),
    that = $(this);
  $.post(location.href, { send_to_qc: id }).done(function () {
    that.attr("disabled", true);
  });
});

// 13 QC
$(".qc-modal-btn").on("click", function () {
  var name = $(this).attr("data-component"),
    offer = $(this).attr("data-offer");
  offer = JSON.parse(offer);
  if (offer != false) {
    $("#add-comment-modal .component-name").val(name);
    $("#add-comment-modal .offer-id").val(offer.id);
    $("#add-comment-modal .supply_number").val(offer.supply_number);
    $("#add-comment-modal .quantity").val(offer.quantity);
    $('#add-comment-modal [name="not_brak_files[]"]').attr(
      "name",
      "not_brak_files[" + offer.id + "]"
    );
    $('#add-comment-modal [name="brak_files[]"]').attr(
      "name",
      "brak_files[" + offer.id + "]"
    );
    // docs
    $("#add-comment-modal  .not_brak_doc").text(offer.not_brak_name);
    $("#add-comment-modal  .not_brak_doc").attr("href", offer.not_brak_path);
    // docs
    $("#add-comment-modal  .brak_doc").text(offer.brak_name);
    $("#add-comment-modal  .brak_doc").attr("href", offer.brak_path);

    $(".not_brak_doc_del").on("click", function () {
      $.post("/docs", {
        offer_id: offer.id,
        doc_id: offer.not_brak_id,
        "delete-doc": 1,
        type: "not_brak_doc",
      })
        .done(function (data) {
          console.log(data);
        })
        .fail(function (err) {
          console.log(err);
        });
      $(this).prev().remove();
    });
    $(".brak_doc_del").on("click", function () {
      $.post("/docs", {
        offer_id: offer.id,
        doc_id: offer.brak_id,
        "delete-doc": 1,
        type: "brak_doc",
      })
        .done(function (data) {
          console.log(data);
        })
        .fail(function (err) {
          console.log(err);
        });
      $(this).prev().remove();
    });

    // $('#add-comment-modal .store').val(offer.storage)
  }
});

// 13
$(".send-to-control").on("click", function () {
  var id = $(this).attr("data-id"),
    that = $(this);
  $.post(location.href, { "send-to-control": id }).done(function () {
    that.attr("disabled", true);
  });
});

// 14
/* *************************** перевыбор предложений поставщиков (для брака) */
$(".change-provider-modal").on("click", function () {
  var data1 = $(this).attr("data-component");
  data1 = JSON.parse(data1);
  if (data1 != false) {
    $.get("/provider_offers/" + data1.id).done(function (data) {
      $("#change-provider-modal tbody").html(data);
    });
    $("#change-provider-modal h4.modal-title").text(data1.name);
    $("#change-provider-modal h5.modal-title").text(data1.pn);
  }
});

/* 14 ****************************  выбор предложений   */
$("#change-provider-modal .btn.btn-success").on("click", function () {
  $("#change-provider-modal tr").each(function (idx, val) {
    var id = $(val).find('input[type="checkbox"]').attr("data-id");
    var res1 = $(val).find('input[type="checkbox"]').is(":checked")
      ? $.get("/provider_offers/choose/" + id + "/1")
      : $.get("/provider_offers/choose/" + id + "/0");
  });
  // $('#change-provider-modal').modal('hide')
});

$(".arrival-info").on("click", function () {
  var component = $(this).attr("data-component"),
    data1 = $(this).attr("data-offer"),
    not_brak = $(this).hasClass("not_brak");
  data1 = JSON.parse(data1);
  if (data1 != false) {
    $("#info-modal h4.modal-title").text(data1.supply_number);
    $("#info-modal .component-name").text(component);
    $("#info-modal .offer-doc").attr("href", data1.supply_document);
    $("#info-modal .offer-comment").text(
      not_brak ? data1.not_brak_comment : data1.brak_comment
    );
  }
});
// 14.7
$(".get-balance-tail").on("click", function () {
  $.post(location.href, { "get-balance-tail": 1 }).done(function () {
    // that.attr('disabled', true)
  });
});
// 15
$(".demand").on("click", function () {
  $.post(location.href, { demand: 1 }).done(function () {
    // that.attr('disabled', true)
  });
});

// choose store
$(".choose-store option").on("click", function () {
  window.location = "/storage/" + $(this).val();
});

// edit provider
$(".edit-provider-btn").on("click", function () {
  var provider = $(this).attr("data-provider");
  provider = JSON.parse(provider);
  if (provider != false) {
    $(".modal-edit-provider .prov-name").val(provider.name);
    $(".modal-edit-provider .prov-spec").val(provider.spec_id);
    $(".modal-edit-provider .provider-id").val(provider.id);
  }
});

// check  permissions
$('.permissions-tbl input[type="checkbox"]').on("click", function () {
  var val1 = $(this).prop("checked") ? "1" : "0",
    name = $(this).attr("name"),
    id = $(this).closest("tr").attr("id");
  // console.log(val1)
  // console.log(name)
  if (val1 !== "" && name != false) {
    $.post("", {
      "edit-roles": 1,
      ajax: 1,
      id: id,
      action: name,
      action_val: val1,
    }).done(function (res) {
      // console.log(res)
    });
  }
});

// edit user
$(".edit-user-btn").on("click", function () {
  var user = $(this).attr("data-user");
  user = JSON.parse(user);
  if (user != false) {
    $(".modal-edit-user .name").val(user.fio);
    $(".modal-edit-user .role").val(user.role_id);
    $(".modal-edit-user .login").val(user.login);
    $(".modal-edit-user .password").val("");
    $(".modal-edit-user .user-id").val(user.id);
  }
});

// del user
$(".delete-user-btn-confirm").on("click", function () {
  var id = $(this).closest("tr").attr("id");
  $.post("", { id: id, "del-user": 1 });
});

// edit employee
$(".edit-employee").on("click", function () {
  var employee = $(this).attr("data-employee");
  employee = JSON.parse(employee);
  if (employee != false) {
    $("#edit-contact-modal .empl-post").val(employee.post);
    $("#edit-contact-modal .empl-id").val(employee.id);
    $.get("/employee_custom_fields/" + employee.id)
      .done(function (data1) {
        $("#edit-contact-modal .fields-current").html(data1);
      })
      .fail(function (err) {
        console.log(err);
      });
  }
});

// del inv doc
$(".del-invoicein").on("click", function () {
  var offer_id = $(this).attr("data-offer"),
    doc_id = $(this).attr("data-doc");
  $.post("/docs", {
    offer_id: offer_id,
    doc_id: doc_id,
    "delete-doc": 1,
    type: "invoicein_doc",
  })
    .done(function (data) {
      console.log(data);
    })
    .fail(function (err) {
      console.log(err);
    });
  $(this).parent().remove();
});

// del cf
$(document).on("click", ".delete-cfield-btn", function () {
  $.post("", { id: $(this).attr("data-cf-id"), "delete-cf": 1 });
});

// delete doc
$(document).on("click", ".del-doc, .jFiler-item-trash-action", function () {
  var offer_id = $(this).attr("data-offer"),
    document_id = $(this).attr("data-doc"),
    type = $(this).attr("data-type");
  $.post("/docs", {
    offer_id: offer_id,
    doc_id: document_id,
    "delete-doc": 1,
    type: type,
  })
    .done(function (data) {
      console.log(data);
    })
    .fail(function (err) {
      console.log(err);
    });
  $(this).parent().remove();
});

// check empty doc
$("#add-coming-modal form").on("submit", function (ev) {
  if (
    $(".supply_document_added").hasClass("d-none") &&
    $(".jFiler-items").length == 0
  ) {
    ev.preventDefault();
    $(".jFiler-input").css("border", "1px solid red");
  }
});
// remove border from inp btn
$("#add-coming-modal").on("hidden.bs.modal", function () {
  $(".jFiler-input").css("border", "none");
});

// confirm
$("#confirm-delete").on("show.bs.modal", function (e) {
  console.log($(e.relatedTarget).attr("href"));
  $(".btn-ok", this).attr("href", $(e.relatedTarget).attr("href"));
});

$(".apply_margin").on("click", function () {
  margin_arr = {};
  $(".margin-table tr").each(function (idx, val) {
    if ($(val).find(".offer-margin").length)
      margin_arr[$(val).find(".offer-margin").attr("data-id")] = $(val)
        .find(".offer-margin")
        .val();
  });
  // console.log(margin_arr)
  if (Object.values(margin_arr).length > 0) {
    $.post("", { margin_arr: margin_arr }).done(function (data) {
      console.log(data);
      location.reload();
    });
  }
});

/* ****************** edit category */

// $('.edit-category-btn1').on('click', function(){
/*$('[data-target="#categor_edit"]').on('click', function(){
  // var id = $(this).attr('data-id')
  var id = 14
  if(id != false){
      $.get('/edit-category/'+id).done(function(data){
        console.log(data)
          $('#categor_edit .modal-body').html(data)
      })
  }
})
*/

// поиск в деталке категории (по компонентам)
// $('.category-comp_name #searchInput').on('keyup', function(){
//     var searched = $(this).val()
//     // console.log(searched)
//     $("#category-components-list tbody tr").each(function(idx, val){
//         // console.log($(val).find('td').eq(1).find('.tabledit-span').text().indexOf(searched))
//         if($(val).find('td').eq(1).find('.tabledit-span').text().toLowerCase().indexOf(searched) === -1){
//           console.log($(val))
//           $(val).attr('style','display:none !important');
//         }else{
//           $(val).removeAttr('style')
//         }
//     })

// })
/*$(".category-comp_name #searchInput").on("keyup", function () {
  var searched = $(this).val();
  var ammount = 0;
  $("#category-components-list tbody tr").each(function (idx, val) {
    if (
      $(val)
        .find("td")
        .text()
        .toLowerCase()
        .indexOf(searched) === -1
    && $(val)
      .find("th")
      .text()
      .toLowerCase()
      .indexOf(searched) === -1) {
      // console.log($(val));
      $(val).attr("style", "display:none !important");
    } else {
      $(val).removeAttr("style");
      ammount = ammount + 1;
    }
  });
  if (searched !== "") {
    if (ammount > 0) {
      $("p.value-text_search").text(
        `По вашему запросу "${searched}" найдено ${ammount} позиций:`
      );
    } else {
      $("p.value-text_search").text(
        `По вашему запросу "${searched}" ничего не найдено`
      );
    }
  } else {
    $("p.value-text_search").text(``);
  }
});*/

// Поиск
if($('#category-components-list_filter input[type="search"]').length){
    $('#category-components-list_filter input[type="search"]').attr('class', 'form-control inp-search_categ btn-search-cat').attr('placeholder', 'Search').after('<span class="fe-search"></span>')
}


/*$(".categor-comp_name-modal #searchInput").on("keyup", function () {
  var searched = $(this).val();
  // console.log(searched)
  $(".table-componet__categ-list tbody tr").each(function (idx, val) {
    // console.log($(val).find('td').eq(1).find('.tabledit-span').text().indexOf(searched))
    if (
      $(val)
        .find("td")
        .eq(0)
        .find(".choose-component-enginer")
        .text()
        .toLowerCase()
        .indexOf(searched) === -1
    ) {
      // console.log($(val));
      $(val).attr("style", "display:none !important");
    } else {
      $(val).removeAttr("style");
    }
  });
});*/

// категории в модалке выбора компа
$(".choose-modal-category").on("click", function (ev) {
  ev.preventDefault()
  let textChoose = $(this).text();
  $(".heading-engener-change").find("h3").text(textChoose);
  $('#choose-component .btn-add__comp').attr('data-category', $(this).attr("data-id"))
  $.get("/category-ajax/" + $(this).attr("data-id"), function (resp) {
    $(".table-category-content__el").html(resp);
    // reinit datatables
    categor_components_list_nameDT()

  }).fail(function(err) { console.log(err); });
});

// добавить компонент в модалке модалки
$('.btn-add__comp_in_order').on('click', function(){
    $('#categor-add_component form').attr('action', '/category/'+$(this).attr('data-category'))
    $('#categor-add_component form').append('<input type="hidden" name="ajax" value="1">')
})

// 
$('.order-tabs-content [name="add_component"]').on('click', function(ev){
  ev.preventDefault()
  ev.stopPropagation()
  var form = $(this).closest('form')
  $.post(form.attr('action'), form.serialize()+'&add_component=1').done(function(data){
    // console.log(data)
    $('#categor-add_component').modal('hide')
    $.get("/category-ajax/" + form.attr('action').replace( /^\D+/g, ''), function (resp) {
      $(".table-category-content__el").html(resp);
    });
  })
})


// Кнопка очищение инпута в modal add category

$(document).on('click', '.add_compon-del-span', function() {
  $(this).each(function() {
     $(this).closest('.row').parents('.row').find('.form-custom_name').val("");
 // console.log('click');
});
});

// Кнопка очищение инпута в modal edit category
    $(document).on('click', '.edit-del', function() {
      $(this).each(function() {
         $(this).closest('.row').parents('.row').find('.edit_categor-modal-view').val("");
         $(this).closest('.row').parents('.row').find('.edit_categor-modal-view').attr('value', '');
     // console.log('click');
   });
});

// add input
// window.onload = function() {
var x = 0;

function addInput() {
  if (x < 10) {
    var str = '<div class="col-md-6 first-bl_custom-name"><div class="row justify-content-between"><div class="col-9"><label>Property Name:</label></div><div class="col"><span class="add_compon-del-span inp-' + (x + 5) +'">Delete</span></div></div><input id="inp-' + (x + 5) + '" type="text" name="custom_f_name[]" class="empl-post form-control form-custom_name" placeholder="Property Name" value=""></div><div class="col-md-6 ml-auto"><label>Unit of measurement:</label><input type="text" name="custom_f_value[]" class="empl-post form-control" placeholder="Unit of measurement" value=""></div></div>';

    document.getElementById("input" + x).innerHTML = str;

    x++;
  } else {
  }
}
// addInput();
//   }
if($('.last-step').length){
  $('.last-step').filter((index) => {return index != $('.last-step').length-1}).removeClass('last-step')
}

// popover formation oder

$(document).popover({
  // trigger: 'click',
  // hideOnHTMLClick: true,
  placement: "bottom",
  html: true,
  selector: ".pcb-position_text",
  content: function () {
    return $(this).parent().find(".pcb-position_text").html();
  },
});

$(document).on("click", function (e) {
  if (
    !$(e.target).closest(".pcb-position_text").length &&
    !$(e.target).closest(".popover").length
  ) {
    $(".pcb-position_text").popover("hide");
  }
});

$(".pcb-position_text").on("show.bs.popover", function () {
  $(".pcb-position_text")
    .not($(this))
    .each(function () {
      $(this).popover("hide");
    });
});

$(".pcb-position_text").click(function () {
  // var packageSelectPCB = $(this).find('input[name="position_on_pcb"]').attr('value');
  var packageSelectPCB = $(this)
    .find(".pcb-position_text .tabledit-span")
    .html();
  $(this).attr("data-content", packageSelectPCB);
});

$(".tabledit-save-button").click(function () {
  $(".pcb-position_text").attr("data-content", "");
});


// + btn
$('.add-row.btn').on('click', function(){
  $('.tabledit-save-button').each(function(k , v){
     if($(v).is(':visible'))
      $(v).trigger('click')
  })
})

$('#sidebar-menu').metisMenu({
  triggerElement: '.for-arrow'
  })

if($('a[href="'+location.pathname+'"]') !== undefined){
  
  $('a[href="'+location.pathname+'"]').parents('li').addClass('mm-active')
  $('a[href="'+location.pathname+'"]').parents('.nav-second-level').addClass('mm-show')
  $('a[href="'+location.pathname+'"]').siblings('.nav-second-level').addClass('mm-show')
}
// select2tree
if($("#select_tree").length){
  var mydata = $("#select_tree").attr('data-json')
  var mydata = JSON.parse(mydata)
  $("#select_tree").select2ToTree({treeData: {dataArr:mydata}, maximumSelectionLength: 30});
}

