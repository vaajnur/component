$(document).ready(function () {

const datepickerOptions = {
  language: 'ru',
  autoclose: true,
  format: 'yyyy.mm.dd',
}

  $('#datepicker_from').datepicker({
    ...datepickerOptions
  }).on('changeDate', function () {
    // $('#datatable-orders-journal').DataTable().draw()
    console.log('checkDate from')
  })

  $('#datepicker_to').datepicker({
    ...datepickerOptions
  }).on('changeDate', function () {
    $('#datatable-orders-journal').DataTable().draw()
    console.log('checkDate to')
  })

  const comingProviderTable = $('#coming-providers-table').DataTable({
    ...datatableOptions,
  })

  const archiveTable = $('#datatable-archive').DataTable({
    ...datatableOptions,
  })

  const bankStatementTable = $('#bank-statement-table').DataTable({
    ...datatableOptions, order: [[ 0, "desc" ]]
  })

  const clientsTable = $('#datatable-clients').DataTable({
    ...datatableOptions,
  })

  const providerTabel = $('#datatable-providers').DataTable({
    ...datatableOptions,
  })

  const storageTabel = $('#datatable-storage').DataTable({
    ...datatableOptions,
  })

  const invoiceJournalTabel = $('#datatable-invoice-journal').DataTable({
    ...datatableOptions,
  })

  const usersTabel = $('#datatable-users').DataTable({
    ...datatableOptions,
  })

  const comingProviderTableCq = $('#comings-providers-table-qc').DataTable({
    ...datatableOptions,
  })

  const expenseJournalTableExpense = $('#table-expense').DataTable({
    ...datatableOptions,
  })

  const expenseJournalTableExpenseDeffect = $('#table-expense-deffect').DataTable({
    ...datatableOptions,
  })

  $("#reset-all").click(function() {
    yadcf.exResetAllFilters(tableOrderJournal);
  });

  $("#reset-all-clients").click(function() {
    yadcf.exResetAllFilters(clientsTable);
  });

  $("#reset-all-providers").click(function() {
    yadcf.exResetAllFilters(providerTabel);
  });

  $("#reset-all-archive").click(function() {
    yadcf.exResetAllFilters(archiveTable);
  });

  $("#reset-all-storage").click(function() {
    yadcf.exResetAllFilters(storageTabel);
  });

  $("#reset-all-coming-provider").click(function() {
    yadcf.exResetAllFilters(comingProviderTable);
  });

  $("#reset-all-table-expense").click(function() {
    yadcf.exResetAllFilters(expenseJournalTableExpense);
  });

  $("#reset-all-table-expense-deffect").click(function() {
    yadcf.exResetAllFilters(expenseJournalTableExpenseDeffect);
  });

  $("#reset-all-table-bank").click(function() {
    yadcf.exResetAllFilters(bankStatementTable);
  });


  if ($('#comings-providers-table-qc').length > 0) {
    yadcf.init(comingProviderTableCq, [
      {
        column_number: [2],
        filter_container_id: 'date-pick',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'dd.mm.yyyy',
        reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'span',
        html_data_selector: 'tabledit-span',
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [3],
        filter_container_id: 'component-filter-select-pick',
        filter_default_label: 'Компонент',
        reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
      }
    ])
  }

  if ($('#datatable-providers').length > 0) {
    yadcf.init(providerTabel, [
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [0],
        filter_container_id: 'client-filter-select',
        filter_default_label: 'Поставщики',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [1],
        filter_container_id: 'specialisation-filter-select',
        filter_default_label: 'Специализация',
        filter_reset_button_text: false
      }
    ])
  }

  if ($('#datatable-archive').length > 0) {
    yadcf.init(archiveTable, [
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [0],
        filter_container_id: 'client-filter-select',
        filter_default_label: 'Клиенты',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [1],
        filter_container_id: 'category-filter-select',
        filter_default_label: 'Категория',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false
      }
    ])
  }

  if ($('#datatable-storage').length > 0) {
    yadcf.init(storageTabel, [
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [1],
        filter_container_id: 'position-filter-select',
        filter_default_label: 'Позиция',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false
      }
    ])
  }

  if ($('#bank-statement-table').length > 0) {
    yadcf.init(bankStatementTable, [
      {
        column_number: [1],
        filter_container_id: 'date-pick',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'dd.mm.yyyy',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false
      }
    ])
  }

  if ($('#datatable-clients').length > 0) {
    yadcf.init(clientsTable, [
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [0],
        filter_container_id: 'client-filter-select',
        filter_default_label: 'Клиенты',
        reset_button_style_class: 'false',
        filter_reset_button_text: false
      }
      // {
      //   filter_type: 'multi_select',
        // select_type: 'select2',
      //   column_number: [3],
      //   filter_container_id: 'client-filter-select',
      //   filter_default_label: 'Клиенты',
      //   reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
      // }
    ])
  }

  if ($('#coming-providers-table').length > 0) {
    yadcf.init(comingProviderTable, [
      {
        column_number: [2],
        filter_container_id: 'date-pick',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'dd.mm.yyyy',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [4],
        filter_container_id: 'component-filter',
        filter_default_label: 'Название компонента',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [6],
        filter_container_id: 'storage-filter',
        filter_default_label: 'склад',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false
      },
    ])
  }

  if ($('#table-expense').length > 0) {
    yadcf.init(expenseJournalTableExpense, [
      {
        column_number: [2],
        filter_container_id: 'date-pick',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'dd.mm.yyyy',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false,
        column_data_type: 'html',
        html_data_type: 'text',
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [4],
        filter_container_id: 'component-filter',
        filter_default_label: 'Название компонента',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false,
        column_data_type: 'html',
        html_data_type: 'text',
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [6],
        filter_container_id: 'storage-filter',
        filter_default_label: 'Storage',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false,
        column_data_type: 'html',
        html_data_type: 'text',
      },
    ])
  }

  if ($('#table-expense-deffect').length > 0) {
    yadcf.init(expenseJournalTableExpenseDeffect, [
      {
        column_number: [2],
        filter_container_id: 'date-pick2',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'dd.mm.yyyy',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false,
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [4],
        filter_container_id: 'component-filter2',
        filter_default_label: 'Название компонента',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false,
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [6],
        filter_container_id: 'storage-filter2',
        filter_default_label: 'Storage',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        column_data_type: 'html',
        html_data_type: 'text',
        filter_reset_button_text: false,
      },
    ])
  }

  const tableOrderJournal = $('#datatable-orders-journal').DataTable({
      ...datatableOptions,
      lengthChange: 0,
      bInfo: false,
    }
  )

  //init table filters (YADCF)
  if ($('#datatable-orders-journal').length > 0) {
    yadcf.init(tableOrderJournal, [
      {
        column_number: [2],
        filter_container_id: 'date-pick',
        filter_type: 'range_date',
        datepicker_type: 'bootstrap-datepicker',
        date_format: 'yyyy.mm.dd',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1 d-none',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [3],
        filter_container_id: 'client-filter-select',
        filter_default_label: 'Client',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false
      },
      {
        filter_type: 'multi_select',
        select_type: 'select2',
        column_number: [4],
        filter_container_id: 'category-filter-select',
        filter_default_label: 'Category',
        // reset_button_style_class: 'btn btn-sm waves-effect btn-secondary ml-1',
        filter_reset_button_text: false
      },
    ])
  }

  //add calendar icon to date_range
  $('.yadcf-filter-range-date-seperator')
    .html('<i class="mdi mdi-calendar-month"></i>')
});