$(document).ready(function () {
  // $('.filter_input-comment ')
  //   .bind('fileuploadcompleted', function (e, data) {
  //        // $('.fileinput-button').hide();
  //        console.log('hide btn')
  //   })
  //   .bind('fileuploaddestroyed', function (e, data) {
  //        // $('.fileinput-button').show();
  //        console.log('show btn')
  //  });

  $(".slide-order").click(function (event) {
    event.preventDefault();
    if (!$(this).hasClass("show")) {
      $(this).text("Collapse request information");
      $(this).siblings(".row").slideToggle();
      $(this).addClass("show");
    } else {
      $(this).siblings(".row").slideToggle();
      $(this).text("Expand application information");
      $(this).removeClass("show");
    }

    event.stopPropagation();
  });

  /*---NAVIGATION-MOBILE-----*/
  $(document).on("click", ".navigation-menu li.has-submenu", function (event) {
    $(this).toggleClass("open");
    $(this).find(".submenu").toggleClass("open");
  });

  $(document).on("click", ".navbar-toggle.nav-link", function () {
    if ($(this).hasClass("open")) {
      $("#navigation").hide();
      $(this).removeClass("open");
    } else {
      $("#navigation").show();
      $(this).addClass("open");
    }
  });

  const datepickerOptions = {
    language: "ru",
    autoclose: true,
    format: "dd.mm.yyyy",
  };

  if (
    $("form input.input-date").length > 0 ||
    $("input.input-date").length > 0
  ) {
    $("input.input-date").datepicker({ ...datepickerOptions });
  }

  /* Добавление строки в модальном окне "Добавить новый контакт"/"Редактировать контакт"/ страница "Страница клиента" */
  function createNewFieldContactModal() {
    return `<div class='row align-items-center form-group form-group-row border-bottom'>
              <div class='col-11'>
                <div class='row'>
                  <div class="form-group col-12 col-md-5">
                    <div>
                      <label>Название поля:</label>
                      <input type="text" name="custom_f_name[]" class="form-control" placeholder='Введите название поля'>
                    </div>
                  </div>
                  <div class="form-group col-10 col-md-5">
                    <div>
                      <label>Значение поля:</label>
                      <input type="text" name="custom_f_value[]" class="form-control" placeholder='Введите значение поля'>
                    </div>
                  </div>
                  <div class="d-flex align-items-center pt-2">
                    <button type="button" class="btn mr-1 ml-1 btn-sm btn-primary active canceled-delete-field-btn" style="display: none; float: none;">
                      <span class="mdi mdi-close"></span>
                    </button>
                    <button class="btn btn-icon btn-sm btn-danger ml-1 delete-field-btn">
                      <i class="fas fa-trash-alt"></i>
                    </button>
                    <button type="button" class="confirm-delete-field-btn btn btn-sm rounded btn-danger" style="display: none; float: none;">
                      <span class="mdi mdi-check"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
                  `;
  }

  function createNewFieldStorageModal(value) {
    return `<div class="form-group form-group-row w-100">
              <label class="col-form-label w-100">${value}:</label>
              <div class="d-flex align-items-center">
                <input name="name" type="text" class="form-control" placeholder="Введите ${value}">
                <button type="button" class="btn mr-1 ml-1 btn-sm btn-primary active canceled-delete-field-btn" style="display: none; float: none;">
                  <span class="mdi mdi-close"></span>
                </button>
                <button class="btn btn-icon btn-sm btn-danger ml-1 delete-field-btn">
                  <i class="fas fa-trash-alt"></i>
                </button>
                <button type="button" class="confirm-delete-field-btn btn btn-sm rounded btn-danger" style="display: none; float: none;">
                  <span class="mdi mdi-check"></span>
                </button>
              </div>
            </div>`;
  }

  $(document).on(
    "click",
    "#add-new-contact-modal #add-new-field_contact-modal",
    function () {
      const field = createNewFieldContactModal();
      $(this).closest("form").find(".fields").append(field);
    }
  );
  $(document).on(
    "click",
    "#edit-contact-modal #edit-field_contact-edit-modal",
    function () {
      const field = createNewFieldContactModal();
      $(this).closest("form").find(".fields").append(field);
    }
  );

  /* Добавление/удаление строки в модальном окне "Добавить склад" страница "Склад" */
  $(document).on(
    "click",
    "#modal-add-new-storage-field #add-new-field_add-storage-modal",
    function () {
      const formFields = $("#modal-add-storage").find(".form-fields");
      const input = $(this).closest(".modal-body").find("input");
      const field = createNewFieldStorageModal(input.val());
      formFields.append(field);
      input.val("");
    }
  );

  $(document).on("click", ".modal .delete-field-btn", function (event) {
    event.preventDefault();
    $(this).hide();
    $(this).closest("div").find("button.canceled-delete-field-btn").show();
    $(this).closest("div").find("button.confirm-delete-field-btn").show();
  });

  $(document).on(
    "click",
    ".modal .canceled-delete-field-btn",
    function (event) {
      event.preventDefault();
      $(this).hide();
      $(this).closest("div").find("button.confirm-delete-field-btn").hide();
      $(this).closest("div").find("button.canceled-delete-field-btn").hide();
      $(this).closest("div").find("button.delete-field-btn").show();
    }
  );

  $(document).on("click", ".modal .confirm-delete-field-btn", function (event) {
    event.preventDefault();
    $(this).closest(".form-group-row").remove();
  });

  /* Добавление строки в модальном окне "Редактировать основную информацию" страница "Страница клиента" */
  $(document).on(
    "click",
    "#add-new-field-modal #add-new-field_edit-info-modal",
    function () {
      const formFields = $("#edit-client-info-modal").find(".form-fields");
      const input = $(this).closest(".modal-body").find("input");
      const value = input.val();
      const prervField = `
                <div class="form-group col-md-6">
                    <div class="d-flex justify-content-between align-items-end">
                      <label>${value}:</label>
                      <button class="btn delete-field-btn">
                        <u class="text-muted fz-11">Удалить поле</u>
                      </button>
                    </div>
                    <input type="text" name="custom_f[${value}]" class="form-control" placeholder="Введите ${value}">
                  </div>`;
      const field = `<div class="form-group col-md-6 form-group-row">
                    <div class='d-flex justify-content-between align-items-end'>
                      <label>${value}</label>
                    </div>
                    <div class='d-flex'>
                      <input type="text"  name="custom_f[${value}]" class="form-control" placeholder='Введите ${value}'>
                      <div class="d-flex align-items-center">
                        <button type="button" class="btn mr-1 ml-1 btn-sm btn-primary active canceled-delete-field-btn"
                                style="display: none; float: none;">
                          <span class="mdi mdi-close"></span>
                        </button>
                        <button class="btn btn-icon btn-sm btn-danger ml-1 delete-field-btn">
                          <i class="fas fa-trash-alt"></i>
                        </button>
                        <button type="button" class="confirm-delete-field-btn btn btn-sm rounded btn-danger"
                                style="display: none; float: none;">
                          <span class="mdi mdi-check"></span>
                        </button>
                      </div>
                    </div>
                  </div>`;

      formFields.append(field);
      input.val("");
    }
  );
  /**/

  // table-rows link / clicked
  $(document).on('click', ".clickable-row", function () {
    window.location = $(this).data("href");
  });

  // get sum modal add invoice
  function getSumComponentPrice() {
    const count = $(this)
      .closest(".row.align-items-center")
      .find(".component-count")
      .val();
    const price = $(this)
      .closest(".row.align-items-center")
      .find(".component-price")
      .val();
    if (count && price) {
      $(this)
        .closest(".row.align-items-center")
        .find(".component-sum")
        .val(count * price);
    } else {
      $(this).closest(".row.align-items-center").find(".component-sum").val("");
    }
    let total = 0;
    $(this)
      .closest(".modal-body")
      .find(".component-sum")
      .each(function () {
        total += +$(this).val();
      });
    $(this)
      .closest(".modal-body")
      .find(".total-sum")
      .html("Общая сумма: " + total);
  }

  $(document).on(
    "keyup",
    "#add-invoice-modal2 .component-count",
    getSumComponentPrice
  );
  $(document).on(
    "keyup",
    "#add-invoice-modal2 .component-price",
    getSumComponentPrice
  );

  // ad position
  function createNewRowTableOrder(id) {
    if ($("#components").length) {
      comp_select = $("#components-select")
        .clone()
        .removeAttr("id")[0].outerHTML;
    }

    return `<tr id="${id}" class='added-row'>
      <td style="display:none;"><span class="tabledit-span tabledit-identifier"></span><input class="tabledit-input tabledit-identifier" type="hidden" name="id" value=""></td>
      <td><span class="tabledit-span tabledit-identifier">${id}</span><input class="tabledit-input tabledit-identifier" type="hidden" name="pp_number" value=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input style="cursor:pointer;" class="tabledit-input tabledit-identifier" type="text" name="name"  placeholder="name" value=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="qty" placeholder="quantity" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="pcb_count" placeholder="" value="" style=""></td>
      <td class='tabledit-edit-mode min-width-130 pcb-position_text' data-toggle='popover' data-placement='right' data-content=''><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="position_on_pcb" value="" style="" placeholder="pcb count" ></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="pn" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="doc_id" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="remark" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="substitutes" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="marking" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="code" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="dc" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="body" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="mfg" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="qty_in_packing" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="qty_to_buy_pcs" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="unit_price" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="sum" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="lead_time_days" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="weight_per_1_unit_kg" value="" style=""></td>
      <td class="tabledit-edit-mode"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="tp" value="" style=""></td>
      <td class="tabledit-edit-mode d-none"><span class="tabledit-span" style="display: none;"></span><input class="tabledit-input form-control input-sm" type="text" name="component_id" value="" style=""></td>
    <td style="white-space: nowrap; width: 1%;">
      <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
              <button type="button" class="tabledit-edit-button btn btn-sm mr-1 rounded btn-primary active" style="float: none;">
                <span class="mdi mdi-close"></span>
              </button>
              <button type="button" class="tabledit-delete-button btn btn-sm rounded btn-danger" style="float: none; display: none;">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
            <button type="button" class="tabledit-save-button btn btn-sm rounded btn-success" style="float: none;">
              <span class="mdi mdi-check"></span>
            </button>
            <button type="button" class="tabledit-confirm-button btn btn-sm rounded btn-danger" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
   </td>
   </tr>`;
  }

  // ////////////// *********** add row
  $("button.add-row").on("click", function () {
    const table = $("#engineer-table-order tbody");
    // let id = table.children().last().attr('id')
    let pp_num = table.children().last().find("td").eq(1).text();
    var clone1 = table.children().last().clone(true, true);
    if (parseInt(pp_num).toString() == "NaN") {
      pp_num = 0;
      table.children().last().remove();
    }
    pp_num++;
    table.append(createNewRowTableOrder(pp_num));
    table.find("tr").last().find("select").select2({
      tags: true,
      selectOnClose: true,
    });

    /*    var id = parseInt(clone1.attr('id'))
    clone1.find('td').eq(0).text(pp_num)
    table.append(clone1)
    clone1.find('.tabledit-edit-button').trigger('click')
    clone1.find('td').each(function(idx, el){
        $(el).find('.tabledit-input').val('')
    })*/
  });

  /** edit ************* */
  $(document).on("click", "table .tabledit-edit-button", function () {
    // for modal in formation order
    var name1 = $("#engineer-table-order .tabledit-input[name='name']").filter(
      function () {
        return $(this).css("display") != "none";
      }
    );
    name1.css("cursor", "pointer");

    $(this).closest("tr").find(".td-date input").datepicker("destroy");
    $(this)
      .closest("tr")
      .find(".td-date input")
      .datepicker({
        ...datepickerOptions,
      });
    $(this).closest("tr").find("select").select2({
      tags: true,
      selectOnClose: true,
    });
  });

  $(document).on("click", "table .tabledit-save-button", function () {
    $(this).closest("tr").removeClass("added-row");
    $(this).closest("tr").find(".td-date input").datepicker("destroy");
    // hide select
    $(this).closest("tr").find(".select2-container--disabled").hide();
  });

  $(document).on(
    "click",
    "#bank-statement-table .tabledit-edit-button",
    function (event) {
      const btn = $(this).closest("tr").find(".select-statement");
      if ($(btn).hasClass("d-none")) {
        $(btn).removeClass("d-none");
        $(this).closest("tr").find(".invoice-num").addClass("d-none");
      } else {
        $(btn).addClass("d-none");
        $(this).closest("tr").find(".invoice-num").removeClass("d-none");
      }
    }
  );

  $(document).on(
    "click",
    "#bank-statement-table .tabledit-save-button",
    function () {
      const btn = $(this).closest("tr").find(".select-statement");
      if ($(btn).hasClass("d-none")) {
        $(btn).removeClass("d-none");
        $(this).closest("tr").find(".invoice-num").addClass("d-none");
      } else {
        $(btn).addClass("d-none");
        $(this).closest("tr").find(".invoice-num").removeClass("d-none");
      }
    }
  );

  function getTableRowId(tableSelector) {
    let id = $(tableSelector).children().last().attr("id");
    if (!id) {
      id = 0;
    }
    return ++id;
  }

  /*--------------- ADD ROW TO TABLE - MODAL - ADD PROVIDER - CONTROL PAGE ----------*/
  $(document).on("click", ".modal #add-field_provider-modal", function () {
    var component_id = $("#add-provider-modal .component_id").attr(
      "data-component-id"
    );
    let id = getTableRowId("#add-provider-modal_control-order-page tbody");
    var providers_select;
    try {
      $.ajax({
        url: "/guides",
        data: { get_providers_select: 1 },
        async: false,
        method: "get",
        success: function (resp) {
          /*console.log(resp);*/ providers_select = resp;
        },
      });
    } catch (e) {
      // statements
      console.log(e);
    }
    // --------------
    try {
      $.ajax({
        url: "/guides",
        data: { get_currencies_select: 1 },
        async: false,
        method: "get",
        success: function (resp) {
          /*console.log(resp);*/ currencies = resp;
        },
      });
    } catch (e) {
      // statements
      console.log(e);
    }
    // --------------
    try {
      $.ajax({
        url: "/guides",
        data: { get_delivery_periods_select: 1 },
        async: false,
        method: "get",
        success: function (resp) {
          /*console.log(resp);*/ periods = resp;
        },
      });
    } catch (e) {
      // statements
      console.log(e);
    }
    // --------------

    const tr = `
      <tr id="${id}" class='added-row'>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span d-none" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="hidden" name="component" value="${component_id}" style="">
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          ${providers_select}
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="quantity" value="" style="">
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="price" value="" style="">
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          ${currencies}
        </td>
        <td class="tabledit-edit-mode td-date">
          <span class="tabledit-span" style="display: none;"></span>
          ${periods}
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="weight" value="" style="">
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="volume" value="" style="">
        </td>
        <td style="white-space: nowrap; width: 1%;" class=''>
          <div class="tabledit-toolbar btn-toolbar edit-mode" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
              <button type="button" class="tabledit-edit-button btn btn-sm mr-1 btn-primary rounded active" style="float: none;">
                <span class="mdi mdi-close"></span>
              </button>
              <button type="button" class="tabledit-delete-button btn btn-sm rounded btn-danger" style="float: none; display: none;">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
            <button type="button" class="tabledit-save-button btn btn-sm rounded btn-success" style="float: none;">
              <span class="mdi mdi-check"></span>
            </button>
            <button type="button" class="tabledit-confirm-button btn btn-sm rounded btn-danger" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
        </td>
      </tr>
    `;
    $(this).closest(".modal-body").find("tbody").append(tr);
    $(this).closest(".modal-body").find("table").removeClass("d-none");
    // $(this).closest('.modal-body').find('tbody .td-date input').datepicker({...datepickerOptions})

    $("#add-provider-modal_control-order-page tbody")
      .children("tr")
      .last()
      .find("select")
      .each(function () {
        // console.log($(this))
        $(this).select2({ tags: true, selectOnClose: true });
      });
  });

  /*--------------- ADD ROW #1 TO TABLE (PAGE - BANK) ---------------*/
  $(document).on("click", ".bank-page .add-expense", function () {
    let id = getTableRowId("#bank-statement-table tbody");
    const tr = `
      <tr id="${id}" class='added-row'>
        <td>
          <span class="tabledit-span tabledit-identifier">${id}</span>
          <input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="1"></td>
        <td class="tabledit-edit-mode td-date">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm input-date" 
          placeholder="Выберете дату"
          type="text" 
          name="date" readonly>
        </td>
        <td>0</td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="consumption" value="" style="">
        </td>
        <td>????</td>
        <td>
          <span class="invoice-num d-none">№ счёта: 978976</span>
          <button class='btn select-statement btn-block btn-primary' 
          data-target="#add-expense-modal" data-toggle="modal">Выбрать расход</button>
        </td>
        <td style="white-space: nowrap; width: 1%;" class=''>
          <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
              <button type="button" class="tabledit-edit-button btn btn-sm mr-1 rounded btn-primary active" style="float: none;">
                <span class="mdi mdi-close"></span>
              </button>
              <button type="button" class="tabledit-delete-button btn btn-sm rounded btn-danger" style="float: none; display: none;">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
            <button type="button" class="tabledit-save-button btn btn-sm rounded btn-success" style="float: none;">
              <span class="mdi mdi-check"></span>
            </button>
            <button type="button" class="tabledit-confirm-button btn btn-sm rounded btn-danger" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
        </td>
      </tr>`;
    $(".bank-page #bank-statement-table tbody").append(tr);
    $(".bank-page #bank-statement-table tbody .td-date input").datepicker({
      ...datepickerOptions,
    });
  });

  /*--------------- ADD ROW #2 TO TABLE (PAGE - BANK) ---------------*/
  $(document).on("click", ".bank-page .add-arrival", function () {
    let id = getTableRowId("#bank-statement-table tbody");
    const tr = `
      <tr id="${id}" class='added-row'>
        <td>
          <span class="tabledit-span tabledit-identifier">${id}</span>
          <input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="1"></td>
        <td class="tabledit-edit-mode td-date">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm input-date" 
           placeholder="Выберете дату"
          type="text" 
          name="date" readonly>
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="consumption" value="" style="">
        </td>
        <td>0</td>
        <td>????</td>
        <td>
          <span class="invoice-num d-none">№ счёта: 978976</span>
          <button class='btn select-statement btn-block btn-primary'
                      data-target='#journal-invoices-modal'
                      data-toggle='modal'>Выбрать приход</button>
        </td>
        <td style="white-space: nowrap; width: 1%;" class=''>
          <div class="tabledit-toolbar btn-toolbar edit-mode" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
              <button type="button" class="tabledit-edit-button btn btn-sm mr-1 btn-primary rounded active" style="float: none;">
                <span class="mdi mdi-close"></span>
              </button>
              <button type="button" class="tabledit-delete-button btn btn-sm rounded btn-danger" style="float: none; display: none;">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
            <button type="button" class="tabledit-save-button btn btn-sm rounded btn-success" style="float: none;">
              <span class="mdi mdi-check"></span>
            </button>
            <button type="button" class="tabledit-confirm-button btn btn-sm rounded btn-danger" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
        </td>
      </tr>
    `;

    $(".bank-page #bank-statement-table tbody").append(tr);
    $(".bank-page #bank-statement-table tbody .td-date input").datepicker({
      ...datepickerOptions,
    });
  });

  /*--------------- ADD ROW #1 TO TABLE (PAGE - BANK) ---------------*/
  $(document).on("click", ".add-arrival", function () {
    console.log("click");
    let id = getTableRowId("#arrivals-table tbody");
    if (
      !$("#arrivals-table tbody").children().attr("id") ==
      "No data available in table"
    )
      var first_td = `<td>
          <span class="tabledit-span tabledit-identifier"></span>
          <input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="">
        </td>`;
    else var first_td = "";
    const tr = `
      <tr id="${id}" class='added-row'>
        ${first_td}
        <td class='max-width-90'></td>
        <td class="tabledit-edit-mode td-date">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm input-date" 
          placeholder="Выберете дату"
          type="text" 
          name="date" readonly>
        </td>
        <td class="tabledit-edit-mode">
          <span class="tabledit-span" style="display: none;"></span>
          <input class="tabledit-input form-control input-sm" type="text" name="consumption" value="" style="">
        </td>
        <td style="white-space: nowrap; width: 1%;" class=''>
          <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
              <button type="button" class="tabledit-edit-button btn btn-sm mr-1 rounded btn-primary active" style="float: none;">
                <span class="mdi mdi-close"></span>
              </button>
              <button type="button" class="tabledit-delete-button btn btn-sm rounded btn-danger" style="float: none; display: none;">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
            <button type="button" class="tabledit-save-button btn btn-sm rounded btn-success" style="float: none;">
              <span class="mdi mdi-check"></span>
            </button>
            <button type="button" class="tabledit-confirm-button btn btn-sm rounded btn-danger" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
        </td>
      </tr>`;
    $(".accountant-order-page #arrivals-table tbody").append(tr);
    $(".accountant-order-page #arrivals-table tbody .td-date input").datepicker(
      {
        ...datepickerOptions,
      }
    );
  });

  $(document).on("click", "#account-modal .select-all", function () {
    $(this)
      .closest(".modal-body")
      .find('input[type="checkbox"]')
      .prop("checked", true);
  });

  // SHOW / SELECT / CHANGE BUTTONS
  $(document).on("click", "table button.tabledit-edit-button", function (ev) {
    /*if($('.tabledit-input:visible').length){
      ev.preventDefault()
      ev.stopPropagation()
    }*/
    // если строка новая, удалить при нажатии на крестик
    if ($(this).closest("tr").hasClass("added-row")) {
      $(this).closest("tr").remove();
      return;
    }

    // del
    $(this).closest(".btn-group").find(".tabledit-delete-button").hide();
    if ($(this).find("span").hasClass("mdi-pencil")) {
      // edit
      $(this).find(".mdi-pencil").attr("class", "mdi mdi-close");
      // save
      $(this).closest("tr").find(".tabledit-save-button").show();
    } else {
      // hide select
      $(this).closest("tr").find(".select2-container--disabled").hide();
      // edit
      $(this).find(".mdi-close").attr("class", "mdi mdi-pencil");
      // del
      $(this).closest(".btn-group").find(".tabledit-delete-button").show();
      // save
      $(this).closest("tr").find(".tabledit-save-button").hide();
    }
  });

  // DELETE ROW FROM TABLE
  $(document).on("click", "table button.tabledit-delete-button", function () {
    if (!$(this).find("i").hasClass("mdi-close")) {
      $(this).attr(
        "class",
        "tabledit-delete-button mr-1 btn btn-sm btn-primary rounded"
      );
      $(this).find("i").attr("class", "mdi mdi-close");
      $(this).closest(".btn-group").find(".tabledit-edit-button").hide();
    } else {
      $(this).attr(
        "class",
        "tabledit-delete-button btn btn-sm btn-danger rounded"
      );
      $(this).find("i").attr("class", "fas fa-trash-alt");
      $(this).closest(".btn-group").find(".tabledit-edit-button").show();
      $(this)
        .closest(".tabledit-toolbar")
        .find(".tabledit-confirm-button")
        .hide();
    }
  });

  //HIDE / SELECT
  $(document).on("click", "table .tabledit-save-button", function () {
    $(this).closest("tr");
    $(this)
      .closest(".tabledit-toolbar")
      .find(".mdi-close")
      .attr("class", "mdi mdi-pencil");
    $(this).closest(".tabledit-toolbar").find(".tabledit-delete-button").show();
    // setTimeout(function(){location.reload()}, 2000)
  });

  $(document).on("click", ".delete-category-item-btn", function () {
    const btn = $(this);
    btn.closest("li").remove();
  });

  $(".btn-left-side-bar, #sidebar-menu a").on("click", function () {
    $(".left-side-bar").toggleClass("show");
  });

  $(".btn-left-side-bar-modal").on("click", function () {
    $(".modal-add-component .slimscroll-menu").show();
    $(".modal-add-component .left-side-bar-modal").hide();
  });

  $(".modal-add-component .slimscroll-menu a").on("click", function () {
    $(".modal-add-component .slimscroll-menu").hide();
    $(".modal-add-component .left-side-bar-modal").show();
  });

  $(document).on("click", ".copy-table-row-btn", function () {
    const btn = $(this);
    const oldField = btn.closest("tr");
    const newField = oldField.clone();
    oldField.find(".copy-table-row-btn").addClass("d-none");
    oldField.find(".delete-table-row-btn").removeClass("d-none");
    newField.find(".delete-table-row-btn").removeAttr("disabled");
    newField.insertAfter(oldField);
  });

  $(document).on("click", ".delete-item-btn", function () {
    $(this).hide();
    $(this).closest("td").find("a").hide();
    $(this).closest("td").find("button[data-toggle='modal']").hide();
    $(this).closest("td").find("button.delete-item-btn-confirm").show();
    $(this).closest("td").find("button.canceled-change-btn").show();
  });

  $(document).on("click", ".canceled-change-btn", function () {
    $(this).hide();
    $(this).closest("td").find("a").show();
    $(this).closest("td").find("button[data-toggle='modal']").show();
    $(this).closest("td").find(".delete-item-btn-confirm").hide();
    $(this).closest("td").find(".delete-item-btn").show();
  });

  $(document).on("click", ".delete-item-btn-confirm", function () {
    $(this).closest("tr").addClass("tabledit-deleted-row");
    $(this).closest("tr").css({
      background: "#ff726f",
      transition: "1s",
    });
    $(this)
      .closest("tr")
      .fadeOut(700, function () {
        $(this).remove();
        var id = $(this).attr("id");
        $.post("", { del: "Y", id: id });
      });
  });

  $(document).on("click", ".delete-table-row-btn", function () {
    const btn = $(this);
    const oldField = btn.closest("tr");
    oldField.remove();
  });

  /*--------------- CUSTOM FILE UPLOAD (FILTER) INIT ---------------*/
  var jfiler_upload_options = {
    url: "/docs",
    data: null,
    type: "POST",
    enctype: "multipart/form-data",
    beforeSend: function () {},
    success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
      console.log(data);
      try {
        var data = JSON.parse(data);
        console.log(data);
        $(".jFiler-item-trash-action").attr({
          "data-offer": data.offer_id,
          "data-doc": data.id,
          "data-type": data.type,
        });
        // statements
      } catch (e) {
        // statements
        console.log(e);
      }
      console.log("загружен");
    },
    error: function (err) {
      console.log("ошибка");
    },
    statusCode: null,
    onProgress: null,
    onComplete: null,
  };

  const filterOptions = {
    limit: 3,
    maxSize: 10,
    fileMaxSize: 10,
    extensions: ["pdf"],
    showThumbs: true,
    captions: {
      button: "Select",
      feedback: "Select the files to upload",
      feedback2: "",
      drop: "Drop file here to Upload",
      removeConfirmation: "To delete?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "only PDF.",
        filesSize: "{{fi-name}}! Select the file before {{fi-fileMaxSize}} MB.",
        filesSizeAll:
          "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Not allowed to upload folders.",
      },
    },
    uploadFile: jfiler_upload_options,
    onRemove: function (data) {
      console.log("jfiler remove data app.js:703");
      console.log(data);
    },
    onEmpty: function (err) {
      console.log(err);
      console.log("jfiler empty app:705");
    },
  };

  // upload without ajax (on post)
  const filterOptions2 = {
    limit: 3,
    maxSize: 10,
    fileMaxSize: 10,
    extensions: ["pdf"],
    showThumbs: true,
    captions: {
      button: "Select",
      feedback: "Select the files to upload",
      feedback2: "",
      drop: "Drop file here to Upload",
      removeConfirmation: "To delete?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "Только PDF.",
        filesSize: "{{fi-name}}! Select the file before {{fi-fileMaxSize}} MB.",
        filesSizeAll:
          "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Not allowed to upload folders.",
      },
    },
  };

  if ($(".filter_input, #filter_input").length) {
    $(".filter_input, #filter_input").filer(filterOptions);
  }
  if ($(".filter_input_proof").length) {
    $(".filter_input_proof").filer({
      ...filterOptions,
      extensions: ["jpg", "png", "gif"],
    });
  }
  if ($(".filter_input_for_neworder").length) {
    $(".filter_input_for_neworder").filer({ ...filterOptions2 });
  }
  if ($("#filter_input_2").length) {
    $("#filter_input_2").filer(filterOptions);
  }
  if ($(".filter_input-qc").length) {
    $(".filter_input-qc").filer({
      ...filterOptions,
      captions: {
        button: "Add",
      },
    });
  }
  if ($(".filter_input-comment").length) {
    $(".filter_input-comment").filer({
      ...filterOptions,
      // captions: {
      //   // button: 'Прикрепить файл к комментарию',
      //   button: '<i class="fas fa-paperclip"></i>'
      // },
      templates: {
        box: '<ul class="jFiler-item-list 1"></ul>',
        item: `<li class="jFiler-item">
                        <div class="jFiler-item-container">
                            <div class="jFiler-item-inner">
                                <div class="jFiler-item-thumb">
                                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                                </div>
                                <div class="jFiler-item-assets jFiler-row">
                                    <ul class="list-inline pull-left">
                                        <li>{{fi-progressBar}}</li>
                                    </ul>
                                    <ul class="list-inline pull-right">
                                        <li><a class="jFiler-item-trash-action text-black-50"><u>Удалить</u></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>`,
        progressBar: '<div class="bar"></div>',
        itemAppendToEnd: false,
        removeConfirmation: true,
        _selectors: {
          list: ".jFiler-item-list",
          item: ".jFiler-item",
          progressBar: ".bar",
          remove: ".jFiler-item-trash-action",
        },
      },
    });
  }

  /*--------------- SUM CALCULATE (BUHGALTEER MODAL) ---------------*/
  function getPrecentOfSum() {
    return ($(".client-paid-present").textContent =
      Math.round(
        ($(".client-paid").val() * 100) / $(".client-must-paid").val()
      ) + "%");
  }

  $(".client-must-paid").keyup(function () {
    if ($(this).val().length === 0) {
      $(".client-paid-present").text("");
      return;
    }
    $(".client-paid-present").text(getPrecentOfSum());
  });
  $(".client-paid").keyup(function () {
    $(".client-paid-present").text(getPrecentOfSum());
  });
  $(".client-paid-present").text(getPrecentOfSum());

  /*------ TOUCH-SPIN -----*/
  if ($("input[name='counter']").length) {
    $("input[name='counter']").TouchSpin({
      initval: 0,
      max: 1000,
    });
  }

  $(".select2-selection").on("keyup", function (e) {
    if (e.keyCode === 27) {
      alert(123);
    }
  });

  // modal for nomenclatura
  $(document).on(
    "click",
    "#engineer-table-order .tabledit-input[name='name']",
    function () {
      $("#choose-component").modal("show");
      $("#choose-component").attr("data-id", $(this).closest("tr").attr("id"));
    }
  );

  $(document).on("click", ".choose-component-enginer", function (ev) {
    ev.preventDefault();
    comp_obj = '';
    try {
      var comp_obj = JSON.parse($(this).attr("data-component"));
    } catch(e) {
      console.log(e);
    }

    if(comp_obj !== undefined && comp_obj != false){

      var id = $("#choose-component").attr("data-id");
      var tr = $("#" + id);
      var name1 = tr.find(".tabledit-input[name='name']");
      var component_id = tr.find(".tabledit-input[name='component_id']");
      var pn = tr.find(".tabledit-input[name='pn']");
      var body = tr.find(".tabledit-input[name='body']");
      var marking = tr.find(".tabledit-input[name='marking']");
      var mfg = tr.find(".tabledit-input[name='mfg']");
      var doc_id = tr.find(".tabledit-input[name='doc_id']");
      name1.val($(this).text());
      pn.val(comp_obj.pn);
      body.val(comp_obj.body);
      marking.val(comp_obj.marking);
      mfg.val(comp_obj.manufacturer);
      component_id.val(comp_obj.id);
      doc_id.val(comp_obj.document_id);
      $("#choose-component").modal("hide");
    }
  });


  $('.customer-select2').select2({    minimumResultsForSearch: -1})
  // $('.order_type').select2({    minimumResultsForSearch: -1})

});
