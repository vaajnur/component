/*--------------------- DATATABLE JS -------------------*/
const datatableOptions = {
  responsive: true,
  lengthChange: 0,
  // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
  searching: 1,
  bInfo: false,
  "scrollY": 550,
  paging: true,
  dom: 'Bfrtip',
  buttons: ['excel'],
  language: {
    // sLengthMenu: "_MENU_",
    zeroRecords: 'Nothing was found for your search',
    search: '',
    searchPlaceholder: 'Search...',
    paginate: {previous: '‹', next: '›'},
    aria: {paginate: {previous: 'Previous', next: 'Next'}},
  },
  'initComplete': function () {
    $('*[type="search"][class="form-control form-control-sm"]')
      .addClass('input-lg')
      .removeClass('form-control-sm')
  },
  drawCallback: function() {
     // $('select').select2();
  }
};

// ========= tables EXcel
$('#provider-answers table, #offer-formation-table, #logistics table, #account-formation table, #arrivals-table, #purchase-order-table, #purchase-invoice-table, #accountant-payment-table, #supply-request-table, #product-comings-table, #qc table, #shipment table, #deffect table').DataTable({dom: 'Bfrtip', buttons: ['excel'],searching: 0,paging: false, bInfo: false, bSort:false })

if ($('#datatable-price-history').length || $('#datatable-components-add-modal').length) {
  $('#datatable-components, #datatable-price-history, #datatable-components-add, #datatable-components-add-modal').DataTable({
    ...datatableOptions
  })
}

//datatable-orders-journal
if ($('#datatable-active-orders').length || $('#datatable-new-orders').length || $('#datatable-orders-journal').length) {
  $('#datatable-active-orders, #datatable-new-orders /*#datatable-orders-journal*/').DataTable({
    ...datatableOptions,
    searching: 0
  })
}

$.fn.dataTable.ext.errMode = 'none';

if ($('#engineer-table-order').length) {
  const reduceTables = $('#engineer-table-order').DataTable({
    // ...datatableOptions,
    responsive: true,
    lengthChange: 0,
    searching: 1,
    bInfo: false,
    dom: 'Bfrtip',
    buttons: ['excel'],
    language: {
      // sLengthMenu: "_MENU_",
      zeroRecords: 'Nothing was found for your search',
      search: '',
      searchPlaceholder: 'Search...',
      paginate: {previous: '‹', next: '›'},
      aria: {paginate: {previous: 'Previous', next: 'Next'}},
    },
    'initComplete': function () {
      $('*[type="search"][class="form-control form-control-sm"]')
        .addClass('input-lg')
        .removeClass('form-control-sm')
    },
    paging: false,
   /* 'footerCallback': function (row, data, start, end, display) {
      var api = this.api(), data;

      // Remove the formatting to get integer data for summation
      var intVal = function (i) {
        return typeof i === 'string' ?
          i.replace(/[\$,]/g, '') * 1 :
          typeof i === 'number' ?
            i : 0;
      };

      // Total over all pages
      total = api
        .column(16)
        .data()
        .reduce(function (a, b) {
          return intVal(a) + intVal(b);
        }, 0);

      // Total over this page
      pageTotal = api
        .column(16, {page: 'current'})
        .data()
        .reduce(function (a, b) {
          return intVal(a) + intVal(b);
        }, 0);

      // Update footer
      $(api.column(4).footer()).html(
        pageTotal + '$'
      );
    }*/

  })
}



/*--------------------- TABLEDIT JS -------------------*/

const editButtons = {
  edit: {
    class: 'btn mr-1 btn-primary rounded',
    html: '<span class="mdi mdi-pencil"></span>',
    action: 'edit'
  },
  delete: {
    class: 'btn btn-danger rounded',
    html: '<i class="fas fa-trash-alt"></i>',
    action: 'delete'
  },
  save: {
    class: 'btn btn-success rounded',
    html: '<span class="mdi mdi-check"></span>',
    action: 'save'
  },
  confirm: {
    class: 'btn btn-danger rounded',
    html: '<span class="mdi mdi-check"></span>',
  }
}

function tableditForBankPage() {
  $('#bank-statement-table').Tabledit({
    autoFocus: false,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [1, 'date'],
        [3, 'consumption'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForExpenseJournalDeffect() {
  $('#table-expense-deffect').Tabledit({
    columns: {
      identifier: [0, 'id'],
      editable: [
        [0, 'pp'],
        [1, 'number'],
        [2, 'date'],
        [3, 'num_order'],
        [4, 'name_component'],
        [5, 'count'],
        [6, 'store'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForExpenseJournal() {
  $('#table-expense').Tabledit({
    columns: {
      identifier: [0, 'id'],
      editable: [
        [0, 'pp'],
        [1, 'number'],
        [2, 'date'],
        [3, 'num_order'],
        [4, 'name_component'],
        [5, 'count'],
        [6, 'store'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {
      const rows = $('#table-expense').find('tbody').children();
      $(rows).each(function () {
        // $(this).children().eq(-2).append(
        //   `<button  class='btn select-statement btn-block btn-primary d-none'
        //               data-target='#add-expense-modal'
        //               data-toggle='modal'>Выбрать расход</button>`)
        // $(this).children('td:nth-child(3)').html(`
        //   <span class="tabledit-span" style=""></span>
        //   <input class="tabledit-input form-control input-sm input-date"
        //    placeholder="Выберете дату"
        //   type="text" name="date" readonly="" disabled="" style="display: none;">
        // `)
      })
    },
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForOrder() {
  editButtons.delete.action = 'delete_component'
  editButtons.edit.action = 'edit_component'
  // console.log($('#components').text().trim())
  $('#engineer-table-order').Tabledit({
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        // [2, 'name'],
       // [2, 'name', $('#components').text().trim() != false ? $('#components').text().trim() : '[]'],
       [2, 'name'],
        [3, 'qty'],
        [5, 'position_on_pcb'],
        [6, 'pn'],
        [7, 'doc_id'],
        [8, 'remark'],
        [9, 'substitutes'],
        [10, 'marking'],
        [11, 'code'],
        [12, 'dc'],
        [13, 'body'],
        [14, 'mfg'],
        [15, 'qty_in_packing'],
        [16, 'qty_to_buy_pcs'],
        [17, 'unit_price'],
        [18, 'sum'],
        [19, 'lead_time_days'],
        [20, 'weight_per_1_unit_kg'],
        [21, 'tp'],
        [22, 'component_id'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {
      console.log('onDraw')

    },
    onSuccess: function (data, textStatus, jqXHR) { 
      console.log(data);  
      try {
        // statements
        var data = JSON.parse(data)
        if(data.type == 'danger')
          alert(data.text)
      } catch(e) {
        // statements
        console.log(e);
      }

    },
    onFail: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR.responseText) },
    onAlways: function () {
      console.log('onAlways')
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) { console.log(serialize) }
  });
}

function tableditForComingProvider() {
  $('#coming-providers-table').Tabledit({
    columns: {
      identifier: [0, 'id'],
      editable: [
        [0, 'pp'],
        [1, 'number'],
        [2, 'date'],
        [3, 'num_order'],
        [4, 'name_component'],
        [5, 'count'],
        [6, 'store'],
        // [7, 'document'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForBankPageModalAccouns() {
  $('#account-modal-table').Tabledit({
    autoFocus: false,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [4, 'count'],
        [5, 'price'],
        [6, 'sum'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      console.log('onAlways()');
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForBankPageModalInvoices() {
  $('#bank-journal-invoices-table').Tabledit({
    autoFocus: false,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [2, 'num-order'],
        [3, 'date'],
        [4, 'client'],
        [5, 'category'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) {},
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForControlPage() {
  var currencies, periods, providers;
  try {
    $.ajax({ url:'/guides', data: {'get_currencies':1}, async: false, method:'get', success: function(resp){ /*console.log(resp);*/currencies = resp }   })
  } catch(e) {
    // statements
    console.log(e);
  }
  // --------------
   try {
    $.ajax({ url:'/guides', data: {'get_delivery_periods':1}, async: false, method:'get', success: function(resp){ /*console.log(resp);*/periods = resp }   })
  } catch(e) {
    // statements
    console.log(e);
  }
  // --------------
   try {
    $.ajax({ url:'/guides', data: {'get_providers':1}, async: false, method:'get', success: function(resp){ /*console.log(resp);*/providers = resp }   })
  } catch(e) {
    // statements
    console.log(e);
  }
  // -----------
  editButtons.delete.action = 'delete_prov_offer';
  $('#add-provider-modal_control-order-page').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [2, 'provider', providers],
        [3, 'quantity'],
        [4, 'price'],
        [5, 'currency', currencies],
        [6, 'delivery_periods', periods],
        [7, 'weight'],
        [8, 'volume'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) { 
      console.log(data) 
      try {
        var data = JSON.parse(data)
        if(data.type == 'danger')
          alert(data.text)
      } catch(e) {
        // statements
        console.log(e);
      }
    },
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditForOrderAccountantPage() {
  $('#arrivals-table').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [2, 'date'],
        [3, 'consumption'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) { console.log(data) },
    onFail: function (jqXHR, textStatus, errorThrown) {},
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditPurchaseInvoice() {
  $('#purchase-invoice-table').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [9, 'invoicein_doc_date', 'date'],
        [10, 'invoicein'],
      ]
    },
    buttons: {
      edit: {
        class: 'btn mr-1 btn-primary rounded',
        html: '<span class="mdi mdi-pencil"></span>',
        action: 'edit'
      },
      save: {
        class: 'btn btn-success rounded',
        html: '<span class="mdi mdi-check"></span>',
        action: 'save'
      },
      confirm: {
        class: 'btn btn-danger rounded',
        html: '<span class="mdi mdi-check"></span>',
      }
    },
    onDraw: function () {},
    onSuccess: function (data, textStatus, jqXHR) { console.log(data) },
    onFail: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR.responseText) },
    onAlways: function () {
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {}
  });
}

function tableditAccountantPayment() {
  $('#accountant-payment-table').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [2, 'date'],
        [3, 'paymentout'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {
      console.log('onDraw')
    },
    onSuccess: function (data, textStatus, jqXHR) {
      console.log(data);
    },
    onFail: function (jqXHR, textStatus, errorThrown) {
      console.log('onFail(jqXHR, textStatus, errorThrown)');
    },
    onAlways: function () {
      console.log('onAlways()');
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {
      console.log('onAjax(action, serialize)');
    }
  });
}

function tableditSupplyRequest() {
  $('#supply-request-table').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        [5, 'shipment_date'],
      ]
    },
    buttons: {
      edit: {
        class: 'btn mr-1 btn-primary rounded',
        html: '<span class="mdi mdi-pencil"></span>',
        action: 'edit'
      },
      save: {
        class: 'btn btn-success rounded',
        html: '<span class="mdi mdi-check"></span>',
        action: 'save'
      },
      confirm: {
        class: 'btn btn-danger rounded',
        html: '<span class="mdi mdi-check"></span>',
      }
    },
    onDraw: function () {
      console.log('onDraw')
    },
    onSuccess: function (data, textStatus, jqXHR) {
      console.log('onSuccess(data, textStatus, jqXHR)');
    },
    onFail: function (jqXHR, textStatus, errorThrown) {
      console.log('onFail(jqXHR, textStatus, errorThrown)');
    },
    onAlways: function () {
      console.log('onAlways()');
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {
      console.log('onAjax(action, serialize)');
    }
  });
}

function editCategors(){
  editButtons.edit.class = 'edit_category_in_list btn mr-1 btn-primary rounded'
  editButtons.delete.action = 'del_categor'
  $('.categors-list').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        // [1, 'shipment_date'],
      ]
    },
    buttons: editButtons,
    onDraw: function () {
      console.log('onDraw')
    },
    onSuccess: function (data, textStatus, jqXHR) {
      console.log('onSuccess(data, textStatus, jqXHR)');
    },
    onFail: function (jqXHR, textStatus, errorThrown) {
      console.log('onFail(jqXHR, textStatus, errorThrown)');
    },
    onAlways: function () {
      console.log('onAlways()');
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {
      console.log('onAjax(action, serialize)');
    }
  });
  // редактирование категории
  $('.edit_category_in_list').on('click', function(ev){
      ev.preventDefault()
      ev.stopPropagation()
        var id = $(this).closest('tr').attr('id')
      if(id != false){
          $.get('/edit-category/'+id).done(function(data){
            // console.log(data)
              $('#categor_edit .modal-body').html(data)
              // select2tree
              setTimeout(function(){

                if($("#select_tree_edit").length){
                  var mydata = $("#select_tree_edit").attr('data-json')
                  var mydata = JSON.parse(mydata)
                  $("#categor_edit #select_tree_edit").select2ToTree({treeData: {dataArr:mydata}, maximumSelectionLength: 30});
                }
                $('#categor_edit').modal('show')
              }, 100)
          })
      }
  })
}



if($('.categors-list').length)
  editCategors()



  
  // редактирование категории модалка
  $('.edit_category_in_list').on('click', function(ev){
      ev.preventDefault()
      ev.stopPropagation()
        var id = $(this).closest('tr').attr('id')
      if(id != false){
          $.get('/edit-category/'+id).done(function(data){
            // console.log(data)
              $('#categor_edit .modal-body').html(data)
          })
        $('#categor_edit').modal('show')
      }
  })



function editCategorsComponents(){
    editButtons.edit.class = 'edit_components_in_list btn mr-1 btn-primary rounded'
    editButtons.delete.action = 'del_categor_comp'
  $('#category-components-list').Tabledit({
    autoFocus: false,
    hideIdentifier: true,
    columns: {
      identifier: [0, 'id'],
      editable: [
        /*[1, 'name'],
        [2, 'price'],
        [3, 'pn'],
        [4, 'comment'],
        [5, 'body'],
        [6, 'manufacturer'],
        [7, 'marking'],*/
      ]
    },
    buttons: editButtons,
    onDraw: function () {
      console.log('onDraw')
      // fix for btns
      $("#category-components-list tbody tr").each(function(idx, val){
        $(val).find('td').last().removeAttr('style').addClass('ml-auto')
      })
    },
    onSuccess: function (data, textStatus, jqXHR) {
      console.log('onSuccess(data, textStatus, jqXHR)');
      console.log(data);
    },
    onFail: function (jqXHR, textStatus, errorThrown) {
      console.log('onFail(jqXHR, textStatus, errorThrown)');
    },
    onAlways: function () {
      console.log('onAlways()');
      $('.tabledit-deleted-row').fadeOut(700, function () {
        $(this).remove()
      })
    },
    onAjax: function (action, serialize) {
      console.log('onAjax(action, serialize)');
    }
  });
}
if( $('#category-components-list').length)
  if(!$('#category-components-list').hasClass('not_edit'))
    editCategorsComponents()
  // редактирование категории модалка
  $('.edit_components_in_list').on('click', function(ev){
      ev.preventDefault()
      ev.stopPropagation()
      window.location.href = '/component/'+ $(this).closest('tr').attr('id')
   })


if ($('#supply-request-table').length > 0) {
  tableditSupplyRequest()
}

if ($('#accountant-payment-table').length > 0) {
  tableditAccountantPayment()
}

if ($('#bank-journal-invoices-table').length > 0) {
  tableditForBankPageModalInvoices()
}

if ($('#purchase-invoice-table').length > 0) {
  tableditPurchaseInvoice()
}

if ($('#add-provider-modal_control-order-page').length > 0) {
  tableditForControlPage()
}

if ($('#bank-statement-table').length > 0) {
  tableditForBankPage()
}

if ($('#table-expense-deffect').length > 0) {
  tableditForExpenseJournalDeffect()
}

if ($('#table-expense').length > 0) {
  tableditForExpenseJournal()
}

if ($('#coming-providers-table').length > 0) {
  tableditForComingProvider()
}

if ($('#engineer-table-order').length > 0) {
  tableditForOrder()
}

if ($('#account-modal-table').length > 0) {
  tableditForBankPageModalAccouns()
}

if ($('#arrivals-table').length > 0) {
  tableditForOrderAccountantPage()
}


$("#category-components-list").DataTable({ 
paging: true, 
searching: 1,
bInfo: true, 
bSort: false,
lengthChange: 0,
"language": {
            "decimal":        "",
            "emptyTable":     "No data available in table",
            // "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
            "info":           "Найдено _TOTAL_",
            // "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoEmpty":      "Не найдено",
            // "infoFiltered":   "(filtered from _MAX_ total entries)",
            "infoFiltered":   "",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Show _MENU_ entries",
            "loadingRecords": "Loading...",
            "processing":     "Processing...",
            "search":         "Search:",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "First",
                "last":       "Last",
                "next":       "Next",
                "previous":   "Previous"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        },
oLanguage: {
        oPaginate: {
            sNext: '>',
            sPrevious: '<'
        }
    }
})


function categor_components_list_nameDT(){

  $("#categor-components-list-name").DataTable({ 
  paging: true, 
  searching: 1,
  bInfo: true, 
  bSort: false,
  lengthChange: 0,
  "language": {
              "decimal":        "",
              "emptyTable":     "No data available in table",
              // "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
              "info":           "Найдено _TOTAL_",
              // "infoEmpty":      "Showing 0 to 0 of 0 entries",
              "infoEmpty":      "Не найдено",
              // "infoFiltered":   "(filtered from _MAX_ total entries)",
              "infoFiltered":   "",
              "infoPostFix":    "",
              "thousands":      ",",
              "lengthMenu":     "Show _MENU_ entries",
              "loadingRecords": "Loading...",
              "processing":     "Processing...",
              "search":         "Search:",
              "zeroRecords":    "No matching records found",
              "paginate": {
                  "first":      "First",
                  "last":       "Last",
                  "next":       "Next",
                  "previous":   "Previous"
              },
              "aria": {
                  "sortAscending":  ": activate to sort column ascending",
                  "sortDescending": ": activate to sort column descending"
              }
          },
  oLanguage: {
          oPaginate: {
              sNext: '>',
              sPrevious: '<'
          }
      }
  })
  // Поиск
  if($('#categor-components-list-name_filter input[type="search"]').length){
    $('#categor-components-list-name_filter input[type="search"]').attr('class', 'form-control inp-search_categ btn-search-cat').attr('placeholder', 'Search').after('<span class="fe-search"></span>')
  }

}

categor_components_list_nameDT()