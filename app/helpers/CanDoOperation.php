<?php defined('DIRECT') OR exit('No direct script access allowed');

/**
 * 
 */
if( ! function_exists('canDoOperation') ){
	function canDoOperation($action){
		$titan = Loader::getInstance();
		$titan->plugin('session');
		$db = $titan->database();
		$user_id = $titan->session->get('logined')->role;
		$roles = $db->select('*')->from('roles')->where('id', $user_id, '=')->get()->row();
		// var_dump($roles);
		return $roles->{$action};
		return 0;
	}
}