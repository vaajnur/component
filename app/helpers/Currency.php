<?php defined('DIRECT') OR exit('No direct script access allowed');

	/**
	 * [getCurrencies description]
	 * @return [type] [description]
	 */
	function getCurrencies(){
		$cache_time_out = '36000'; // Время жизни кэша в секундах
		$file_currency_cache = APP_DIR.'/cache/currency.txt';
		if(!is_file($file_currency_cache) || filemtime($file_currency_cache) < (time() - $cache_time_out)){
			// echo "111111111";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$out = curl_exec($ch);
			curl_close($ch);
			file_put_contents($file_currency_cache, $out);
		}else{
			// echo "22222222";
			$out = file_get_contents($file_currency_cache);
		}
		return json_decode(json_encode(simplexml_load_string($out)), true);
	}

	/**
	 * [formatForSelect description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	function currencyForSelect(){
		$data = getCurrencies();
		$cur = [];
		foreach ($data['Valute'] as $key => $value) {
			global $cur;
			// $cur[$value['CharCode']] = $value['Name']; 
			$cur[$value['CharCode']] = $value['CharCode']; 
		}
		// $cur = array_merge(['none' => 'no selected', 'RUB' => 'Российский рубль'], $cur);
		$cur = array_merge(['none' => 'no selected', 'RUB' => 'RUB'], $cur);
		return $cur;
	}

	function getCourse($valuta){
		$data = getCurrencies();
		foreach ($data['Valute'] as $key => $value) {
			if($valuta == $value['CharCode'])
				return (float)$value['Value']/$value['Nominal'];
		}
		return 1;
	}

	function convertCurrency($sum, $valute){
		if($valute != 'USD' && $valute != 'RUB'){
			$prev = getCourse($valute);
			if($prev !== 1)
				$sum = $sum*$prev/getCourse('USD');
		}elseif($valute == 'RUB'){
			$sum = $sum/getCourse('USD');
		}elseif($valute == 'USD'){
		}
		return number_format($sum, 2, '.', '');
	}