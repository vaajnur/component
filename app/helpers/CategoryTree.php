<?php defined('DIRECT') OR exit('No direct script access allowed');

/**
 * 
 */

    function getCat()
    {
        $titan = Loader::getInstance();
        $db = $titan->database();
        $categors = $db->select('*')->from('categories')->order_by('name', 'asc')->get()->results();
        foreach($categors as $cat){
            $categors_list[$cat->id] = json_decode(json_encode($cat), true);
        }
        return $categors_list;
    }

    function getTree() {
        $dataset = getCat();
        $tree = array();

        foreach ($dataset as $id => &$node) {    
            //Если нет вложений
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }else{ 
                //Если есть потомки то перебераем массив
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    }

    /**
     * [getTreeChilds description]
     * @param  [type] $tree      [description]
     * @param  [type] $parent_id [description]
     * @return [type]            [description]
     */
    function getTreeChilds($tree, $parent_id){
        $result = [];
        foreach($tree as $id => $nodes){
            if($id == $parent_id && $nodes['childs']){
                array_walk_recursive($nodes['childs'], function ($item, $key) use (&$result) {
                    if($key == 'id')
                        $result[] = $item;    
                });
                array_unshift($result, $parent_id);
            }elseif($id != $parent_id && $nodes['childs']){
                $result = array_merge($result, getTreeChilds($nodes['childs'], $parent_id));
            }
        }
        return $result;
    }

    /**
     * [getTreeForSelect2 description]
     * @param  [type] $dataset [description]
     * @return [type]          [description]
     */
    function getTreeForSelect2($needed_id = false) {
        if($needed_id == false)
            $tree = [array('id' => 0 , 'text' => 'Root cattegory', 'selected'=> true, 'parent_id' => 0)];
         $dataset = getCat();

        foreach ($dataset as $id => &$node) {    
            //Если нет вложений
            $node = ['id' => $node['id'].'_'.$node['level'], 'text' => $node['name'], 'parent_id' => $node['parent_id'], 'inc' => $node['inc'] ];
            if($needed_id != false && $needed_id == $id)
                $node['selected'] = true;
            // debug($node);
            if ($node['parent_id'] == 0){
                $tree[] = &$node;
            }else{ 
                //Если есть потомки то перебераем массив
                $dataset[$node['parent_id']]['inc'][] = &$node;
            }
        }
        return $tree;
    }


    function getTreeSelect($arr, $sub = false) {
        if($sub == false)
            $select = '<select name="parent_id" class="form-control">';
        foreach ($arr as $id => $node) {
            $select .= "<option value='{$node['id']}_{$node['level']}'>".str_repeat('&nbsp;&nbsp;&nbsp;', $node['level'])."{$node['name']}</option>";
            if ($node['childs'] !== null){
                $select .= getTreeSelect($node['childs'], true); 
            }
        }
        if($sub == false)
            $select .= "</select>";
        return $select;
    }

    function getTreeHTML($arr, $sub = false) {
        $tree = '<ul '.($sub==true?' class="nav-second-level" aria-expanded="false"':' class="metismenu" id="side-menu" ').'>';

            // var_dump($tree);
        foreach ($arr as $id => $node) {    
            //Если нет вложений
            $tree .= "<li  class='level-{$node['level']} ".($node['childs'] !== null?" has-children ":"")." ' data-level='{$node['level']}'>";
            $tree .= "<a href='/category/{$node['id']}'>{$node['name']}</a><a href='#' class='for-arrow'>";
                // $tree .= "<a href='javascript: void(0);'><span>{$node['name']}</span>";
            if ($node['childs'] !== null){
                $tree .= "<span class='menu-arrow'></span></a>";
                $tree .= getTreeHTML($node['childs'], true); 
            }else{
                $tree .= "</a>";
            }

            $tree .= "</li>";
        }
            $tree .= "</ul>";
        return $tree;
    }

    function getTreeHTML2($arr, $sub = false) {
        $tree = '<ul '.($sub==true?' class="nav-second-level" aria-expanded="false"':' class="metismenu" id="side-menu" ').'>';

            // var_dump($tree);
        foreach ($arr as $id => $node) {    
            //Если нет вложений
            $tree .= "<li  class='level-{$node['level']} ".($node['childs'] !== null?" has-children ":"")." ' data-level='{$node['level']}'>";
            $tree .= "<a href='#' data-id='{$node['id']}' class='choose-modal-category'>{$node['name']}</a><a href='#' class='for-arrow'>";
                // $tree .= "<a href='javascript: void(0);'><span>{$node['name']}</span>";
            if ($node['childs'] !== null){
                $tree .= "<span class='menu-arrow'></span></a>";
                $tree .= getTreeHTML2($node['childs'], true); 
            }else{
                $tree .= "</a>";
            }

            $tree .= "</li>";
        }
            $tree .= "</ul>";
        return $tree;
    }