<?php defined('DIRECT') OR exit('No direct script access allowed');

/**
 * 
 */
if( ! function_exists('f12') ){
	function f12($var){
		$json = json_encode($var, JSON_UNESCAPED_UNICODE);
		echo "
		<script>
			console.log('f12 dump ====================================================');
			console.log($json);
			// console.log('end dump -------------------------------------------------');
		</script>
		";

	}
}