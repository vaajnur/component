<?php defined('DIRECT') OR exit('No direct script access allowed');
/**
 * Autoload config file
 * 
 * Turan Karatuğ - <tkaratug@hotmail.com.tr>
 */

// Autoload helpers
$autoload['helpers']	= ['url', 'debug', 'canDoOperation', 'f12', 'currency', categoryTree];

// Autoload plugins
$autoload['plugins']	= [
	'input', 
	'asset',
	'session',
	'mail',
	'upload',
	'log',
];