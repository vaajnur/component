<form action='/category' method="post" enctype="multipart/form-data">
    <div class='row'>
        <div class="form-group col-md-6">
            <label>Category name:</label>
            <input type="text" name="name" class="category-post form-control" placeholder='Category name' value="<?=$category->name;?>">
        </div>
        <div class="form-group col-md-6">
            <label>Parent category:</label>
            <? /*if (!empty($categories)) {?>
                <select name="parent_id" class="empl-post form-control" id="">
                <?foreach ($categories as $key => $categor) {?>
                    <option value="<?=$categor->id;?>_<?=$categor->level;?>"><?=$categor->name;?></option>
                <?}?>
                </select>
            <?}*/?>
            <select name="parent_id" id="select_tree_edit" data-json='<?=$menu_tree_select2;?>'></select>
        </div>
    </div>
    
    <? 

    // debug($category);
    if (!empty($category->custom_f)) {
        foreach ($category->custom_f as $key => $fields) {?>
    
        <div class="row">
            <div class="col-md-6 pt-2">
              <div class="row justify-content-between">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span del-property edit-del">Delete</span>
                        </div>
                    </div>
                <input type="text" name="custom_f_name[<?=$fields->name;?>]" class="category-post form-control edit_categor-modal-view" placeholder='Property Name' value="<?=$fields->name;?>">
            </div>
            <div class="col-md-6  pt-2 ml-auto"><label>Unit of measurement:</label>
                <input type="text" name="custom_f_value[<?=$fields->name;?>]" class="category-post form-control" placeholder='Unit of measurement' value="<?=$fields->value;?>">
            </div>
        </div>

        <?}
    }?>

    <div class="row">
        <div class="col-md-6 pt-2">
          <div class="row justify-content-between">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span del-property edit-del">Delete</span>
                        </div>
                    </div>
            <input type="text" name="custom_f_name[]" class="category-post form-control edit_categor-modal-view" placeholder='Property Name' value="">
        </div>
        <div class="col-md-6 pt-2 ml-auto"><label>Unit of measurement:</label>
            <input type="text" name="custom_f_value[]" class="category-post form-control" placeholder='Unit of measurement' value="">
        </div>
    </div>
    
    <input type="hidden" name="referer" value="categories" class="">
    <input type="hidden" name="id" value="<?=$category->id;?>" class="">
    <div class='col-12'>
        <hr class='ym-1'>
    </div>
    <!-- /////////////// new fields /////////////// -->
    <div class='col-12'>
        <div class="row">
            <div class="col-md-6 pt-2">
                <button type="submit" name="edit_category" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Save
                </button>
            </div>
            <div class="col-md-6 pt-2 ml-auto">
                <button type="button" class="btn btn-block btn-dark waves-effect width-md waves-light">Delete</button>
            </div>
        </div>
    </div>
</form>