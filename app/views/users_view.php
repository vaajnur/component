<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row">
              <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Users</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-3'>
            <button
                    type="button"
                    data-toggle='modal'
                    data-target='.modal-add-user'
                    class="btn btn-block btn--md btn-success waves-effect waves-light add-user-btn">Add user
            </button>
          </div>
        </div>
        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-users"
                     class="table w-100 table-bordered">
                <thead>
                <tr>
                  <th>Full name</th>
                  <th>Login</th>
                  <th>Role</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <? if (!empty($users)) {
                    foreach ($users as $key => $user) {?>
                  
                <tr id='<?=$user->id;?>'>
                  <td><?=$user->fio;?></td>
                  <td><?=$user->login;?></td>
                  <td><?=$user->role_name;?></td>
                  <td class='text-right text-nowrap'>
                    <button class='btn btn-icon btn-sm btn-primary ml-1 edit-user-btn'
                            data-toggle='modal'
                            data-user='<?=json_encode($user);?>'
                            data-target='.modal-edit-user'
                    >
                      <i class='fas fa-pencil-alt'></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-primary active canceled-change-btn"
                            style="float: none; display: none;">
                      <span class="mdi mdi-close"></span>
                    </button>
                    <button class='btn btn-icon btn-sm btn-danger ml-1 delete-item-btn'>
                      <i class='fas fa-trash-alt'></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-danger ml-1 delete-item-btn-confirm delete-user-btn-confirm"
                            style="float: none; display: none;">
                      <span class="mdi mdi-check"></span>
                    </button>
                  </td>
                </tr>

                    <?}
                  }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal ADD USER -->
<div class="modal fade bd-example-modal-lg modal-add-user"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add user</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form method="post">
            <div class="form-group w-100">
              <label class="col-form-label w-100">Full name:</label>
              <input name='fio' type="text" class="form-control" placeholder="Full name">
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Role:</label>
              <select class="form-control role" name="role">
                <? if (!empty($roles)) {
                  foreach ($roles as $key => $rol) {?>
                      <option value="<?=$rol->id;?>"><?=$rol->name;?></option>
                  <?}
                }?>
              </select>
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Login:</label>
              <input name='login' type="text" class="form-control" placeholder="Login">
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Password:</label>
              <input name='password' type="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group w-100 pt-2">
              <button type="submit" value="1" name="add-user" class='btn btn-block btn-success waves-effect'>Add</button>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- modal EDIT USER -->
<div class="modal fade bd-example-modal-lg modal-edit-user"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form method="post">
            <div class="form-group w-100">
              <label class="col-form-label w-100">Full name:</label>
              <input name='fio' type="text" class="form-control name" placeholder="Full name">
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Role:</label>
              <select class="form-control role" name="role">
                <? if (!empty($roles)) {
                  foreach ($roles as $key => $rol) {?>
                      <option value="<?=$rol->id;?>"><?=$rol->name;?></option>
                  <?}
                }?>
              </select>
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Login:</label>
              <input name='login' type="text" class="form-control login" placeholder="Login">
            </div>
            <div class="form-group w-100">
              <label class="col-form-label w-100">Password:</label>
              <input name='password' type="password" class="form-control password" placeholder="Password">
            </div>
            <input type="hidden" name="id" class="user-id" value="">
            <div class="form-group w-100 pt-2">
              <button type="submit" name="edit-user" value="1" class='btn btn-block btn-success waves-effect'>Save</button>
              </button>
              <button type="submit" name="del-user" value="1" class='btn btn-block btn-danger'>Delete</button>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
