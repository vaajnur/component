<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">

        <div class="row pb-3">
          <div class="col-12">
            <div class="row align-items-center">
              <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Providers</h4>
                </div>
              </div>
              <div class="col-md-3 offset-md-3">
                <button
                        type="button"
                        data-toggle='modal'
                        data-target='.modal-add-provider'
                        class="btn btn-block btn--md btn-success waves-effect waves-light add-provider-btn">Add provider
                </button>
              </div>
            </div>
          </div>
        </div>




        <div class='row'>
          <div class='col-md-3 offset-md-9 mb-1 mb-md-2'>
            <select class="form-control">
              <? if (!empty($specialization)) {
                foreach ($specialization as $key => $spec) {?>
                  <option value="<?=$spec->id;?>"><?=$spec->name;?></option>
                <?}
              }?>
            </select>
          </div>
          <div class='col-md-2'>
            <h5>Filter:</h5>
          </div>
          <div class='col-md-2 mb-1 mb-dm-0 offset-md-5'>
            <span id='client-filter-select'></span>
          </div>
          <div class='col-md-2 mb-1 mb-dm-0'>
            <span id='specialisation-filter-select'></span>
          </div>
          <div class='col-md-1 mt-1 mt-md-0'>
            <button id='reset-all-providers' class="btn-icon btn btn-block btn-secondary waves-effect">
              Reset
            </button>
          </div>
        </div>



        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-providers"
                     class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                <thead>
                <tr>
                  <th>Provider name</th>
                  <th>Specialization</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <? if (!empty($providers)) {
                    foreach ($providers as $key => $provider) {?>
                  
                      <tr id='<?=$provider->id;?>'>
                        <td><?=$provider->name;?></td>
                        <td><?=$provider->spec;?></td>
                        <td class='text-right text-nowrap'>
                          <button type="button" class="btn btn-sm btn-primary active canceled-change-btn"
                                  style="float: none; display: none;">
                            <span class="mdi mdi-close"></span>
                          </button>
                          <button class='btn btn-icon btn-sm btn-primary ml-1 edit-provider-btn'
                                  data-toggle='modal'
                                  data-provider='<?=json_encode($provider);?>'
                                  data-target='.modal-edit-provider'
                          >
                            <i class='fas fa-pencil-alt'></i>
                          </button>
                          <button class='btn btn-icon btn-sm btn-danger ml-1 delete-item-btn'>
                            <i class='fas fa-trash-alt'></i>
                          </button>
                          <button type="button" class="btn btn-sm btn-danger ml-1 delete-item-btn-confirm"
                                  style="float: none; display: none;">
                            <span class="mdi mdi-check"></span>
                          </button>
                        </td>
                      </tr>

                    <?}
                  }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal ADD COMPONENT -->
<div class="modal fade bd-example-modal-lg modal-add-provider"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="mySmallModalLabel">Add provider</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form method="post">
          <div class="form-group w-100">
            <label class="col-form-label w-100">Name:</label>
            <input name='name' type="text" class="form-control" placeholder="Name">
          </div>
          <div class="form-group w-100">
            <label class="col-form-label w-100">Specialization:</label>
            <select name="specialization" id=""  class="form-control">
              <? if (!empty($specialization)) {
                foreach ($specialization as $key => $spec) {?>
                  <option value="<?=$spec->id;?>"><?=$spec->name;?></option>
                <?}
              }?>
            </select>
          </div>
          <div class="form-group w-100 pt-2">
            <button type="submit" name="send-new-provider" value="1" class='btn btn-block btn-success'>Add</button>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<!-- modal EDIT PROVIDER -->
<div class="modal fade modal-edit-provider"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit provider</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
        <div class="form-group w-100">
          <label class="col-form-label w-100">Name:</label>
          <input name='name' type="text" class="form-control prov-name" placeholder="Название">
        </div>
        <div class="form-group w-100">
          <label class="col-form-label w-100">Specialization:</label>
          <select name="specialization" id=""  class="form-control prov-spec">
            <? if (!empty($specialization)) {
                foreach ($specialization as $key => $spec) {?>
                  <option value="<?=$spec->id;?>"><?=$spec->name;?></option>
                <?}
              }?>
          </select>
        </div>
        <input type="hidden" class="provider-id" name="id" value="">
        <div class="form-group w-100 pt-2">
          <button type="submit" name="edit-provider" value="1" class='btn btn-block btn-success'>Save</button>
          <button type="submit" name="del-provider" value="1" class="mt-2 btn btn-block btn--md btn-danger waves-effect waves-light">Delete
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<?php $this->load->view('layouts/footer_view'); ?>
			
