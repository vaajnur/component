<?php $this->load->view('layouts/header_view'); ?>
<div class="content-page">
    <div class="content">
        <div class='container-fluid'>
            <div class="row pb-3">
                <div class="col-12 mb-4">
                    <div class="page-title-box text-center">
                        <h4 class="page-title">
                            <?=$edit == true?'Edit order':'Create order';?>
                        </h4>
                    </div>
                </div>
                <?if(!empty($mess)){?>
                <div class="col-12">
                    <div class="alert alert-<?=$mess['type'];?>">
                        <?=$mess['text'];?>
                    </div>
                </div>
                <? } ?>
                <div class='col-md-8 offset-md-2'>
                    <? if($edit==true):?>
                    <a href="/order/<?=$id;?>" class="btn btn-primary">Back to detail</a>
                    <? endif;?>
                    <form name="" action="/create-order" accept="" method="post" enctype="multipart/form-data">
                        <? if($edit==true):?>
                        <input type="hidden" value="<?=$id;?>" name="id">
                        <? endif;?>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Number:</label>
                                <input type="text" name='order_num' value="<?=$edit==true?$order->order_num:$order_num;?>" class="form-control" placeholder="Number" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Name:</label>
                                <input type="text" name='name' value="<?=$edit==true?$order->name:'';?>" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Date</label>
                                <input type="text" value="<?=$edit?$order->date:date('Y-m-d');?>" name='date' class="form-control" placeholder="Date" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Client:</label>
                                        <? //debug($clients);?>
                                <select name="customer" id="" class="customer-select2">
                                    <? 
                                    if (!empty($clients)) {
                                        foreach ($clients as $key => $client) {?>
                                            <option <?=$edit?($order->customer==$client->id?' selected ':''):'';?> name="" value="<?=$client->id;?>"><?=$client->full_name;?></option>
                                        <?}
                                    }?>
                                </select>
                                <!-- <input type="text" name='customer' value="<?=$edit?$order->customer:'';?>" class="form-control" placeholder="Client"> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Order type</label>
                                <select class="form-control order_type" name="type" id="">
                                    <option value="simple" <?=$edit&&$order->type=='simple'?' selected ':'';?>>Simple</option>
                                    <option value="contract" <?=$edit&&$order->type=='contract'?' selected ':'';?>>Contract</option>
                                </select>
                                 <label class="col-form-label w-100 pcb_count">PCB count</label>
                                <input type="text" name="pcb_count" value="<?=$edit&&$order->pcb_count==true?$order->pcb_count:'';?>" class="pcb_count form-control <?=$edit&&$order->type=='contract'?'':'d-none';?>" placeholder="PCB count">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Attach</label>
                                <? 
                                 // debug($order);
                                if($edit&&$order->document_id==true):?>
                                <div class="">
                                    <div class="jFiler-item-thumb">
                                        <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                                    </div>
                                    <div class="doc-name"><a target="_blank" href="<?=$order->document_path;?>">
                                            <?=$order->document_name;?></a></div>
                                    <a data-offer="<?=$order->id;?>" data-doc="<?=$order->document_id;?>" data-type="order_document" class="del-doc jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                                </div>
                                <? endif;?>
                                <input type="file" name="order_document[]" class="filter_input_for_neworder">
                            </div>
                            <!-- <div class="form-group col-md-6">
                                <label class="col-form-label w-100">Additional cost:</label>
                                <input type="text" value="<?=$edit&&$order->additional_cost==true?$order->additional_cost:'';?>" name='additional_cost' class="form-control" placeholder="Additional cost">
                            </div> -->
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label class="col-form-label w-100">Description:</label>
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10" placeholder="Description"><?=$edit&&$order->description==true?$order->description:'';?></textarea>
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-12'>
                                <button type='submit' name="<?=$edit == true?'send-edit-order':'send-new-order'?>" value="1" class='btn btn-block btn-success'>
                                    <?=$edit == true?'Edit':'Create';?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
