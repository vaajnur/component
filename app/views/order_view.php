<?php $this->load->view('layouts/header_view'); ?>
<div class="bg-block_img">
  <img class="bg-img_order" src="/public/images/bg-oder.svg" alt="" />
</div>

  <div class="content-page accountant-order-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-md-12">
            <div class="row align-items-center">
              <div class="col-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Order №<?=$order->order_num;?></h4>
                  <br>
                </div>
                <p><?=$order->name;?></p>
              </div>
              <div class='col-3 offset-3'>
                  <a
                          href="/edit-order/<?=$order->id;?>"
                          type="button"
                          class="btn btn-block btn--md btn-success waves-effect waves-light">Edit order
                  </a>
                  <? if($user->role == '1' || $user->role == '7'){?>
                  <button
                          type="button"
                          href="/delete-order/<?=$order->id;?>"
                          data-toggle="modal" 
                          data-target="#confirm-delete"
                          class="btn btn-block btn--md btn-danger waves-effect waves-light">Delete order
                  </button>
                  <?}?>
                  <a
                          href="/generate_pdf/<?=$order->id;?>"
                          type="button"
                          target="_blank"
                          class="btn btn-block btn--md btn-primary waves-effect waves-light">Generate PDF
                  </a>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col">
            <span class='slide-order mb-4'>Expand order information</span>

            <div class='row slide-order__content'>
              <div class="col-md-9">
                <!-- <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Quotation #</div>
                  <div class="col-8"></div>
                </div> -->
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Date</div>
                  <div class="col-8"><span class=''><?=$order->date;?></span></div>
                </div>
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Type</div>
                  <div class="col-8"><span class=''><?=$order->type;?></span></div>
                </div>
                <? if ($order->pcb_count != false) {?>
                  <div class="row mb-4">
                    <div class="col-4 font-weight-bold">Pcb QTY</div>
                    <div class="col-8"><span class=''><?=$order->pcb_count;?></span></div>
                  </div>
                  <?}?>
                  <div class="row mb-4">
                    <div class="col-4 font-weight-bold">№ n\n</div>
                    <div class="col-8"><span class=''><?=$order->order_num;?></span></div>
                  </div>
                  <? if ($order->order_num_2 != false) {?>
                  <div class="row mb-4">
                    <div class="col-4 font-weight-bold">№ n\n 2</div>
                    <div class="col-8"><span class=''>GKK-<?=$order->order_num_2;?></span></div>
                  </div>
                  <?}?>
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Client</div>
                  <div class="col-8"><span class=''><?=$order->customer;?></span></div>
                </div>
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Description</div>
                  <div class="col-8"><span class=''><?=$order->description;?></span></div>
                </div>

                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Documents</div>
                  <div class="col-8"><span class=''><a target="_blank" href="<?=$order->order_document_path;?>"><?=$order->order_document;?></a></span></div>
                </div>
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Additional cost</div>
                  <div class="col-8"><span class=''><?=$order->additional_cost;?></span></div>
                </div>
                <br>
                <div class="row mb-4">
                  <div class="col-4 font-weight-bold">Order shipped:</div>
                  <div class="col-8"><?=$order->demand?'Yes':'No';?></div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>logistics</div>
                  <div class='col-8'>Delivery price: <?=$order->logistic_price;?> р</div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Payment in sum</div>
                  <div class='col-8'><?=$order->paymentin_sum;?> р</div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Payment out sum</div>
                  <div class='col-8'><?=$order->paymentout_sum;?> р</div>
                </div>


                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Payment difference</div>
                  <div class='col-8'><?=$order->payment_diff;?> р</div>
                </div>


                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Quota status</div>
                  <div class='col-8'><?=$order->quota_status;?></div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Supply status</div>
                  <div class='col-8'><?=$order->supply_status;?></div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Payment in status</div>
                  <div class='col-8'><?=$order->status_payin;?></div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Payment out status</div>
                  <div class='col-8'><?=$order->status_payout;?></div>
                </div>

                <div class='row mb-4'>
                  <div class='font-weight-bold col-4'>Demand status</div>
                  <div class='col-8'><?=$order->demand_status;?></div>
                </div>


              </div>

            </div>
          </div>
        </div>


      

        <!-- Комментарий -->
        <!-- <div class="row pt-3">
          <div class='col-12 mb-3'>
            <ul class="nav custom-tabs nav-tabs d-flex">
              <li class="nav-item text-center <?=$tab_class;?>">
                <a href="#comment" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link active">
                  <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                  <span class="d-none d-sm-block">Заявка</span>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12">
            <div class="tab-content border-0 p-0">
              <div role="tabpanel" class="tab-pane fade active show" id="comment">
              </div>
            </div>
          </div>
        </div> -->


        <? //debug($order);?>


        <!-- ************** ALL TABLES ************** -->
        <div class="row pt-3">
          <div class='col-12 mb-3'>
            
            <ul class="order-tabs nav custom-tabs nav-tabs d-flex">

              <!-- 1 -->
              <? 
              $step = 1;
              if(!canDoOperation('order_page')){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step || $order->last_step == false){  $tab_class = 'last-step'; }
                ?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#order" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                    <span class="d-none d-sm-block">Order</span>
                  </a>
                </li>
              <!-- 2 -->
              <?//var_dump($user->role);?>
              <? 
                $tab_class = '';
                $step = 2;
              if(canDoOperation('formation_order') ){
                 if(!($user->role == '2' && $order->to_engineer == true) && !$user->role == '7'){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                  <li class="nav-item text-center <?=$tab_class;?>">
                    <a href="#formation-order" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                      <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                      <span class="d-none d-sm-block">Formation of an order</span>
                    </a>
                  </li>
              <!-- 3 -->
              <? 
                $tab_class = '';
                $step = 3;
              if(canDoOperation('provider_offers') ){
                 if(!($user->role == '3' && $order->to_control == true) && !$user->role == '7'){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#provider-answers" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Supplier responses</span>
                  </a>
                </li>
              <!-- 4 -->
              <? 
                $tab_class = '';
                $step = 4;
              if(canDoOperation('offer_choose') ){
                if(!($user->role == '1' && $order->to_manager == true) && !$user->role == '7'){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                  <li class="nav-item text-center <?=$tab_class;?>">
                    <a href="#offer-formation" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                      <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                      <span class="d-none d-sm-block">Formation an offer</span>
                    </a>
                  </li>
              <!-- 5 -->
              <? 
                $tab_class = '';
                $step = 5;
              if(canDoOperation('logistics') ){
                 if(!($user->role == '4' && $order->to_logist == true) && !$user->role == '7'){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                  <li class="nav-item text-center <?=$tab_class;?>">
                    <a href="#logistics" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                      <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                      <span class="d-none d-sm-block">Logistics</span>
                    </a>
                  </li>
              <!-- 6 -->
              <? 
                $tab_class = '';
                $step = 6;
              if(canDoOperation('account_formation') ){
                 if(!($user->role == '1' && $order->to_manager == true) && !$user->role == '7'){
                  $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#account-formation" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Formation an account</span>
                  </a>
                </li>
              <!-- 7 -->
              <? 
                $tab_class = '';
                $step = 7;
              if(!canDoOperation('receipt_of_payment')){
                    $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }
                ?>
                  <li class="nav-item text-center <?=$tab_class;?>">
                    <a href="#receipt-of-payment" data-toggle="tab" aria-expanded="false"
                       class="border-round-none nav-link">
                      <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                      <span class="d-none d-sm-block">Receipt of payment</span>
                    </a>
                  </li>
              <!-- 8 -->
              <? 
                $tab_class = '';
                $step = 8;
              if(canDoOperation('purchase_order')){
                if((!$user->role == '1' && $order->to_manager == true) && !$user->role == '7'){
                    $tab_class = 'no-active';
                  }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#purchase-order" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Purchase order</span>
                  </a>
                </li>
              <!-- 9 -->
              <? 
                $tab_class = '';
                $step = 9;
              if(canDoOperation('provider_invoices') ){
                if((!$user->role == '3' && $order->purchase_order == true) && !$user->role == '7'){
                    $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#provider-invoices" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Supplier accounts</span>
                  </a>
                </li>
              <!-- 10 -->
              <? 
                $tab_class = '';
                $step = 10;
              if(!canDoOperation('accountant_payment')){
                    $tab_class = 'no-active';
              }elseif($order->last_step == $step){  $tab_class = 'last-step'; }
              ?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#accountant-payment" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Payment of accounts</span>
                  </a>
                </li>
              <!-- 11 -->
              <? 
                $tab_class = '';
                $step = 11;
              if(canDoOperation('supply_request')){
                if(!($user->role == '3' && $order->purchase_order == true) && !$user->role == '7'){
                    $tab_class = 'no-active';
                  }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#supply-request" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Request for delivery</span>
                  </a>
                </li>
              <!-- 12 -->
              <? 
                $tab_class = '';
                $step = 12;
              if(!canDoOperation('product_comings')){
                    $tab_class = 'no-active';
              }elseif($order->last_step == $step){  $tab_class = 'last-step'; }
              ?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#product-comings" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Receipt of the goods</span>
                  </a>
                </li>
              <!-- 13 -->
               <? 
                $tab_class = '';
                $step = 13;
               if(canDoOperation('qc')){
                if((!$user->role == '3' && $order->purchase_order == true) && !$user->role == '7'){
                    $tab_class = 'no-active';
                }elseif($order->last_step == $step){  $tab_class = 'last-step'; }}else{  $tab_class = 'no-active'; }?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#qc" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">QC</span>
                  </a>
                </li>
              <!-- 14 -->
              <? 
                $tab_class = '';
                $step = 14;
              if(!canDoOperation('total')){
                $tab_class = 'no-active';
              }elseif($order->last_step == $step){  $tab_class = 'last-step'; }
              ?>
                <li class="nav-item text-center <?=$tab_class;?>">
                  <a href="#total" data-toggle="tab" aria-expanded="false"
                     class="border-round-none nav-link">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Result</span>
                  </a>
                </li>
            </ul>

            <div class="order-tabs-content tab-content border-0 p-0 pt-3">
              
              <!-- ****************************** 1-->
              <? 
              // if(canDoOperation('order_page') ){
                ?>
                <div role="tabpanel" class="tab-pane fade" id="order">
                  <div class="row">
                    <div class='col-12 col-md-6'>
                      <div class='card'>
                    <div class='card-body'>
                      <h5>Manager's comment</h3>
                          <form method="post">
                        <div class='row'>
                          <div class='col-12'>
                              <div class="form-group">
                                <label class="col-form-label w-100">Comment:</label>
                                <textarea
                                        class="form-control"
                                        rows="5"
                                        name="comment"
                                        placeholder='Enter a comment'
                                        ><?=$order->comment;?></textarea>
                              </div>
                          </div>
                        </div>

                        <div class='row'>
                          <div class='col-md-6 pb-2 pb-md-0'>
                            <button
                                    type="submit"
                                    name="send-to-engineer"
                                    value="1"
                                    class="btn btn-block btn--md btn-<?=$order->to_engineer?'secondary':'success';?> waves-effect waves-light"><?=$order->to_engineer?'Sent to the engineer':'Send to the engineer';?>
                            </button>
                          </div>
                          <div class='col-md-6 pb-2 pb-md-0'>
                            <button
                                    type="submit"
                                    name="save-order"
                                    value="1"
                                    class="btn btn-block btn--md btn-primary waves-effect waves-light">Save
                            </button>
                          </div>
                        </div>

                          </form>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
              <? //} ?>              
              <!-- ****************************** 2 -->
              <? 
              // if(canDoOperation('formation_order') ){
                 //if(($user->role == '2' && $order->to_engineer == true) || $user->role == '7'){
                ?>
                  <? require 'order-tabs/formation-order.php';?>
              <? //}} ?>
              
              <!-- ****************************** 3 -->
              <? 
              // if(canDoOperation('provider_offers') ){
                 //if(($user->role == '3' && $order->to_control == true) || $user->role == '7'){
                ?>
                 <? require 'order-tabs/provider-answers.php';?>
              <? //}} ?>
              
              <!-- ****************************** 4 -->
              <? 
              // if(canDoOperation('offer_choose') ){
                //if(($user->role == '1' && $order->to_manager == true) || $user->role == '7'){
                ?>
              <? require 'order-tabs/offer-formation.php';?>
              <? //}} ?>
              
              <!-- ****************************** 5 -->
              <? 
              // if(canDoOperation('logistics') ){
                 //if(($user->role == '4' && $order->to_logist == true) || $user->role == '7'){
                ?>
                  <? require 'order-tabs/logistics.php';?>
              <? //}} ?>        
              
              <!-- ****************************** 6 -->
              <? 
              // if(canDoOperation('account_formation') ){
                 //if(($user->role == '1' && $order->to_manager == true) || $user->role == '7'){
                ?>
              <? require 'order-tabs/account-formation.php';?>
              <? //}} ?>        
              
              
              <!-- ******************************  7-->
              <? 
              // if(canDoOperation('receipt_of_payment') ){
                ?>
                <? require 'order-tabs/receipt-of-payment.php';?>
              <? //} ?>        
              
              <!-- ****************************** 8 -->
              <? 
              // if(canDoOperation('purchase_order') ){
                 //if(($user->role == '1' && $order->to_manager == true) || $user->role == '7'){
                ?>
                <? require 'order-tabs/purchase-order.php';?>
              <? //}} ?>        
              
              <!-- ****************************** 9-->
              <? 
              // if(canDoOperation('provider_invoices')){
                //if(($user->role == '3' && $order->purchase_order == true) || $user->role == '7'){?>
                <? require 'order-tabs/provider-invoices.php';?>
              <? //}} ?>        
              
              <!-- ****************************** 10-->
              <? 
              // if(canDoOperation('accountant_payment') ){
                ?>
                <? require 'order-tabs/accountant-payment.php';?>
              <? //} ?>        
              
              <!-- ****************************** 11 -->
              <? 
              // if(canDoOperation('supply_request')){
                //if(($user->role == '3' && $order->purchase_order == true) || $user->role == '7'){?>
                  <? require 'order-tabs/supply-request.php';?>
              <? //}} ?>
              
              <!-- ****************************** 12 -->
              <? 
              // if(canDoOperation('product_comings') ){
                ?>
              <? require 'order-tabs/product-comings.php';?>
              <? //} ?>
              

              <!-- ****************************** 13 -->
              <? 
              // if(canDoOperation('qc')){
                //if(($user->role == '3' && $order->purchase_order == true) || $user->role == '7'){?>
                  <? require 'order-tabs/qc.php';?>
              <? //}} ?>
              

              <!-- ****************************** 14 -->
              <? 
              // if(canDoOperation('total') ){
                ?>
              <? require 'order-tabs/total.php';?>
              <? //} ?>
              
            </div>
          </div>
        </div>




      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delete order
            </div>
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layouts/footer_view'); ?>
			
