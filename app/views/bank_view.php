<?php $this->load->view('layouts/header_view'); ?>

  <div class="content-page bank-page">
    <div class="content">
      <div class="container-fluid mt-2 mt-md-0">
        <div class="row pb-3 mb-5">
          <div class='col-md-6'>
            <div class='row'>
              <div class="col">
                <h4 class="page-title">Your account</h4>
              </div>
              <div class='col'>
                <select class="form-control">
                  <option selected="$">Dollar account $</option>
                  <option value="RU">Rouble account RU</option>
                </select>
              </div>
              <div class='col-12 pt-3'>
                <span>Current account: 32 000$</span>
              </div>
            </div>
          </div>
          <div class='col-md-3 offset-md-3 mt-3 mt-md-0'>
            <button type="button"
                    data-toggle="modal"
                    data-target="#currency-trans"
                    class="btn btn-block btn--md btn-primary waves-effect waves-light add-provider-btn">Transfer currency
            </button>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <h4 class="page-title">Bank account statement</h4>
          </div>
          <div class='col-md-3 mb-2 mb-md-0'>
            <button type="button"
                    class="add-arrival btn btn-block btn--md btn-success waves-effect waves-light d-none">Add a receipt
            </button>
          </div>
          <div class='col-md-3'>
            <button type="button"
                    class="add-expense btn btn-block btn--md btn-success waves-effect waves-light d-none">Add expense
            </button>
          </div>
          <div class='col-12 pt-4'>
            <div class='row'>
              <div class='col-md-2'>
                <h5>filter:</h5>
              </div>
              <div class='col-md-3 offset-md-6'>
                <span id='date-pick'></span>
              </div>
              <div class='col-md-1 mt-1 mt-md-0'>
                <button id='reset-all-table-bank' class="btn-icon btn btn-block btn-secondary waves-effect">
                  Reset
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="bank-statement-table"
                     class="table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>Date</th>
                  <th>Receipt</th>
                  <th>Expense</th>
                  <th>Balance</th>
                  <th>Comment</th>
                </tr>
                </thead>
                <tbody>


                  <? $counter = 0;?>
              <? if (!empty($paymentin)) {
                foreach ($paymentin as $key => $receipt) { $counter++; ?>
                <tr id="<?=$counter;?>">
                  <td>
                    <span class="tabledit-span tabledit-identifier"><?=$counter;?></span>
                    <input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="<?=$counter;?>" disabled="">
                  </td>
                  <td class="td-date tabledit-view-mode">
                    <span class="tabledit-span" style=""><?=$receipt->date;?></span>
                    <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                           type="text" name="date" readonly="" disabled="" style="display: none;" value='<?=$receipt->date;?>'>
                  </td>
                  <td class=""><?=$receipt->sum;?></td>
                  <td></td>
                  <td></td>
                  <td>
                    <span class="invoice-num"></span>
                    <button class="btn select-statement btn-block btn-primary d-none"
                            data-target="#journal-invoices-modal" data-toggle="modal">Choose receipt
                    </button>
                  </td>
                </tr>

                    <?}
                  }?>

              <? if (!empty($paymentouts)) {
                foreach ($paymentouts as $key => $invoice) { $counter++;?>
                  
                <tr id="<?=$counter;?>">
                  <td>
                    <span class="tabledit-span tabledit-identifier"><?=$counter;?></span>
                    <input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="<?=$counter;?>" disabled=""></td>
                  <td class="td-date tabledit-view-mode">
                    <span class="tabledit-span" style=""><?=$invoice->paymentout_date;?></span>
                    <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                           type="text" name="date" readonly="" disabled="" style="display: none;" value='<?=$invoice->paymentout_date;?>'>
                  </td>
                  <td></td>
                  <td class="tabledit-view-mode">
                    <span class="tabledit-span" style=""><?=$invoice->paymentout;?></span>
                    <input class="tabledit-input form-control input-sm" type="text" name="consumption" value="" style="display: none;" disabled="">
                  </td>
                  <td></td>
                  <td>
                    <span class="invoice-num">№ Account: <?=$invoice->invoicein;?></span>
                    <button class="btn select-statement btn-block btn-primary d-none" data-target="#add-expense-modal" data-toggle="modal">Select an expense</button>
                  </td>
                </tr>

                    <?}
                  }?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade"
     id='add-expense-modal'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Account journal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='row'>
          <div class='col-12 pt-2'>
            <table id="invoice-journal-modal"
                   class="table-hover table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
              <thead>
              <tr>
                <th>№ n\n</th>
                <th>№ Account</th>
                <th>Total sum</th>
              </tr>
              </thead>
              <tbody>
              <tr data-toggle='modal' data-target='#account-modal'>
                <td>1</td>
                <td>030293</td>
                <td>10 000</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade"
     id='account-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Account journal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='row'>
          <div class='col-6'>
            <h4>№ account: 030293</h4>
          </div>
          <div class='col-6 text-right'>
            <h4>Total sum: 10 000</h4>
          </div>
          <div class='col-12 pt-2'>
            <table id="account-modal-table"
                   class="table w-100 table-bordered">
              <thead>
              <tr>
                <th>№ n\n</th>
                <th>№ application</th>
                <th></th>
                <th>Component name</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Sum</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>1</td>
                <td>1</td>
                <td>
                  <div class="checkbox checkbox-single">
                    <input type="checkbox" id="singleCheckbox1" value="option1" aria-label="Single checkbox One">
                    <label for="singleCheckbox1"></label>
                  </div>
                </td>
                <td>Capacitor ceramic</td>
                <td>500</td>
                <td>10</td>
                <td>5000</td>
              </tr>
              <tr>
                <td>3</td>
                <td>1</td>
                <td>
                  <div class="checkbox checkbox-single">
                    <input type="checkbox" id="singleCheckbox2" value="option1" aria-label="Single checkbox One">
                    <label for="singleCheckbox2"></label>
                  </div>
                </td>
                <td>Capacitor ceramic</td>
                <td>500</td>
                <td>10</td>
                <td>5000</td>
              </tr>
              <tr>
                <td>3</td>
                <td>1</td>
                <td>
                  <div class="checkbox checkbox-single">
                    <input type="checkbox" id="singleCheckbox3" value="option1" aria-label="Single checkbox One">
                    <label for="singleCheckbox3"></label>
                  </div>
                </td>
                <td>Capacitor ceramic</td>
                <td>500</td>
                <td>10</td>
                <td>5000</td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class='col-12'>
            <div class='row'>
              <div class='col-6'>
                <button type="button" class="select-all btn btn-block btn-success waves-effect width-md waves-light">
                  Выбрать всё
                </button>
              </div>
              <div class='col-3'>
                <button type="button" data-dismiss="modal"
                        class="btn btn-block btn-primary waves-effect width-md waves-light">Back
                </button>
              </div>
              <div class='col-3'>
                <button type="button" class="btn btn-block btn-success waves-effect width-md waves-light">Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade"
     id='journal-invoices-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Application log</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <table id="bank-journal-invoices-table"
               class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
          <thead>
          <tr>
            <td></td>
            <th>№ n\n</th>
            <th>№ application</th>
            <th>Date</th>
            <th>Client</th>
            <th>Category</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <div class="checkbox checkbox-single">
                <input type="checkbox" id="singleCheckbox5" value="option1" aria-label="Single checkbox One">
                <label for="singleCheckbox5"></label>
              </div>
            </td>
            <td>1</td>
            <td>1</td>
            <td class="td-date tabledit-view-mode">
              <span class="tabledit-span" style="">28.02.2021</span>
              <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                     type="text" name="date" readonly="" disabled="" style="display: none;" value='28.02.2021'>
            </td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr>
            <td>
              <div class="checkbox checkbox-single">
                <input type="checkbox" id="singleCheckbox6" value="option1" aria-label="Single checkbox One">
                <label for="singleCheckbox6"></label>
              </div>
            </td>
            <td>1</td>
            <td>1</td>
            <td class="td-date tabledit-view-mode">
              <span class="tabledit-span" style="">28.02.2021</span>
              <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                     type="text" name="date" readonly="" disabled="" style="display: none;" value='28.02.2021'>
            </td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr>
            <td>
              <div class="checkbox checkbox-single">
                <input type="checkbox" id="singleCheckbox7" value="option1" aria-label="Single checkbox One">
                <label for="singleCheckbox7"></label>
              </div>
            </td>
            <td>1</td>
            <td>1</td>
            <td class="td-date tabledit-view-mode">
              <span class="tabledit-span" style="">28.02.2021</span>
              <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                     type="text" name="date" readonly="" disabled="" style="display: none;" value='28.02.2021'>
            </td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          </tbody>
        </table>
        <button type="button" class="btn btn-block btn-success waves-effect width-md waves-light">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg"
     id='currency-trans'
     tabindex="-1"
     role="dialog"
     style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Conversion of currency</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='row'>
          <div class='col-12 pt-2'>
            The value of the US dollar against the Chinese yuan and the Chinese yuan against the US dollar
            <form action=''>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">USD</label>
                  <input type="text" name="USD" class="form-control" placeholder="USD">
                </div>
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">CNY</label>
                  <input type="text" name="CNY" class="form-control" placeholder="CNY">
                </div>
              </div>
              <div class='form-row d-flex align-items-end'>
                <div class='col-12'>
                  <label class="col-form-label w-100">Attach a supporting document
:</label>
                  <input type="file" name="files[]" id="filter_input" multiple="multiple">
                  <button type="button"
                          class="add-expense btn btn-block btn--md btn-success waves-effect waves-light">Transfer USD in
                    CNY
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
