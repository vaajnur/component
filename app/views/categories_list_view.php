<?php $this->load->view('layouts/header_view'); ?>

  <div class="content-page category-list__page">
    <div class="content">
      <div class="container-fluid">

        <div class="row pb-3">
          <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-center">
              <h3 class="page-title">Categories</h3>
            </div>
          </div>
		
			<div class="col-2 pl-0">
			    <button class="btn btn-lg btn-dark mb-2" data-toggle='modal' data-target='#categor' type="button">Add&nbsp;a&nbsp;category</button>
			    <!-- <button class="btn btn-lg btn-dark" data-toggle='modal' data-target='#categor_edit' type="button">Edit&nbsp;Categories</button> -->
			</div>


		</div>



        <div class="row pb-3">
          <div class="col-11">
			<table class="table borderless categors-list ml-4">
				<thead>
					<tr>
						<th>id</th>
						<th class="text-left">name</th>
					</tr>
				</thead>
				<tbody>
					<? if (!empty($categories)) {
						foreach ($categories as $key => $categor) {?>
					
							<tr>
								<td><?=$categor->id;?></td>
								<td class="text-left"><?=$categor->name;?></td>
							</tr>
						<?}
					}?>
				</tbody>
			</table>
          </div>
		</div>


    </div>
  </div>
</div>

<? require __DIR__.'/modals/edit-category-modal.php';?>
<? require __DIR__.'/modals/add-category-modal.php';?>

<?php $this->load->view('layouts/footer_view'); ?>
			
