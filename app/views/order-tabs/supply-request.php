<div role="tabpanel" class="tab-pane fade" id="supply-request">
  <div class='row align-items-center mb-3 text-right mr-2'>
    <div class='col-md-2 offset-md-6'>
      <span>Total sum:</span> <b><?=number_format($order->summary_price, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2'>
      <span>For payment:</span> <b><?=number_format($order->for_pay, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2'>
      <? //var_dump($order->paymentin_sum);?>
      <span>Paid for:</span> <b><?=$order->payed;?>% from <?=$order->prepayment;?>%</b>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="table-responsive scroll-table__block">
        <table id='supply-request-table' class="table table-centered mb-0">
          <thead>
          <tr>
            <th></th>
            <th>№ n\n</th>
            <th>Component name</th>
            <th>Provider name</th>
            <th>Status</th>
            <!-- <th>Уведомление</th> -->
            <th>Date of shipment</th>
            <th>Receipt</th>
            <th>Add</th>
          </tr>
          </thead>
          <tbody>
    <? if(!empty($order->components)){
                $counter = 0;
              foreach ($order->components as $key => $component) {?>
          <? if(!empty($component->offers)){
                foreach ($component->offers as  $offer) {
                  $offer->status = $offer->paymentout >= $offer->sum  && $offer->paymentout != false && $offer->sum != false ? 'Full' : ( $offer->paymentout > 0 ? 'Half' : 'No' )  ;
                  // проверка есть ли счет у данного предложения
                  if($offer->invoicein == false)continue ;
                  $counter++;
                  // f12($offer);
                  ?>
                  <tr>
                    <td><?=$offer->id;?></td>
                    <td><?=$counter;?></td>
                    <td><?=$component->name;?></td>
                    <td><?=$offer->provider;?></td>
                    <td><?=$offer->status;?></td>
                    <!-- <td>
                      <div class="checkbox checkbox-single pt-2">
                        <input type="checkbox" id="notification" value="1" aria-label="Single checkbox One" <?=$offer->shipment_notify;?>>
                        <label for="notification"></label>
                      </div>
                    </td> -->
                    <td class="td-date tabledit-view-mode class='min-width-130'">
                      <span class="tabledit-span" style=""><?=$offer->shipment_date;?></span>
                      <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                             type="text" name="consumption" readonly="" disabled="" style="display: none;"
                             value='<?=$offer->shipment_date;?>'>
                    </td>
                    <td>
                      <div class="checkbox checkbox-single pt-2">
                        <input type="checkbox" id="coming" value="1" aria-label="Single checkbox One" <?=$offer->supply_number?' checked ':'';?> readonly disabled="disabled">
                        <label for="coming"></label>
                      </div>
                    </td>
                    <td>
                      <button
                              class="btn btn-<?=$offer->supply_number?'secondary':'primary';?> add_goods_arrival"
                              data-toggle='modal'
                              data-target='#add-coming-modal'
                              data-component='<?=$component->name;?>'
                              data-offer='<?=json_encode($offer);?>'
                      ><?=$offer->supply_number?'Receipt added':'Receipt add';?></button>
                    </td>
                  </tr>

                      <?}}?>
              <?}}?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>



<div class="modal fade bs-example-modal-lg"
     id="add-coming-modal"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Receipt add</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <h4 class="add-coming-modal-component">....</h4>
        <form action="" method="post" name="" enctype="multipart/form-data">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">№ receipt:</label>
                <input type="text" name="supply_number" class="form-control supply_number" placeholder="Enter № receipt">
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Receipt date:</label>
                <input class="form-control input-date" placeholder="Select a date" type="text" name="supply_date">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Storage:</label>
                
                <select class="custom-select storage" name="storage">
                <? if (!empty($order->storages)) {
                  foreach ($order->storages as $key => $store) {?>
                    <option value="<?=$store->id;?>"><?=$store->name;?></option>
                  <?}
                }?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Attach a supporting document:</label>
                <div class="supply_document_added d-none">
                  <div class="jFiler-item-thumb">
                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                  </div>
                  <div class="doc-name"><a target="_blank" href=""></a></div>
                  <a data-offer="" data-doc="" class="del-supply-doc jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                </div>
                <input value="" type="file" name="supply_document[]" multiple="multiple" class="filter_input">
              </div>
            </div>
            <input type="hidden" name="id" value="" class="offer-id">
            <div class="form-row pt-3">
              <div class="col">
                <button type="submit" name="add-edit-supply" value="1" class="btn btn-block btn--md btn-success waves-effect waves-light">Save
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
