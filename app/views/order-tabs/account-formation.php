<div role="tabpanel" class="tab-pane fade" id="account-formation">
  <!-- form -->
  <form action="" class="margin_form" name="margin_form" method="post" enctype="">
  <div class='row align-items-end'>
    <div class='col-md-2'>
      <div class="form-group">
        <label class="col-form-label w-100">Logistics:</label>
        <input type="text" name='123' value="<?=$order->logistic_price;?>" class="form-control" placeholder="" readonly>
      </div>
    </div>
    <div class='col-md-2'>
      <div class="form-group">
        <label class="col-form-label w-100">Margin in %:</label>
        <input type="text" name='profit_margin' value="<?=$order->profit_margin;?>" class="form-control" placeholder="">
      </div>
    </div>

    <div class='col-md-2'>
      <div class="form-group">
        <button class="btn btn-success" type="submit" name="add_margin"><span class="fa fa-check"></span></button>
      </div>
    </div>

    <div class='col-md-1 offset-5 float-right'>
      <div class="form-group">
        <button class="btn btn-success apply_margin" type="button" name="apply_margin">Update</button>
      </div>
    </div>

    <!-- применить маржу -->
    <!-- <div class='col-md-2'>
      <div class="form-group">
        <div class="checkbox checkbox-single">
          <input data-id="" type="checkbox" name="profit_margin_applied" id="profit_margin_applied" value="1" aria-label="" <?//=$order->profit_margin_applied>0?' checked ':'';?>>
          <label for="profit_margin_applied"></label>
        </div>
      </div>
    </div> -->


    <!-- <div class="form-check form-check-inline">
      <input class="form-check-input" type="checkbox" value="" id="profit_margin_yes" name="profit_margin_yes">
      <label class="form-check-label" for="profit_margin_yes">
        Apply
      </label>
    </div> -->


  </div>
  </form>


  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-centered mb-0 margin-table">
            <thead>
            <tr>
              <th>№ n\n:</th>
              <th>Сomponent name</th>
              <th>QTY</th>
              <th>Price</th>
              <th>Margin</th>
              <th>Sum</th>
            </tr>
            </thead>
            <tbody>
            <? if(!empty($order->components)){
              foreach ($order->components as $key => $component) {?>
            <!-- <tr>
              <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                <u><?=$component->name;?></u>
              </td>
              <td></td>
              <td></td>
              <td></td>
            </tr> -->
              <? if(!empty($component->offers)){
                foreach ($component->offers as  $offer) {
                  ?>
                  <tr>
                    <td>-</td>
                    <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                      <u><?=$component->name;?></u>
                    </td>
                    <td><?=$offer->quantity;?></td>
                    <td><?=$offer->price;?></td>
                    <td><?='<input data-id="'.$offer->id.'" type="text" name="profit_margin" value="'.($offer->profit_margin !== ''?$offer->profit_margin:$order->profit_margin).'" class="form-control offer-margin">';?></td>
                    <td><?=number_format($offer->sum, 0 , '.', ' ');?></td>
                  </tr>
                <?}}?>
              <?}
            }?>
            </tbody>
          <tfooter>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total:</b> <?=number_format($order->summary_price, 0 , '.', ' ');?></td>
          </tfooter>
        </table>
      </div>
    </div>
  </div>
  <!-- green btn -->
  <div class='row align-items-end'>
      
    <div class='col-md-3'>
    <form action="" method="post" class="prepayment_form" name="prepayment_form">
      <div class="form-group">
        <label class="col-form-label w-100">Prepayment in %:</label>
        <input type="text" name='prepayment' value="<?=$order->prepayment;?>" class="form-control" placeholder="Enter the prepayment">
      </div>
    </form>
    </div>
    <div class='col-md-3 pb-2 pb-md-0'>
      <div class="form-group">
        <label class="col-form-label w-100"></label>
        <button
                type="button"
                class="send-prepayment btn btn-block btn--md btn-success waves-effect waves-light">Send to accountant
        </button>
      </div>
    </div>
    <!-- на след этап  -->
    <div class='col-md-3 pb-2 offset-md-3 pb-md-0'>
      <div class="form-group">
        <label class="col-form-label w-100"></label>
        <button
                type="button"
                class="create-invoice btn btn-block btn--md btn-primary waves-effect waves-light">Сreate an invoice
        </button>
      </div>
    </div>
  </div>
</div>