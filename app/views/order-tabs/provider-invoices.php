<div role="tabpanel" class="tab-pane fade" id="provider-invoices">
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table id='purchase-invoice-table' class="table table-centered mb-0">
          <thead>
          <tr>
            <th>id</th>
            <th>№ n\n</th>
            <th>Name component</th>
            <th>Provider name</th>
            <th>Price</th>
            <th>QTY</th>
            <th>Sum</th>
            <th>Date</th>
            <th>Document</th>
            <th>Document date</th>
            <th>Invoice</th>
            <th>For payment</th>
          </tr>
          </thead>
          <tbody>
            <? if(!empty($order->components)){
              foreach ($order->components as $key => $component) {
                ?>
          <? if(!empty($component->offers)){
                foreach ($component->offers as $k=> $offer) {?>
                  <tr>
                    <td><?=$offer->id;?></td>
                    <td><?=$k==0?$key+1:'';?></td>
                    <td><?=$component->name;?></td>
                    <td><?=$offer->provider;?></td>
                    <td><?=$offer->price;?></td>
                    <td><?=$offer->quantity;?></td>
                    <td><?=$offer->sum;?></td>
                    <td><?=$offer->date1;?></td>
                    <td>
                      <form enctype="multipart/form-data" class="invoicein_form">
                        <? if($offer->document):?>
                        <div class="">
                          <div class="jFiler-item-thumb">
                            <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                          </div>
                          <?=$offer->document;?>
                          <a data-offer="<?=$offer->id;?>" data-doc="<?=$offer->document_id;?>" class="del-invoicein jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                        </div>
                        <? else:?>
                        <? endif;?>
                        <input type="file" value="" name="invoicein_doc[<?=$offer->id;?>]" class="filter_input-comment <?=$offer->document?' d-none ':''?>" multiple="multiple">
                      </form>
                    </td>
                    <td class='min-width-130'><?=$offer->invoicein_doc_date;?></td>
                    <td class='min-width-130'><?=$offer->invoicein;?></td>
                    <td>
                      <button data-offer-id="<?=$offer->id;?>" class='btn btn-primary submit_invoicein' <?=$offer->invoicein_sended?' disabled ':'';?>  >Send</button>
                    </td>
                  </tr>
                    <?}}?>
              <?}}?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>