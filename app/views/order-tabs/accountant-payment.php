<div role="tabpanel" class="tab-pane fade" id="accountant-payment">
  <div class='row align-items-center mb-3 text-right'>
    <div class='col-md-2 offset-md-6'>
      <span>Total amount:</span> <b><?=number_format($order->summary_price, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2'>
      <span>For payment:</span> <b><?=number_format($order->for_pay, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2'>
      <? //var_dump($order->paymentin_sum);?>
      <span>Paid for:</span> <b><?=$order->payed;?>% from <?=$order->prepayment;?>%</b>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table id='accountant-payment-table' class="table table-centered mb-0">
          <thead>
          <tr>
            <th></th>
            <th>№ n\n</th>
            <th>Date</th>
            <th>Expense</th>
            <th>Document</th>
            <th>Comment</th>
          </tr>
          </thead>
          <tbody>
            <?
            if(!empty($order->providerInvoiceSended)){
              foreach ($order->providerInvoiceSended as $key => $invoice) {?>

                <tr>
                  <td><?=$invoice->id;?></td>
                  <td><?=$key+1;?></td>
                  <td class="td-date tabledit-view-mode class='min-width-130'">
                    <span class="tabledit-span" style=""><?=$invoice->paymentout_date;?></span>
                    <input class="tabledit-input form-control input-sm input-date" placeholder="Select of date"
                           type="text" name="consumption" readonly="" disabled="" style="display: none;" value='<?=$invoice->paymentout_date;?>'>
                  </td>
                  <td class='min-width-130'><?=$invoice->paymentout;?></td>
                  
                  <td class="text-nowrap">
                  <? if ($invoice->document_path != false) {?>
                    <div class="download-doc">
                      <i class="mr-2 fas fa-file-pdf fa-2x text-black-50"></i>
                      <a target="_blank" href="<?=$invoice->document_path;?>" class="text-black-50"><u>Download!</u></a>
                    </div>
                  <?}?>
                  </td>
                  <td>№ account: <?=$invoice->invoicein;?></td>
                </tr>

              <?}
            }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>