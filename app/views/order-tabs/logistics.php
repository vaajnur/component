<div role="tabpanel" class="tab-pane fade" id="logistics">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-centered mb-0">
            <thead>
            <tr>
              <th>№ n\n</th>
              <th>Component name</th>
              <th>QTY</th>
              <th>Weight</th>
              <th>Volume</th>
            </tr>
            </thead>
            <tbody>
            <? if(!empty($order->components)){
              $counter = 0;
              foreach ($order->components as $key => $component) {
                $counter++;
                ?>
            <!-- <tr>
              <td><?=$component->id;?></td>
              <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                <u><?=$component->name;?></u>
              </td>
              <td></td>
              <td></td>
              <td></td>
            </tr> -->
              <? if(!empty($component->offers)){
                foreach ($component->offers as $k=> $offer) {?>
                  <tr>
                    <td class='max-width-90'><?=$k==0?$counter:'';?></td>
                    <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                      <u><?=$component->name;?></u>
                    </td>
                    <td><?=$offer->quantity;?></td>
                    <td><?=$offer->weight;?></td>
                    <td><?=$offer->volume;?></td>
                  </tr>
                <?}}else{$counter--;}?>

              <?}}?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- ========== form =========  -->
    <form action="" method="post" enctype="" name="logist_form">
        <div class='row align-items-end'>
            <div class='col-md-3'>
              <div class="form-group">
                <label class="col-form-label w-100">Delivery price:</label>
                <input type="text" name='price-order-logist' value="<?=$order->logistic_price;?>" class="form-control" placeholder="Enter the price">
              </div>
            </div>
            <div class='col-md-3 pb-2 pb-md-0'>
                <div class="form-group">
                  <label class="col-form-label w-100"></label>
                  <button
                      name="send-logistics-price"
                          type="submit"
                          value="1"
                          class="btn btn-block btn--md btn-primary waves-effect waves-light">Save
                  </button>
                </div>
            </div>
      </div>
    </form>
</div>