<div role="tabpanel" class="tab-pane fade" id="total">
  <ul class="nav custom-tabs nav-tabs d-flex justify-content-between">
    <li class="nav-item w-50 text-center">
      <a href="#shipment" data-toggle="tab" aria-expanded="false"
         class="rounded-left nav-link border-right-0 active">
        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
        <span class="d-none d-sm-block">Shipment</span>
      </a>
    </li>
    <li class="nav-item w-50 text-center">
      <a href="#deffect" data-toggle="tab" aria-expanded="false" class="rounded-right nav-link ">
        <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
        <span class="d-none d-sm-block">Defect</span>
      </a>
    </li>
  </ul>
  <div class="tab-content border-0 p-0 pt-3">
    <!-- ****************************** Отгрузка -->
    <div role="tabpanel" class="tab-pane first-tab-content fade active show" id="shipment">
      <div class="row align-items-center mb-3 text-right">
        <div class='col-md-3'>
          <button class='btn btn-success btn-block get-balance-tail' <?=$order->prepayment == false?'':'disabled';?>>Get the remainder</button>
        </div>
        <div class='col-md-3'>
          <button class='btn btn-success btn-block demand' <?=$order->demand == false?'':'';?> data-toggle='modal'
                                  data-target='#demand-doc'>Load to demand</button>
        </div>
        <div class="col-md-6">
          <span>Done:</span> <b><?=$order->payed;?>% from <?=$order->prepayment;?>%</b>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-centered mb-0">
              <thead>
              <tr>
                <th>№ n\n</th>
                <th>№ receipt</th>
                <th>Receipt date</th>
                <th>Component</th>
                <th>QTY</th>
                <th>Storage</th>
                <th>Document</th>
              </tr>
              </thead>
              <tbody>

        <? if(!empty($order->components)){
            foreach ($order->components as $key => $component) {?>
            <? if(!empty($component->offers)){
                  foreach ($component->offers as  $offer) {
                    // проверка на номер прихода
                    if($offer->supply_number == false  || $offer->send_to_control != 1)continue ;
                    // debug($offer);
                    ?>
                      <tr>
                        <td><?=$offer->id;?></td>
                        <td>
                          <u role='button'  class="arrival-info not_brak" data-toggle='modal' data-target='#info-modal' data-component="<?=$component->name;?>"  data-offer='<?=json_encode($offer);?>'><?=$offer->supply_number;?></u>
                        </td>
                        <td><?=$offer->supply_date;?></td>
                        <td><?=$component->name;?></td>
                        <td><?=$offer->not_brak;?></td>
                        <td><?=$offer->store_name;?></td>
                        <td>
                          <? if($offer->supply_document_path):?>
                          <div class="d-flex align-items-center mr-2">
                          <i class="mdi mdi-24px mdi-file"></i>
                          <a target="_blank" href="<?=$offer->supply_document_path;?>" class="text-muted text-underline">Download</a>
                        </div>
                        <? endif;?>
                      </td>
                      </tr>
                    <?}
                  }?>
              <?}
            }?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- ****************************** Брак -->
    <div role="tabpanel" class="tab-pane fade" id="deffect">
      <div class="row align-items-center mb-3 text-right">
        <div class='col-md-3'>
          <button class='btn btn-success btn-block get-balance-tail' <?=$order->prepayment == false?'':'disabled';?>>Get the remainder</button>
        </div>
        <div class='col-md-3'>
          <button class='btn btn-success btn-block demand' <?=$order->demand == false?'':'';?>  data-toggle='modal'
                                  data-target='#demand-doc'>Load to demand</button>
        </div>
        <div class="col-md-6">
          <span>Done:</span> <b><?=$order->payed;?>% from <?=$order->prepayment;?>%</b>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-centered mb-0">
              <thead>
              <tr>
                <th>№ п\п</th>
                <th>№ receipt</th>
                <th>Receipt date</th>
                <th>Component</th>
                <th>QTY</th>
                <th>Storage</th>
                <th>Document</th>
                <th>Provider</th>
              </tr>
              </thead>
              <tbody>

        <? if(!empty($order->components)){
            foreach ($order->components as $key => $component) {?>
            <? if(!empty($component->offers)){
                  foreach ($component->offers as  $offer) {
                    // проверка на номер прихода
                    if($offer->supply_number == false  || $offer->send_to_control != 1)continue ;
                    ?>
                      <tr>
                        <td><?=$offer->id;?></td>
                        <td>
                          <u role='button' class="arrival-info" data-toggle='modal' data-target='#info-modal' data-component="<?=$component->name;?>" data-offer='<?=json_encode($offer);?>'><?=$offer->supply_number;?></u>
                        </td>
                        <td><?=$offer->supply_date;?></td>
                        <td><?=$component->name;?></td>
                        <td><?=$offer->brak;?></td>
                        <td><?=$offer->store_name;?></td>
                        <td>
                          <? if($offer->supply_document_path):?>
                            <div class="d-flex align-items-center mr-2">
                            <i class="mdi mdi-24px mdi-file"></i>
                            <a target="_blank" href="<?=$offer->supply_document_path;?>" class="text-muted text-underline">Download</a>
                          </div>
                          <? endif;?>
                        </td>
                        <td>
                          <button
                                  class="btn btn-primary change-provider-modal"
                                  data-toggle='modal'
                                  data-component='<?=json_encode($component);?>'
                                  data-target='#change-provider-modal'
                          >Modify
                          </button>
                        </td>
                      </tr>
                    <?}
                  }?>
              <?}
            }?>


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- //////////////////////////////////// -->

<div class="modal fade"
     tabindex="-1"
     id='demand-doc'
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <h4 class="modal-title"> Shipment Document</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body d-flex">

        <form name="" action="" accept="" method="post" enctype="multipart/form-data">
            <div class="form-row">



              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Add a document:</label>
                <? if($order->demand_doc == true){?>
                <div class="demand_doc">
                  <div class="jFiler-item-thumb">
                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                  </div>
                  <div class="doc-name"><a target="_blank" href="<?=$order->demand_doc_path;?>"><?=$order->demand_doc;?></a></div>
                  <a data-offer="<?=$order->id;?>" data-doc="<?=$order->demand_doc_id;?>" data-type="demand_doc" class="del-doc jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                </div>
                <? }?>
                <input type="file" name="demand_document[<?=$order->id;?>]" class="filter_input">
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Document number</label>
                <input type="text" value="<?=$order->demand_number;?>" name='demand_number' class="form-control" placeholder="Document number">
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Document date</label>
                <input type="date" value="<?=$order->demand_date;?>" name='demand_date' class="form-control" placeholder="Document date">
              </div>
            </div>

            <div class='form-row'>
              <div class='col-12'>
                <button type='submit' name="send-demand-doc" value="1" class='btn btn-block btn-success'>Send</button>
              </div>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>

<div class="modal fade"
     tabindex="-1"
     id='info-modal'
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        № receipt<h4 class="modal-title"> ...</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body d-flex">
        <div class="form-group col-md-4">
          <label class="col-form-label w-100">Component:</label>
          <div class='mt-2 component-name'>Capacitor ceramic №2</div>
        </div>
        <div class="form-group col-md-4">
          <label class="col-form-label w-100">Document:</label>
          <div class="d-flex align-items-center">
            <i class="mdi mdi-24px mdi-file"></i>
            <a href="#" class="text-muted text-underline offer-doc">Download</a>
          </div>
        </div>
        <div class="form-group col-md-4">
          <label class="col-form-label w-100">Comment:</label>
          <div class='mt-2 offer-comment'>...</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade"
     id="change-provider-modal"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header align-items-center">
        <h4 class="modal-title">***</h4>
        <span>&nbsp; - &nbsp;</span>
        <h5  class="modal-title">***</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row align-items-center mb-3 text-right">
          <div class="col-md-6 offset-md-6">
            <span>Components selected:</span> <b>1000 from 800</b>
          </div>
        </div>
        <table class="table w-100 table-bordered">
          <thead>
          <tr>
            <td>
              <i class='mdi mdi-bookmark-check-outline'></i>
            </td>
            <th>Provider name:</th>
            <th>QTY:</th>
            <th>Price:</th>
            <th>Date:</th>
            <th>Weight:</th>
            <th>Volume:</th>
          </tr>
          </thead>
          <tbody>
              <!-- ... -->
          </tbody>
        </table>
        <div class="row">
          <div class="col-md-3">
            <button type="button" class="btn btn-block btn-success waves-effect width-md waves-light">Save
            </button>
          </div>
          <div class="col-md-3 mb-2 mb-md-0">
            <button type="button"
                    class="btn btn-block btn-primary waves-effect width-md waves-light">Generate an order
            </button>
          </div>
          <div class="col-md-3 mb-2 mb-md-0 offset-md-3">
            <button type="button"
                    class="btn btn-block btn-primary waves-effect width-md waves-light">Add an invoice
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




