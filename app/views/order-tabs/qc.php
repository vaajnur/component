<div role="tabpanel" class="tab-pane fade" id="qc">
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-centered mb-0">
          <thead>
          <tr>
            <th>№ n\n</th>
            <th>№ receipt</th>
            <th>Component</th>
            <th>QTY</th>
            <th>Control</th>
          </tr>
          </thead>
          <tbody>

      <? if(!empty($order->components)){
          foreach ($order->components as $key => $component) {?>
          <? if(!empty($component->offers)){
                foreach ($component->offers as  $offer) {
                  // проверка на номер прихода
                  if($offer->supply_number == false || $offer->send_to_qc != 1)continue ;
                  ?>
                    <tr>
                      <td><?=$offer->id;?></td>
                      <td>
                        <u role='button' data-component="<?=$component->name;?>" data-toggle='modal' class="qc-modal-btn" data-target='#add-comment-modal' data-offer='<?=json_encode($offer);?>'><?=$offer->supply_number;?></u>
                      </td>
                      <td><?=$component->name;?></td>
                      <td><?=$offer->quantity;?></td>
                      <td>
                        <button
                                data-id="<?=$offer->id;?>"
                                class="btn btn-<?=$offer->send_to_control?'secondary':'primary';?> send-to-control"
                        ><?=$offer->send_to_control?'Sent':'Send';?>
                        </button>
                      </td>
                    </tr>
                    <?}}?>
              <?}}?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>



<div class="modal fade bs-example-modal-xl"
     id='add-comment-modal'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myExtraLargeModalLabel"
     style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myExtraLargeModalLabel">Add a comment</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <h4>№ receipt <b class="supply_number">2390</b></h4>
        <form action="" method="post" enctype="multipart/form-data" name="">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Component:</label>
              <!-- <select class="custom-select">
                <option selected="1">Компонент 1</option>
                <option value="2">Компонент 2</option>
                <option value="3">Компонент 3</option>
                <option value="4">Компонент 4</option>
                <option value="5">Компонент 5</option>
                <option value="6">Компонент 6</option>
                <option value="7">Компонент 7</option>
                <option value="8">Компонент 8</option>
              </select> -->
              <input type="text" name="component" class="form-control component-name" value="" readonly="">
            </div>
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Total QTY:</label>
              <input type="number" class="form-control quantity" placeholder="Enter the QTY" readonly="">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">QTY of defective components:</label>
              <input type="number" name="brak" class="form-control" placeholder="Enter the QTY">
            </div>
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Attach a supporting document:</label>
              <input type="file" name="brak_files[]" id="filter_input" multiple="multiple">
              <!-- doc -->
              <div class="d-flex align-items-center mr-2 d-none">
                <i class="mdi mdi-24px mdi-file"></i>
                <a target="_blank" href="#" class="text-muted text-underline brak_doc">...</a>&nbsp;/&nbsp;
                <a href="#" class="text-muted text-underline brak_doc_del" data-type="brak_doc">Delete</a>
              </div>
            </div>
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Comment:</label>
              <input type="text" name="brak_comment" class="form-control" placeholder="Enter a comment">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">QTY of not defective components:</label>
              <input type="number" name="not_brak" class="form-control" placeholder="Enter the QTY">
            </div>
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Attach a supporting document:</label>
              <input type="file" name="not_brak_files[]" id="filter_input_2" multiple="multiple">
              <!-- doc -->
              <div class="d-flex align-items-center mr-2 d-none">
                <i class="mdi mdi-24px mdi-file"></i>
                <a target="_blank" href="#" class="text-muted text-underline not_brak_doc">...</a>&nbsp;/&nbsp;
                <a href="#" class="text-muted text-underline not_brak_doc_del" data-type="not_brak_doc">Delete</a>
              </div>
            </div>
            <div class="form-group col-md-4">
              <label class="col-form-label w-100">Comment:</label>
              <input type="text" name="not_brak_comment" class="form-control" placeholder="Enter a comment">
            </div>
          </div>
          <input type="hidden" name="id" class="offer-id" value="">
          <div class="form-row pt-3">
            <!-- <div class="col-md-6">
              <button type="submit" class="btn btn-block btn--md btn-success waves-effect waves-light">Add a component
              </button>
            </div> -->
            <div class="col-md-6">
              <button type="submit" class="btn btn-block btn--md btn-success waves-effect waves-light">Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>