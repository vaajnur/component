<div role="tabpanel" class="tab-pane fade" id="product-comings">
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table id='product-comings-table' class="table table-centered mb-0">
          <thead>
          <tr>
            <th>№ n\n</th>
            <th>№ receipt</th>
            <th>Receipt date</th>
            <th>Component</th>
            <th>QTY</th>
            <th>Storage</th>
            <th>Document</th>
            <th>QC</th>
          </tr>
          </thead>
          <tbody>

      <? if(!empty($order->components)){
          $counter = 0;
          foreach ($order->components as $key => $component) {?>
          <? if(!empty($component->offers)){
                foreach ($component->offers as  $offer) {
                  // проверка на номер прихода
                  if($offer->supply_number == false)continue ;
                  $counter++;
                  ?>
          <tr>
            <td><?=$counter?></td>
            <td><?=$offer->supply_number;?></td>
            <td><?=$offer->supply_date;?></td>
            <td><?=$component->name;?></td>
            <td><?=$offer->quantity;?></td>
            <td><?=$offer->store_name;?></td>
            <td>
              <div class='d-flex'>
                <? if($offer->supply_document_path):?>
                <div class='d-flex align-items-center mr-2'>
                  <i class='mdi mdi-24px mdi-file'></i>
                  <a target="_blank" href='<?=$offer->supply_document_path;?>' class='text-muted text-underline'>Download</a>
                </div>
                <? endif;?>
                <button
                        data-toggle='modal'
                        data-component='<?=$component->name;?>'
                        data-target='#edit-coming'
                        data-offer='<?=json_encode($offer);?>'
                        type="button"
                        class="btn mr-1 btn-primary rounded edit-coming-btn">
                  <span class="mdi mdi-pencil"></span>
                </button>
              </div>
            </td>
            <td>
              <button
                data-id="<?=$offer->id;?>"
                      class="send_to_qc btn btn-<?=$offer->send_to_qc?'secondary':'primary';?>"
              ><?=$offer->send_to_qc?'Sent':'Send';?>
              </button>
            </td>
          </tr>

                      <?}}?>
              <?}}?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade bs-example-modal-lg"
     id="edit-coming"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit receipt</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <h4 class="coming-offer">Capacitor ceramic #1</h4>
        <form name="" action="" method="post" enctype="">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label class="col-form-label w-100">№ receipt:</label>
              <input type="text" name="supply_number" class="form-control supply_number" value="" placeholder="Enter № receipt">
            </div>
            <div class="form-group col-md-6">
              <label class="col-form-label w-100">Receipt date:</label>
              <input class="form-control input-date supply_date" placeholder="Select a date" type="text" name="supply_date" readonly>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label class="col-form-label w-100">Storage:</label>
              <select class="custom-select store" name="storage">
                 <? if (!empty($order->storages)) {
                  foreach ($order->storages as $key => $store) {?>
                    <option value="<?=$store->id;?>"><?=$store->name;?></option>
                  <?}
                }?>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label class="col-form-label w-100">Attach a supporting document:</label>

              <div class="d-none proof_foto">
                  <div class="jFiler-item-thumb">
                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                  </div>
                  <div class="doc-name"><a target="_blank" href=""></a></div>
                  <a data-offer="" data-doc="" class="del-proof-foto jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                </div>
                <input value="" type="file" name="proof_foto[]" multiple="multiple" class="filter_input_proof">


              <div class='d-flex align-items-center mr-2'>
                <i class='mdi mdi-24px mdi-file'></i>
                <a target="_blank" href='#' class='text-muted text-underline supply_doc'>Download</a>&nbsp;/&nbsp;
                <a href='#' class='text-muted text-underline supply_doc_del'>Delete</a>
              </div>
            </div>
          </div>
          <input type="hidden" name="id" value="" class="offer-id">
          <div class="form-row pt-3">
            <div class="col">
              <button type="submit"  name="add-edit-supply" value="1" class="btn btn-block btn--md btn-success waves-effect waves-light">Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>