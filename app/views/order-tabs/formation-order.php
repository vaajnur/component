<div role="tabpanel" class="tab-pane fade" id="formation-order">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive scroll-table__block">
                <table id='engineer-table-order' class="table table-centered mb-0 table-editable-order">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>№</th>
                            <th>Name</th>
                            <th>Required QTY</th>
                            <th>Required QTY on <?=$order->pcb_count;?></th>
                            <th>Position on PCB</th>
                            <th>P/N</th>
                            <th>PDF/Link</th>
                            <th>Comment</th>
                            <th>Substitutes</th>
                            <th>Marking</th>
                            <th>Code</th>
                            <th>D/C</th>
                            <th>Body</th>
                            <th>MFG</th>
                            <th>QTY in the package</th>
                            <th>QTY purchases/piece</th>
                            <th>Price per unit($)</th>
                            <th>Sum($)</th>
                            <th>Order lead time (days)</th>
                            <th>Weight per 1 unit (kg)</th>
                            <th>T/p ($)</th>
                            <th class="d-none">component_id</th>
                        </tr>
                        <tr>
                            <th>id</th>
                            <th>#</th>
                            <th>品名</th>
                            <th>所需数量</th>
                            <th>所需数量</th>
                            <th>型号</th>
                            <th>注</th>
                            <th></th>
                            <th>代用品</th>
                            <th></th>
                            <th></th>
                            <th>品的数量</th>
                            <th>封装</th>
                            <th>厂家/牌子</th>
                            <th>包装的数量</th>
                            <th>采数量</th>
                            <th>价 ($)</th>
                            <th>价 ($)</th>
                            <th>Sum($)</th>
                            <th>交期(日)</th>
                            <th>片重量 (公斤)</th>
                            <th>目价($)</th>
                            <th  class="d-none"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if(!empty($order->components)){
              foreach ($order->components as $key => $component) {
                // debug($component);
                ?>
                        <tr>
                            <td><?=$component->id;?></td>
                            <td><?=$key+1;?></td>
                            <td class='min-width-130'><?=$component->name;?></td>
                            <td class='min-width-130'><?=$component->qty?></td>
                            <td class='min-width-130'><?=$component->qty*$order->pcb_count;?></td>
                            <td class='min-width-130 pcb-position_text' data-toggle='popover' data-placement='right' data-content=''><?=$component->position_on_pcb;?></td>
                            <td class='min-width-130'><?=$component->pn;?></td>
                            <td class='min-width-130'><?=$component->document;?></td>
                            <td class='min-width-130'><?=$component->remark;?></td>
                              <td class='min-width-130'><?=$component->substitutes;?></td>      
                              <td class='min-width-130'><?=$component->marking;?></td>
                              <td class='min-width-130'><?=$component->code;?></td>
                              <td class='min-width-130'><?=$component->dc;?></td>
                              <td class='min-width-130'><?=$component->body;?></td>
                              <td class='min-width-130'><?=$component->mfg;?></td>
                              <td class='min-width-130'><?=$component->qty_in_packing;?></td>
                              <td class='min-width-130'><?=$component->qty_to_buy_pcs;?></td>
                              <td class='min-width-130'><?=$component->unit_price;?></td>
                              <td class='min-width-130'><?=$component->sum;?></td>
                              <td class='min-width-130'><?=$component->lead_time_days;?></td>
                              <td class='min-width-130'><?=$component->weight_per_1_unit_kg;?></td>
                              <td class='min-width-130'><?=$component->tp;?></td>
                              <td class='min-width-130 d-none'><?=$component->component_id;?></td>
                        </tr>
                        <?}
            }else{?>
                        <tr>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66"></td>
                            <td height="66" class="d-none"></td>
                        </tr>
                        <?}?>
                    </tbody>
                </table>
            </div>
            <button class='add-row btn btn-success mt-2'>
                <i class='fas fa-plus'></i>
            </button>
        </div>
    </div>
    <!-- green btn -->
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 pb-2 pb-md-0 ml-3 mb-2">
                <button type="submit" name="to-control" value="1" class="btn btn-block btn--md btn-<?=$order->to_control?'secondary':'success';?> waves-effect waves-light">
                    <?=$order->to_control?'Sent to the control':'Send to the control';?>
                </button>
            </div>
        </div>
    </form>
</div>
<select name="name" id="components-select" class="tabledit-input form-control input-sm d-none">
    <? 
    if(!empty($components)){
      array_walk($components, function($item){ 
        echo "<option value='$item->id'>$item->name</option>";
      });
    }?>
</select>
<div id="components" class="d-none">
    <? 
    if(!empty($components)){
      $res = [];
      array_walk($components, function($item) use(&$res){ $res[$item->id] = $item->name;});
      echo json_encode($res);
    }?>
</div>

<? require dirname(__DIR__).'/modals/choose-component.php';?>
<? require dirname(__DIR__).'/modals/add-component-modal.php';?>
