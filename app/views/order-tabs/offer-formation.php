<div role="tabpanel" class="tab-pane fade" id="offer-formation">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id='offer-formation-table' class="table table-centered mb-0">
            <thead>
            <tr>
              <th>№ п\п</th>
              <th>Component name</th>
              <th>P/N</th>
              <th>Provider name</th>
              <th>QTY</th>
              <th>Price</th>
              <th>Date</th>
              <th>Weight</th>
              <th>Volume</th>
            </tr>
            </thead>
            <tbody>
        <? if(!empty($order->components)){
            foreach ($order->components as $key => $component) {?>
              <? 
              if(!empty($component->offers)){
                foreach ($component->offers as $k=> $offer) {
                  ?>
                  <tr>
                    <td> <?=($k==0?$key+1:'');?></td>
                    <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                      <u><?=$component->name;?></u>
                    </td>
                    <td><?=$component->pn;?></td>
                    <td><?=$offer->provider;?></td>
                    <td><?=$offer->quantity;?></td>
                    <td><?=$offer->price;?></td>
                    <td><?=$offer->period;?></td>
                    <td><?=$offer->weight;?></td>
                    <td><?=$offer->volume;?></td>
                  </tr>
                    <?}}else{?>
                    <tr>
                      <td><?=$key+1;?></td>
                      <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' class="chose-provider-btn1" data-target='#chose-provider-modal'>
                        <u><?=$component->name;?></u>
                      </td>
                      <td><?=$component->pn;?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  <?}?>


            <?}}?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <!-- g b -->
    <form action="" method="post">
    <div class='row'>
      <div class='col-md-3 pb-2 pb-md-0'>
        <button
                type="submit"
                value="1"
                name="send-to-manager"
                class="btn btn-block btn--md btn-<?=$order->to_manager?'secondary':'success';?> waves-effect waves-light"><?=$order->to_manager?'Sent to manager':'Form an offer';?>
        </button>
      </div>
      <div class='col-md-3 pb-2 pb-md-0'>
        <button
                type="submit"
                value="1"
                name="send-to-logist"
                class="btn btn-block btn--md btn-<?=$order->to_logist?'secondary':'success';?> waves-effect waves-light"><?=$order->to_logist?'Sent to the logistician':'Send to the logistician';?>
        </button>
      </div>
    </div>
    </form>


  </div>



  <div class="modal fade"
     id="chose-provider-modal"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header align-items-center">
        <h4 class="modal-title">...</h4>
        <span> &nbsp; - &nbsp; </span>
        <h5 class="modal-title">...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='mb-2 text-right'>Components selected: <b><i class="scored"></i> from <i class="quantity"></i></b></div>
        <table class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
          <thead>
          <tr>
            <td></td>
            <td>Selected</td>
            <th>Provider name:</th>
            <th>QTY:</th>
            <th>Price:</th>
            <th>Currency:</th>
            <th>Delivery date:</th>
            <th>Weight:</th>
            <th>Volume:</th>
          </tr>
          </thead>
          <tbody>

          <!-- <tr>
            <td>
              <div class="checkbox checkbox-single">
                <input type="checkbox" id="singleCheckbox1" value="option1" aria-label="Single checkbox One">
                <label for="singleCheckbox1"></label>
              </div>
            </td>
            <td>Наименование поставщика</td>
            <td>12</td>
            <td>10</td>
            <td>08.15.2020</td>
            <td>1600гр.</td>
            <td>360см3</td>
          </tr> -->

          </tbody>
        </table>
        <div class="row">
          <div class='col-md-3'>
            <button type="button" class="btn btn-block btn-primary waves-effect width-md waves-light btn-choose-offers">Save</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>