<div role="tabpanel" class="tab-pane fade" id="receipt-of-payment">
  <div class='row align-items-center mb-3 ml-2 mr-2'>
    <div class='col-md-3 pb-2 pb-md-0'>
      <button
              type="button"
              class="btn btn-block btn--md btn-primary waves-effect waves-light add-arrival">Add a receipt
      </button>
    </div>
    <div class='col-md-2 offset-md-3 text-right'>
      <span>Total sum:</span> <b><?=number_format($order->summary_price, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2 text-right'>
      <span>For payment:</span> <b><?=number_format($order->for_pay, 0 , '.', ' ');?></b>
    </div>
    <div class='col-md-2 text-right'>
      <? //var_dump($order->paymentin_sum);?>
      <span>Paid for:</span> <b><?=$order->payed;?>% from <?=$order->prepayment;?>%</b>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="table-responsive scroll-table__block">
        <table id='arrivals-table' class="table table-centered mb-0">
          <thead>
          <tr>
            <th></th>
            <th>№ n\n</th>
            <th>Date</th>
            <th>Supply</th>
          </tr>
          </thead>
          <tbody>
            <? if(!empty($order->paymentin)){
              $sum = 0;
            foreach($order->paymentin as $k =>  $pay){
              $sum += (float)$pay->sum;
              ?>
              <tr>
                <td class='max-width-1'><?=$pay->id;?></td>
                <td class='max-width-90'><?=$k+1;?></td>
                <td class="td-date tabledit-view-mode min-width-130">
                  <span class="tabledit-span" style=""><?=$pay->date;?></span>
                  <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date"
                         type="text" name="date" readonly="" disabled="" style="display: none;" value='<?=$pay->date;?>'>
                </td>
                <td class='min-width-130'><?=$pay->sum;?></td>
              </tr>
            <?}
            }else{?>
            <?}?>
          </tbody>
          <tfoot>
            <tr>
              <td></td>
              <td></td>
              <td colspan="1" class="text-right1"><b>Total:</b> <?=$sum;?></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>