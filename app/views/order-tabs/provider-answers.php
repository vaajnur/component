<div role="tabpanel" class="tab-pane fade" id="provider-answers">
  <div class="card">
    <div class="card-body">
      <div class="table-responsive scroll-table__block">
        <table id='' class="table table-centered mb-0">
          <thead>
          <tr>
            <th>№ n\n</th>
            <th>Component name</th>
            <th>P/N</th>
            <th>Required QTY</th>
            <th>Reached QTY</th>
          </tr>
          </thead>
          <tbody>
            <? if(!empty($order->components)){

              foreach ($order->components as $key => $component) {?>
          <tr>
            <td class='max-width-90'><?=$key+1;?></td>
            <td role='button' data-toggle='modal' data-component='<?=json_encode($component);?>' data-order='<?=json_encode($order);?>' data-target='#add-provider-modal' class="add-provider-btn1">
              <u><?=$component->name;?></u>
            </td>
            <td><?=$component->pn;?></td>
            <td><?=$component->qty*$order->pcb_count;?></td>
            <td><?=$component->scored;?></td>
          </tr>
            <?}
            }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <form action="" method="post">
        <div class="row">
          <div class="col-md-3 pb-2 pb-md-0 ml-3 mb-2">
            <button type="submit" name="send-to-manager" value="1" class="btn btn-block btn--md btn-<?=$order->to_manager?'secondary':'success';?> waves-effect waves-light"><?=$order->to_manager?'Sent to the manager':'Send to the manager';?></button>
          </div>
        </div>
  </form>
</div>


<div class="modal fade"
     id='add-provider-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header align-items-center">
        <h4 class="modal-title">{{quartz}}</h4>
        <div class="component_id"></div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='row align-items-center'>
          <div class='col-md-6'>
            <span>P/N <span>&nbsp; - &nbsp;</span></span>
            <h5 class='mb-3'>{{pn}}</h5>
          </div>
          <div class='col-md-6'>
            <div class='row'>
              <div class="col-md-6 text-right">
                <span>Reached QTY:</span> <b class="scored">...</b>
              </div>
              <div class="col-md-6 text-right">
                <span>Required QTY:</span> <b class="quantity">...</b>
              </div>
            </div>
          </div>
        </div>
        <table id="add-provider-modal_control-order-page"
               class="table w-100 table-bordered table-editable-order">
         <thead>
          <tr>
            <th>id</th>
            <th>Selected</th>
            <th>Provider name:</th>
            <th>QTY:</th>
            <th>Price:</th>
            <th>Currency:</th>
            <th>Delivery date:</th>
            <th>Weight:</th>
            <th>Volume:</th>
          </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
        <div class='row'>
          <div class="col-md-3 mb-2 mb-md-0">
            <button type="button" id="add-field_provider-modal"
                    class="btn btn-block btn-primary waves-effect width-md waves-light">Add provider
            </button>
          </div>
          <div class='col-md-3'>
            <button type="button" class="close-modal btn btn-block btn-success waves-effect width-md waves-light">Close
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>