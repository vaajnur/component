<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row align-items-center mb-4">
          <div class="col-md-6">
            <div class="page-title-box d-flex align-items-center justify-content-between">
              <h4 class="page-title">Expense log</h4>
            </div>
          </div>
          <div class='col-md-3 mb-2 mb-md-0'>
            <button type="button"
                    data-toggle="modal"
                    data-target="#add-expense"
                    class="btn btn-block btn--md btn-success waves-effect waves-light add-expense-btn"
            >Add expense
            </button>
          </div>
          <div class='col-md-3'>
            <button type="button"
                    data-toggle="modal"
                    data-target="#add-expense-deffect"
                    class="btn btn-block btn--md btn-success waves-effect waves-light add-expense-deffect-btn"
            >Add defect expense
            </button>
          </div>
        </div>
        <ul class="nav custom-tabs nav-tabs d-flex justify-content-between">
          <li class="nav-item w-50 text-center">
            <a href="#rate" data-toggle="tab" aria-expanded="false"
               class="rounded-left nav-link border-right-0 active">
              <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
              <span class="d-none d-sm-block">Expense</span>
            </a>
          </li>
          <li class="nav-item w-50 text-center">
            <a href="#rate-deffect" data-toggle="tab" aria-expanded="false" class="rounded-right nav-link ">
              <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
              <span class="d-none d-sm-block">Defect expense</span>
            </a>
          </li>
        </ul>
        <div class="tab-content border-0 p-0 pt-3">
          <!-- ******************** not BRAK ***************** -->
          <div role="tabpanel" class="tab-pane fade active show" id="rate">
            <div class="row">
              <div class='col-md-2'>
                <h5>Filter:</h5>
              </div>
              <div class='col-md-2 mb-2 mb-md-0 offset-md-3'>
                <span id='component-filter'></span>
              </div>
              <div class='col-md-2 mb-2 mb-md-0'>
                <span id='storage-filter'></span>
              </div>
              <div class='col-md-2 offset-0'>
                <span id='date-pick'></span>
              </div>
              <div class='col-md-1 mt-1 mt-md-0'>
                <button id='reset-all-table-expense' class="btn-icon btn btn-block btn-secondary waves-effect">
                  Reset
                </button>
              </div>
              <div class="col-12 mt-2">
                <div class="card-box table-responsive">
                  <table id="table-expense"
                         class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                    <thead>
                    <tr>
                      <th>№ n\n</th>
                      <th>№ receipt</th>
                      <th>Date receipt</th>
                      <th>№ order</th>
                      <th>Name component</th>
                      <th>QTY</th>
                      <th>Storage</th>
                    </thead>
                    <tbody>
                      <? if (!empty($expense_journal)) {
                        foreach ($expense_journal as $key => $expense) {?>
                          <tr>
                            <td><?=$key+1;?></td>
                            <td><?=$expense->supply_number;?></td>
                            <td class="td-date tabledit-view-mode">
                              <span class="tabledit-span" style=""><?=$expense->supply_date;?></span>
                              <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date" type="text" name="date" readonly="" style="display: none;" disabled="">
                            </td>
                            <td><?=$expense->order_num;?></td>
                            <td><?=$expense->component;?></td>
                            <td><?=$expense->not_brak;?></td>
                            <td><?=$expense->quantity;?></td>
                          </tr>
                        <?}
                      }?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ********************* BRAK **************** -->
          <div role="tabpanel" class="tab-pane fade" id="rate-deffect">
            <div class="row">
              <div class='col-md-2'>
                <h5>Filter:</h5>
              </div>
              <div class='col-md-2 mb-2 mb-md-0 offset-md-3'>
                <span id='component-filter2'></span>
              </div>
              <div class='col-md-2 mb-2 mb-md-0'>
                <span id='storage-filter2'></span>
              </div>
              <div class='col-md-2 offset-0'>
                <span id='date-pick2'></span>
              </div>
              <div class='col-md-1 mt-1 mt-md-0'>
                <button id='reset-all-table-expense-deffect' class="btn-icon btn btn-block btn-secondary waves-effect">
                  Reset
                </button>
              </div>
              <div class="col-12 mt-2">
                <div class="card-box table-responsive">
                  <table id="table-expense-deffect"
                         class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                    <thead>
                    <tr>
                      <th>№ n\n</th>
                      <th>№ receipt</th>
                      <th>Date receipt</th>
                      <th>№ order</th>
                      <th>Name component</th>
                      <th>QTY</th>
                      <th>Storage</th>
                    </thead>
                    <tbody>

                      <? if (!empty($expense_journal_brak)) {
                        foreach ($expense_journal_brak as $key => $expense) {?>
                          <tr>
                            <td><?=$key+10;?></td>
                            <td><?=$expense->supply_number;?></td>
                            <td class="td-date tabledit-view-mode">
                              <span class="tabledit-span" style=""><?=$expense->supply_date;?></span>
                              <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date" type="text" name="date" readonly="" style="display: none;" disabled="">
                            </td>
                            <td><?=$expense->order_num;?></td>
                            <td><?=$expense->component;?></td>
                            <td><?=$expense->brak;?></td>
                            <td><?=$expense->quantity;?></td>
                          </tr>
                        <?}
                      }?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ///////////////// 1 ///////////////// -->
<div class="modal fade bs-example-modal-lg"
     id="add-expense"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add expense</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form>
          <div class='row'>
            <div class="col-12">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">№ expense:</label>
                  <input type="number" class="form-control" placeholder="Enter № receipt">
                </div>
                <div class="form-group col-md-3">
                  <label class="col-form-label w-100">Date expense:</label>
                  <input class="form-control input-date" placeholder="Select a date" type="text" name="date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label class="col-form-label w-100">№ order:</label>
                  <select class="custom-select" name="order">
                    <? if (!empty($orders)) {
                      foreach ($orders as $key => $order) {?>
                          <option value="<?=$order->id;?>"><?=$order->order_num;?></option>
                      <?}
                    }?>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">Name component:</label>
                  <input type="text" class="form-control" placeholder="Capacitor ceramic" name="component">
                </div>
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">QTY:</label>
                  <input name="quantity" type="number" class="form-control" placeholder="123">
                </div>
              </div>
              <div class="form-row pt-3">
                <div class="col">
                  <button type="submit" name="add-new-paymentout" class="btn btn-block btn--md btn-primary waves-effect waves-light">Add
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- /////////////////////// 2 ////////////////// -->
<div class="modal fade bs-example-modal-lg"
     id="add-expense-deffect"
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add defect expense</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form>
          <div class='row'>
            <div class="col-12">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">№ expense:</label>
                  <input type="number" class="form-control" placeholder="Enter № receipt">
                </div>
                <div class="form-group col-md-3">
                  <label class="col-form-label w-100">Date expense:</label>
                  <input class="form-control input-date" placeholder="Select a date" type="text" name="date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label class="col-form-label w-100">№ order:</label>
                  <select class="custom-select" name="order">
                    <? if (!empty($orders)) {
                      foreach ($orders as $key => $order) {?>
                          <option value="<?=$order->id;?>"><?=$order->order_num;?></option>
                      <?}
                    }?>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">Name component:</label>
                  <input type="text" class="form-control" placeholder="Capacitor ceramic">
                </div>
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">QTY:</label>
                  <input name="count" type="number" class="form-control" placeholder="123">
                </div>
              </div>
              <div class="form-row pt-3">
                <div class="col">
                  <button type="submit" class="btn btn-block btn--md btn-primary waves-effect waves-light">Add
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
