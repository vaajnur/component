<? if (!empty($employee_custom_fields)) {
  foreach ($employee_custom_fields as $key => $cf) {?>
    <div class="row align-items-center form-group form-group-row border-bottom">
      <div class="col-11">
        <div class="row">
          <div class="form-group col-12 col-md-5">
            <div>
              <label>Custom name:</label>
              <input type="text" name="custom_f_name[<?=$cf->name;?>]" class="form-control" value="<?=$cf->name;?>">
            </div>
          </div>
          <div class="form-group col-10 col-md-5">
            <div>
              <label>Custom value:</label>
              <input type="text" name="custom_f_value[<?=$cf->name;?>]" class="form-control" value="<?=$cf->value;?>">
            </div>
          </div>
          <div class="d-flex align-items-center pt-2">
            <button type="button" class="btn mr-1 ml-1 btn-sm btn-primary active canceled-delete-field-btn" style="display: none; float: none;">
              <span class="mdi mdi-close"></span>
            </button>
            <button class="btn btn-icon btn-sm btn-danger ml-1 delete-field-btn">
              <i class="fas fa-trash-alt"></i>
            </button>
            <button   data-cf-id="<?=$cf->id;?>" type="button" class="confirm-delete-field-btn btn btn-sm rounded btn-danger delete-cfield-btn" style="display: none; float: none;">
              <span class="mdi mdi-check"></span>
            </button>
          </div>
        </div>
      </div>
    </div>
  <?}
}?>