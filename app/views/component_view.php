<?php $this->load->view('layouts/header_view'); ?>

Component

<div class="wrapper-category component-page" id="wrapper-category">


  <div class="row mt-4">
    <div class="col-md-12 mt-5 pt-2 pl-5 pr-5">
      <div class="heading">
        <h3>Component Page</h3>
      </div>
    </div>
    <? //debug($component);?>
  </div>


		<?if(!empty($mess)){?>
		<div class="row">
            <div class="col-12">
              <div class="alert alert-<?=$mess['type'];?>">
                <?=$mess['text'];?>
              </div>
            </div>
		</div>
          <? } ?>  
  
    <div class="row p-5">
      <div class="col-md-6 col-sm-12">
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Name:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->name;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Partnumber:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->pn;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Body:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->body;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Comment:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->comment;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Manufacturer:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->manufacturer;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Marking:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <label><?=$component->marking;?></label>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-md-6 col-sm-6">
            <label class="font-weight-bold">Add PDF:</label>
          </div>
          <div class="col-md-6 col-sm-6 ml-auto">
            <? 
             // debug($component);
            if($component->document_id==true):?>
            <div class="">
                <div class="jFiler-item-thumb">
                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                </div>
                <div class="doc-name"><a target="_blank" href="<?=$component->document_path;?>">
                        <?=$component->document_name;?></a></div>
               
            </div>
            <? endif;?>
            <!-- <img src="../../public/images/google-docs.png" alt="docs" /> -->
          </div>
        </div>
        
  
      <!-- common fields -->
        <div class="row pt-3">
          <div class="col-md-12">
              <h4 class="text-center">Common fields</h4>
              <hr>
          </div>
          <? if (!empty($component->custom_f)) {
            foreach ($component->custom_f as $key => $cf) {?>
                <div class="col-md-6 col-sm-6">
                  <label class="font-weight-bold"><?=$cf->name;?></label>
                </div>
                <div class="col-md-6 col-sm-6 ml-auto">
                  <label><?=$cf->value;?></label>
                </div>
            <?}
          }?>
        </div>


      </div>
      <div class="col-md-5 col-sm-12 align-self-end">
        <div class="row justify-content-end">
      
        <div class='col-md-7 col-sm-12 pt-2 pl-0 pr-0'>
                       <button type="submit" id=''   data-toggle='modal' data-target='#categor-add_component' name="" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Edit
                       </button>
                   </div>
                   <div class='col-md-7 col-sm-12 pt-4 pl-0 pr-0'>
                                  <button type="button" data-target="#confirm-delete" data-toggle="modal" id='' name="delete_component" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Delete
                                  </button>
                              </div>
                   
                   
                 </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delete component
            </div>
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                <a href="/delete-component/<?=$component->id;?>" class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<? require __DIR__.'/modals/edit-component-modal.php';?>

<?php $this->load->view('layouts/footer_view'); ?>
			
