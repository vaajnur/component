<table class="table table-borderless table-componet__categ-list" id="categor-components-list-name">
    <thead>
        <tr class="d-flex">
            <th class="col-lg-1 col-sm-1">Component Name</th>
            <th class="col-lg-1 col-sm-1">Price</th>
            <th class="col-lg-1 col-sm-1">Patrnumber</th>
            <th class="col-lg-1 col-sm-1">Comment</th>
            <th class="col-lg-1 col-sm-1">Body</th>
            <th class="col-lg-1 col-sm-1">Manufacturer</th>
            <th class="col-lg-1 col-sm-1">Marking</th>
            <th class="col-lg-1 col-sm-1"></th>
        </tr>
    </thead>
    <tbody>
        <? 
   // debug($category);
   if (!empty($category->components)) {
       foreach ($category->components as $key => $component) {?>
        <tr class="d-flex">
            <td class="col-lg-1 col-md-1 col-sm-1"><?="<a class='choose-component-enginer d-inline-block' href='#' data-component='".json_encode($component)."'>{$component->name}</a><br>";?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->price;?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->pn;?></td>
             <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->comment;?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->body;?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->manufacturer;?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->marking;?></td>
            <td class="col-lg-1 col-md-1 col-sm-1"><?="<a class='choose-component-enginer d-inline-block btn-component__enginer' href='#' data-component='".json_encode($component)."'>Select</a><br>";?></td>
        </tr>
        <?}
       }?>
    </tbody>
</table>