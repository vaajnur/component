<?php $this->load->view('layouts/header_view'); ?>

  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row align-items-center">
              <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Order log</h4>
                </div>
              </div>
              <? if(canDoOperation('order_page')){?>
              <div class='col-md-3 offset-md-3'>
                <a href='/create-order'>
                  <button
                          type="button"
                          class="btn btn-block btn--md btn-success waves-effect waves-light add-user-btn">Create order
                  </button>
                </a>
              </div>
              <? } ?>
            </div>
          </div>
        </div>
        <div class='row'>
         <div class='col-12'>
           <div class='row'>
             <div class='col-md-2'>
               <h5>Filter:</h5>
             </div>
             <div class='col-md-2 mb-1 mb-md-0 offset-md-3'>
               <span id='client-filter-select'></span>
             </div>
             <div class='col-md-2 mb-1 mb-md-0'>
               <span id='category-filter-select'></span>
             </div>
             <div class='col-md-2'>
               <span id='date-pick'></span>
             </div>
             <div class='col-md-1 mt-1 mt-md-0'>
               <button id='reset-all' class="btn-icon btn btn-block btn-secondary waves-effect">
                 Reset
               </button>
             </div>
           </div>
         </div>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-orders-journal"
                     class="table-hover table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>№ orders</th>
                  <th>Name</th>
                  <th>Data</th>
                  <th>Client</th>
                  <th>Category</th>
                  <th>Quotation</th>
                  <th>Payment from the client </th>
                  <th>Payment to suppliers </th>
                  <th>Delivery to the storage </th>
                  <th>Shipment to the client</th>
                </tr>
                </thead>
                <tbody>
                <? 
                // debug($this->session->get('logined')->code);
                $user_role = $this->session->get('logined')->code;
                foreach($orders as $k => $order){
                  if(  
                    ($user_role == 'engineer' && $order->to_engineer == false) ||
                    ($user_role == 'control' && $order->to_control == false) ||
                    ($user_role == 'logistician' && $order->to_logist == false) ||
                    ($user_role == 'manager' && $order->to_manager == false) 
                    )
                    continue;
                  ?>
                  <tr class='clickable-row' data-href='/order/<?=$order->id;?>'>
                    <td><?=$k+1;?></td>
                    <td><?=$order->order_num;?></td>
                    <td><?=$order->name;?></td>
                    <td><?=$order->date;?></td>
                    <td><?=$order->customer;?></td>
                    <td><?=$order->type=='1'?'Simple':'Contract';?></td>
                    <td><?=$order->quota_status;?></td>
                    <td><?=$order->status_payin;?></td>
                    <td><?=$order->status_payout;?></td>
                    <td><?=$order->supply_status;?></td>
                    <td><?=$order->demand_status;?></td>
                  </tr>
                <?}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php $this->load->view('layouts/footer_view'); ?>
			
