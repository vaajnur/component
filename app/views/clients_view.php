<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row align-items-center">
              <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Client</h4>
                </div>
              </div>

              <div class="col-md-3 offset-md-3">
                <a href="" onclick="return false;">
                  <button
                          type="button"
                          data-toggle='modal'
                          data-target='.modal-add-client'
                          class="btn btn-block btn--md btn-success waves-effect waves-light add-client-btn">Add a client
                  </button>
                </a>
              </div>
            </div>
          </div>
          <?if(!empty($mess)){?>
            <div class="col-12">
              <div class="alert alert-<?=$mess['type'];?>">
                <?=$mess['text'];?>
              </div>
            </div>
          <? } ?>             
        </div>
        <div class='row'>
          <div class='col-md-2'>
            <h5>filter:</h5>
          </div>
          <div class='col-md-2  mb-1 mb-dm-0 offset-md-7'>
            <span id='client-filter-select'></span>
          </div>
          <div class='col-md-1 mt-1 mt-md-0'>
            <button id='reset-all-clients' class="btn-icon btn btn-block btn-secondary waves-effect">
              Reset
            </button>
          </div>
        </div>
        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-clients"
                     class="table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                <thead>
                <tr>
                  <th>n/n</th>
                  <th>Client's name</th>
                  <th>QTY of transactions</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <? if (!empty($clients)) {
                    foreach ($clients as $key => $client) {?>
                  
                <tr id='<?=$client->id;?>'>
                  <td><?=$key+1;?></td>
                  <td><?=$client->full_name;?></td>
                  <td><?=$client->orders_count;?></td>
                  <td class='text-right text-nowrap'>
                    <button type="button" class="btn btn-sm btn-primary active canceled-change-btn"
                            style="float: none; display: none;">
                      <span class="mdi mdi-close"></span>
                    </button>
                    <a href='/page-client/<?=$client->id;?>'>
                      <button class='btn btn-icon btn-sm btn-primary ml-1'>
                        <i class='fas fa-pencil-alt'></i>
                      </button>
                    </a>
                    <button class='btn btn-icon btn-sm btn-danger ml-1 delete-item-btn'>
                      <i class='fas fa-trash-alt'></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-danger ml-1 delete-item-btn-confirm"
                            style="float: none; display: none;">
                      <span class="mdi mdi-check"></span>
                    </button>
                  </td>
                </tr>

                    <?}
                  }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal ADD CLIENT -->
<div class="modal fade bd-example-modal-lg modal-add-client"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="mySmallModalLabel">Add a client</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" method="post" name="" enctype="">
          <div class="row">
          <div class="col-md-6">
            <label class="col-form-label w-100">Client's name:</label>
            <input name='full_name' type="text" class="form-control" placeholder="Name" value="<?=$_POST['full_name'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Legal address:</label>
              <input name='legal_address' type="text" class="form-control" placeholder="Legal address:"  value="<?=$_POST['legal_address'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Postal address:</label>
              <input name='postal_address' type="text" class="form-control" placeholder="Postal address:"  value="<?=$_POST['postal_address'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Phone / Fax number:</label>
              <input name='phone_fax' type="text" class="form-control" placeholder="Phone / Fax number:"></div  value="<?=$_POST['phone_fax'];?>">
          <div class="col-md-6">
              <label class="col-form-label w-100">INN:</label>
              <input name='inn' type="text" class="form-control" placeholder="INN:"  value="<?=$_POST['inn'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">KPP:</label>
              <input name='kpp' type="text" class="form-control" placeholder="KPP:"  value="<?=$_POST['kpp'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">OGRN:</label>
              <input name='ogrn' type="text" class="form-control" placeholder="OGRN:"  value="<?=$_POST['ogrn'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Payment account:</label>
              <input name='payment_account' type="text" class="form-control" placeholder="Payment account:"  value="<?=$_POST['payment_account'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Correspondent account:</label>
              <input name='correspondent_account' type="text" class="form-control" placeholder="Correspondent account:"  value="<?=$_POST['correspondent_account'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">BIC of the bank:</label>
              <input name='bic_of_the_bank' type="text" class="form-control" placeholder="BIC of the bank:"  value="<?=$_POST['bic_of_the_bank'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Bank:</label>
              <input name='bank' type="text" class="form-control" placeholder="Bank:"  value="<?=$_POST['bank'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Full name of the Director:</label>
              <input name='fio_director' type="text" class="form-control" placeholder="Full name of the Director:"  value="<?=$_POST['fio_director'];?>">
          </div>
          <div class="col-md-6">
              <label class="col-form-label w-100">Email:</label>
              <input name='email' type="text" class="form-control" placeholder="Email:"  value="<?=$_POST['email'];?>">
          </div>

          <div class="col-md-12 pt-2">
            <button type="submit" name="send-new-client" value="1" class='btn btn-block btn-success'>Add</button>
            </button>
          </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
