<?php $this->load->view('layouts/header_view'); ?>
Category
<div class="wrapper">
    <div class="row col-lg-12 col-md-12 mt-4 nomencltr-categories">
        
        <? require __DIR__.'/layouts/sidebar.php';?>
        
        <section class="col-lg-9 col-md-8 mt-5 pt-2 pl-5 pr-5">
            <div class="heading">
                <h3>Nomenclature</h3>
            </div>
            
            
            <table class="table table-borderless table-componet__categ-list <?=canDoOperation('add_components')?'':' not_edit ';?>" id="category-components-list">
              <thead>
                <tr class="d-flex">
                  <!-- <th class="col-2">id</th> -->
                  <!-- <th class="col-lg-4 col-sm-6">Component Name</th> -->
                  <!-- <th class="col-lg-5 col-sm-3">Price</th> -->
                </tr>
              </thead>
              <tbody>
            <? 
            // debug($category);
            if (!empty($category->components)) {
                foreach ($category->components as $key => $component) {?>
                <tr  class="d-flex pt-1">
                  <td class="col-2"><?=$component->id;?></td>
                  <td class="col-lg-4 col-md-4 col-sm-3"><?=$component->name;?></td>
                  <td class="col-lg-5 col-md-4 col-sm-3"><?=$component->price;?></td>
                </tr>
                <?}
              }?>
              </tbody>
            </table>

        </section>
    </div>
</div>


<? require __DIR__.'/modals/add-category-modal.php';?>
<? require __DIR__.'/modals/edit-category-modal.php';?>


<?php $this->load->view('layouts/footer_view'); ?>