<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-md-9">
            <div class="row align-items-center">
              <div class="col-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Order №1</h4>
                </div>
              </div>
            </div>
          </div>
          <div class='col-md-3'>
            <button
                    type="button"
                    class="btn btn-block btn--md btn-success waves-effect waves-light">Edit a request
            </button>
            <button
                    type="button"
                    class="btn btn-block btn--md btn-danger waves-effect waves-light">Delete a request
            </button>
          </div>
        </div>
        <div class='row'>
          <div class="col-md-9">
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">Quotation #	</div>
              <div class="col-8"></div>
            </div>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">for assembly	</div>
              <div class="col-8"><span class='font-weight-bold'>Seller:</span>	RTKT Electronics </div>
            </div>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">Product purpose</div>
              <div class="col-8"><span class='font-weight-bold'>Покупатель:</span></div>
            </div>
            <br>
            <br>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">attn:	</div>
              <div class="col-8 font-weight-bold">attn:</div>
            </div>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">Estimated date of shipment:	 </div>
              <div class="col-8"><span class='font-weight-bold'>date:</span>	June 30, 2014</div>
            </div>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">Repeat: new order</div>
              <div class="col-8"></div>
            </div>
            <br>
            <br>
            <div class="row mb-4">
              <div class="col-4 font-weight-bold">Order: N638,	Modul IO24_v_10_0</div>
              <div class="col-8"><span class='font-weight-bold'>QTY:</span> 3000</div>
            </div>
          </div>
          <div class='col-md-3 mb-4 d-flex align-items-start justify-content-end flex-column'>
            <div class='font-weight-bold mb-3'>Logistic</div>
<!--            <div class='font-weight-bold mb-3'>Delivery time: </div>-->
            <div class='font-weight-bold'>Delivery price:	</div>
          </div>
        </div>

        <div class="row pt-3">
          <div class='col-12 mb-3'>
            <ul class="nav custom-tabs nav-tabs d-flex">
              <li class="nav-item text-center">
                <a href="#comment" data-toggle="tab" aria-expanded="false" class="border-round-none nav-link active">
                  <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                  <span class="d-none d-sm-block">Order</span>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12">
            <div class="tab-content border-0 p-0">
              <div role="tabpanel" class="tab-pane fade active show" id="comment">
                <div class='row'>
                  <div class='col-md-6'>
                    <form>
                      <div class="form-group">
                        <label class="col-form-label w-100">Comment:</label>
                        <textarea
                                class="form-control"
                                rows="5"
                                placeholder='Enter comment'
                                ></textarea>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class='row'>
          <div class='col-md-3 pb-2 pb-md-0'>
            <button
                    type="button"
                    class="btn btn-block btn--md btn-success waves-effect waves-light">Send to the engineer
            </button>
          </div>
          <div class='col-md-3 pb-2 pb-md-0'>
            <button
                    type="button"
                    class="btn btn-block btn--md btn-success waves-effect waves-light">Save
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade"
     id='add-provider-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Quartz</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <table id="add-provider-modal_control-order-page"
               class="table w-100 table-bordered">
          <thead>
          <tr>
            <th><i class='mdi mdi-sticker-check-outline'></i></th>
            <th>Supplier name:</th>
            <th>Price:</th>
            <th>Date:</th>
            <th>Weight:</th>
            <th>Volume:</th>
          </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
        <div class='row'>
          <div class="col-md-6 mb-2 mb-md-0">
            <button type="button" id="add-field_provider-modal"
                    class="btn btn-block btn-primary waves-effect width-md waves-light">add new shipper response
            </button>
          </div>
          <div class='col-md-6'>
            <button type="button" class="btn btn-block btn-success waves-effect width-md waves-light">Save
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
