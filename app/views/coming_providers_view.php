<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row align-items-center">
              <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Receipt log from provider</h4>
                </div>
              </div>
              <div class='col-md-3 offset-md-3'>
                <button
                        type="button"
                        data-toggle='modal'
                        data-target='#add-coming-modal'
                        class="btn btn-block btn--md btn-success waves-effect waves-light">Add receipt
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-2'>
            <h5>Filter:</h5>
          </div>
          <div class='col-md-2 mb-2 mb-md-0 offset-md-3'>
            <span id='component-filter'></span>
          </div>
          <div class='col-md-2 mb-2 mb-md-0'>
            <span id='storage-filter'></span>
          </div>
          <div class='col-md-2 offset-0'>
            <span id='date-pick'></span>
          </div>
          <div class='col-md-1 mt-1 mt-md-0'>
            <button id='reset-all-coming-provider' class="btn-icon btn btn-block btn-secondary waves-effect">
              Reset
            </button>
          </div>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="coming-providers-table"
                     class="table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>№ receipt</th>
                  <th>Date receipt</th>
                  <th>№ orders</th>
                  <th>Component name</th>
                  <th>QTY</th>
                  <th>Storage</th>
                  <th>Document</th>
                </tr>
                </thead>
                <tbody>
                  <? if (!empty($coming_providers)) {
                    foreach ($coming_providers as $key => $provider_com) {?>
                <tr>
                  <td><?=$key+1;?></td>
                  <td data-toggle='modal' data-target='#coming-modal' role='button' class='text-underline'><?=$provider_com->supply_number;?></td>
                  <td class="td-date tabledit-view-mode">
                    <span class="tabledit-span" style=""><?=$provider_com->supply_date;?></span>
                    <input class="tabledit-input form-control input-sm input-date" placeholder="Select a date" type="text" name="date" readonly="" style="display: none;" disabled="">
                  </td>
                  <td><?=$provider_com->order_num;?></td>
                  <td><?=$provider_com->component;?></td>
                  <td><?=$provider_com->quantity;?></td>
                  <td><?=$provider_com->storage;?></td>
                  <td class='text-nowrap'>
                    <i class="mr-2 fas fa-file-pdf fa-2x text-black-50"></i>
                    <a href="#" class="text-black-50"><u>Download!</u></a>
                  </td>
                </tr>
                    <?}
                  }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ADD-COMING-MODAL -->
<div class="modal fade bs-example-modal-lg"
     id='add-coming-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add receipt</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form method="post">
          <div class="col-12">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">№ receipt:</label>
                <input type="number" name="supply_number" class="form-control" placeholder="Enter № receipt">
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label w-100">receipt date:</label>
                <input class="form-control input-date" placeholder="Select a date" type="text" name="supply_date" readonly>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label w-100">№ Order:</label>
                <select class="custom-select" name="order">
                    <? if (!empty($orders)) {
                      foreach ($orders as $key => $order) {?>
                          <option value="<?=$order->id;?>"><?=$order->order_num;?></option>
                      <?}
                    }?>
                  </select>
              </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">Component name:</label>
                  <input type="text" class="form-control" placeholder="ceramic capacitor" name="component">
                </div>
                <div class="form-group col-md-6">
                  <label class="col-form-label w-100">QTY:</label>
                  <input name="quantity" type="number" class="form-control" placeholder="123">
                </div>
              </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Storage:</label>
                <select class="custom-select" name="storage">
                  <? if(!empty($storages)){
                    foreach ($storages as $key => $store) {?>
                        <option value="<?=$store->id;?>"><?=$store->name;?></option>
                    <?}
                  } ?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label w-100">Attach a supporting document:</label>
                <input type="file" name="document" id="filter_input" multiple="multiple">
              </div>
            </div>
            <div class="form-row pt-3">
              <!-- <div class="col">
                <button type="submit" class="btn btn-block btn--md btn-success waves-effect waves-light">add component
                </button>
              </div> -->
              <div class="col">
                <button type="submit" name="add-new-supply" value="1" class="btn btn-block btn--md btn-success waves-effect waves-light">Save
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- ****************** info modal -->
<div class="modal fade bs-example-modal-lg"
     id='coming-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">№ receipt 2390</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <p>
          <div class='font-weight-bold mb-2'>Component:</div>
          <div>Capacitor ceramic</div>
        </p>
        <p>
        <div class='font-weight-bold mb-2'>QTY defective components:</div>
        <div>200</div>
        </p>
        <p>
        <div class='font-weight-bold mb-2'>Comment:</div>
        <div>Текст комментария..</div>
        </p>
        <p>
          <div class="text-nowrap">
            <i class="mr-2 fas fa-file-pdf fa-2x text-black-50"></i>
            <a href="#" class="text-black-50"><u>Download!</u></a>
          </div>
        </p>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
