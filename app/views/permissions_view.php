<?php $this->load->view('layouts/header_view'); ?>

  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class='row'>
         <div class='col-12'>

         	<!-- ///////////////////////////////////////// -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Access permission</h3>
        </div>

        <div class='row'>
          <div class='col-md-3'>
            <button
                    type="button"
                    data-toggle='modal'
                    data-target='.modal-add-role'
                    class="btn btn-block btn--md btn-success waves-effect waves-light add-user-btn">Add a role
            </button>
          </div>
        </div>
        
        <div class="box-body table-responsive">
            <span id="permissions_message"></span>
            
            <table class="table table-bordered permissions-tbl">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th>Order</th>
                        <th>Formation of an order</th>
                        <th>Add components</th>
                        <th>Supplier responses</th>
                        <th>Formation of an offer</th>
                        <th>Logistics</th>
                        <th>Formation of an account</th>
                        <th>The receipt of payment</th>
                        <th>Order</th>
                        <th>Supplier's account</th>
                        <th>Payment of accounts</th>
                        <th>Request for delivery</th>
                        <th>Receipt of the goods</th>
                        <th>QC</th>
                        <th>Result</th>
                    </tr>
                </thead>
                <tbody>
                        <? if (!empty($permissions)) {
                            foreach ($permissions as $key => $perm) {?>
                    <tr id="<?=$perm->id;?>">
                        <td><?=$perm->name;?></td>
<td><input type="checkbox" value="1" <?=$perm->order_page==1?' checked ':'';?> name="order_page"></td>
<td><input type="checkbox" value="1" <?=$perm->formation_order==1?' checked ':'';?> name="formation_order"></td>
<td><input type="checkbox" value="1" <?=$perm->add_components==1?' checked ':'';?> name="add_components"></td>
<td><input type="checkbox" value="1" <?=$perm->provider_offers==1?' checked ':'';?> name="provider_offers"></td>
<td><input type="checkbox" value="1" <?=$perm->offer_choose==1?' checked ':'';?> name="offer_choose"></td>
<td><input type="checkbox" value="1" <?=$perm->logistics==1?' checked ':'';?> name="logistics"></td>
<td><input type="checkbox" value="1" <?=$perm->account_formation==1?' checked ':'';?> name="account_formation"></td>
<td><input type="checkbox" value="1" <?=$perm->receipt_of_payment==1?' checked ':'';?> name="receipt_of_payment"></td>
<td><input type="checkbox" value="1" <?=$perm->purchase_order==1?' checked ':'';?> name="purchase_order"></td>
<td><input type="checkbox" value="1" <?=$perm->provider_invoices==1?' checked ':'';?> name="provider_invoices"></td>
<td><input type="checkbox" value="1" <?=$perm->accountant_payment==1?' checked ':'';?> name="accountant_payment"></td>
<td><input type="checkbox" value="1" <?=$perm->supply_request==1?' checked ':'';?> name="supply_request"></td>
<td><input type="checkbox" value="1" <?=$perm->product_comings==1?' checked ':'';?> name="product_comings"></td>
<td><input type="checkbox" value="1" <?=$perm->qc==1?' checked ':'';?> name="qc"></td>
<td><input type="checkbox" value="1" <?=$perm->total==1?' checked ':'';?> name="total"></td>
                    </tr>
                         <?}
                        }?>
                </tbody>
            </table>

        </div>


    </div>
</section>

         	<!-- ///////////////////////////////////////// -->
	      </div>
	    </div>         	
      </div>
    </div>
  </div>
</div>



<!-- modal ADD role -->
<div class="modal fade bd-example-modal-lg modal-add-role"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add a role</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form method="post">
            <div class="form-group w-100">
              <label class="col-form-label w-100">Role:</label>
              <input name='name' type="text" class="form-control" placeholder="Role">
            </div>

            <div class="form-group w-100 pt-2">
              <button type="submit" value="1" name="add-role" class='btn btn-block btn-success waves-effect'>Add</button>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php $this->load->view('layouts/footer_view'); ?>
			
