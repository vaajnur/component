<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
  <meta content="Coderthemes" name="author"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <!-- App favicon -->
  <link rel="shortcut icon" href="public/images/favicon.ico">
  <!-- Bootstrap Css -->
    <?php echo get_css('bootstrap'); ?>
  <?php echo get_css('icons'); ?>
  <?php echo get_css('app'); ?>

</head>

<body>
<div class="account-pages mt-5 mb-5">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 col-indexlg-6 col-xl-5">
        <div class="card">
          <div class="card-body p-4">
            <div class="text-center mb-4">
              <h4 class="text-uppercase mt-0">Authorization</h4>
            </div>

            <div class="col-12">
            </div>
            <form id="login" name="" action="" method="post" enctype="multipart/form-data">
              <div class="form-group mb-3">
                <label for="username">Login</label>
                <input class="form-control" type="text" id="username" name="login" required="" placeholder="Login">
              </div>
              <div class="form-group mb-3">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" required="" id="password"
                       placeholder="******">
              </div>
              <div class="form-group mb-3">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="checkbox-signin" name="remember" checked="">
                  <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                </div>
              </div>
              <div class="form-group mb-0 text-center">
                <button class="btn btn-primary btn-block" type="submit" name="send-login" value="1">Sing up</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

<?php echo get_js('vendor'); ?>
<?php echo get_js('tabledit'); ?>
<?php echo get_js('app'); ?>

</body>
</html>
