<div class="modal hide fade categor-comp_name-modal" id='choose-component' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-lg-2">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h4 class="modal-title">Choose component</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div> -->
            <div class="row col-lg-12 col-md-12 nomencltr-categories">

                <? require dirname(__DIR__).'/layouts/sidebar2.php';?>
               
                <section class="col-lg-8 col-md-8 pt-2 pl-2 pr-2">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="heading-engener-change ml-4">
                        <h3><?=$category->name;?></h3>
                    </div>
                    <!-- <div class="row justify-content-end">
                        <div class="pt-2 col-lg-6 col-md-8 col-sm-12">
                            <form name="search" action="#" method="get" class="form-inline form-search pull-right">
                                <div class="input-group">
                                    <input class="form-control inp-search_categ btn-search-cat" id="searchInput" type="text" name="search" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-white" disabled><span class="fe-search"></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> -->
                    <div class="modal-body order-select__component">
                        <section class="col-lg-11 col-md-12 table-category-content__el">
                            <table class="table table-borderless table-componet__categ-list" id="categor-components-list-name">
                                <thead>
                                    <tr class="d-flex">
                                        <th class="col-lg-1 col-sm-1">Component Name</th>
                                        <th class="col-lg-1 col-sm-1">Price</th>
                                        <th class="col-lg-1 col-sm-1">Patrnumber</th>
                                        <th class="col-lg-1 col-sm-1">Comment</th>
                                        <th class="col-lg-1 col-sm-1">Body</th>
                                        <th class="col-lg-1 col-sm-1">Manufacturer</th>
                                        <th class="col-lg-1 col-sm-1">Marking</th>
                                        <th class="col-lg-1 col-sm-1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? 
                               // debug($category);
                               if (!empty($category->components)) {
                                   foreach ($category->components as $key => $component) {?>
                                    <tr class="d-flex">
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?="<a class='choose-component-enginer d-inline-block' href='#' data-component='".json_encode($component)."'>{$component->name}</a><br>";?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->price;?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->pn;?></td>
                                         <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->comment;?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->body;?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->manufacturer;?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?=$component->marking;?></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1"><?="<a class='choose-component-enginer d-inline-block btn-component__enginer' href='#' data-component='".json_encode($component)."'>Select</a><br>";?></td>
                                    </tr>
                                    <?}
                                   }?>
                                </tbody>
                            </table>
                        </section>
                        <button class="btn btn-lg btn-dark mb-2 btn-add__comp btn-add__comp_in_order" data-toggle="modal" data-target="#categor-add_component" type="button" data-category="<?=$category->id;?>">Add&nbsp;a&nbsp;component</button>
                    </div>
            </div>
        </div>
    </div>
</div>
