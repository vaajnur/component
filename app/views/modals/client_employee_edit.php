<div class="modal hide fade"
     id='edit-contact-modal'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit a contact</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action='' method="post" enctype="multipart/form-data">
          <div class='row'>
            <div class="form-group col-md-6">
              <label>Appointment:</label>
              <input type="text" name="post" class="empl-post form-control" placeholder='Appointment' value="">
            </div>
            <input type="hidden" name="empl_id" value="" class="empl-id">
            <div class='col-12'>
              <hr class='ym-1'>
            </div>

            <div class='col-12 mb-2'>
              <label>fields :</label>
            </div>
            <!-- //////////////// current fields /////////////// -->
            <div class='col-12 fields-current'>


            </div>
            <!-- /////////////// new fields /////////////// -->
            <div class='col-12 fields'></div>
            <div class='col-12 pt-2'>
              <button type="button"
                      id='edit-field_contact-edit-modal'
                      class="btn btn-block btn-primary waves-effect width-md waves-light">Add a new field
              </button>
              <button type="submit" name="edit-employee" value="1" class="btn btn-block btn-success waves-effect width-md waves-light">Save
              </button>
              <button type="button" class="btn btn-block btn-danger waves-effect width-md waves-light">Delete</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>