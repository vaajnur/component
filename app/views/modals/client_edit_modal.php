<!-- EDIT CLIENT INFORMATION MODAL-->
<div class="modal hide fade"
     id='edit-client-info-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit basic information</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action='' method="post" enctype="multipart/form-data">

          <div class="row">
            <?foreach ($base_fields as $key => $value) {?>
              <div class="form-group form-group-row col-md-6">
              <div class="d-flex justify-content-between align-items-end">
                <label><?=$value;?></label>
              </div>
              <div class="d-flex">
                <input type="text" name="<?=$key;?>" value="<?=$client->$key?>" class="form-control" placeholder="<?=$value;?>">
              </div>
            </div>
            <?}?>
          </div>

          <h5 class="text-center">Custom fields</h5>
          <div class="row form-fields">
            <? if (!empty($client->custom_f)) {
              foreach ($client->custom_f as $key => $cf) {?>
            
            <div class="form-group form-group-row col-md-6">
              <div class='d-flex justify-content-between align-items-end'>
                <label><?=$cf->name;?></label>
              </div>
              <div class='d-flex'>
                <input type="text" name="custom_f[<?=$cf->name;?>]" value="<?=$cf->value;?>" class="form-control" placeholder='Enter <?=$cf->name;?>'>
                <div class="d-flex align-items-center">
                  <button type="button" class="btn mr-1 ml-1 btn-sm btn-primary active canceled-delete-field-btn"
                          style="display: none; float: none;">
                    <span class="mdi mdi-close"></span>
                  </button>
                  <button class="btn btn-icon btn-sm btn-danger ml-1 delete-field-btn">
                    <i class="fas fa-trash-alt"></i>
                  </button>
                  <button  data-cf-id="<?=$cf->id;?>" type="button" class="confirm-delete-field-btn btn btn-sm rounded btn-danger  delete-cfield-btn"
                          style="display: none; float: none;">
                    <span class="mdi mdi-check"></span>
                  </button>
                </div>
              </div>
            </div>
              <?}
            }?>
          </div>
          <div class='row'>
            <div class='col-12'>
              <button type="button"
                      data-target='#add-new-field-modal'
                      data-toggle='modal'
                      class="btn btn-block btn-primary waves-effect width-md waves-light">Add new field
              </button>
              <button type="submit" name="edit-client" value="1"  class="btn btn-block btn-success waves-effect width-md waves-light">Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
