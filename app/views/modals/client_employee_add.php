<div class="modal hide fade"
     id='add-new-contact-modal'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add new contact</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action='' method="post" enctype="multipart/form-data">
          <div class="row form-rows">
            <div class="form-group col-md-6">
              <label>Appointment:</label>
              <input type="text" name="post" class="form-control" placeholder='Appointment'>
            </div>
            <div class='col-12'>
              <hr class='ym-1'>
            </div>

            <div class='col-12 mb-2'>
              <label>field:</label>
            </div>
            <div class='col-12 fields'></div>
            <div class='col-12 pt-2'>
              <button type="button"
                      id='add-new-field_contact-modal'
                      class="btn btn-block btn-primary waves-effect width-md waves-light">Add new field
              </button>
              <button type="submit" name="add-employee" value="1" class="btn btn-block btn-success waves-effect width-md waves-light">Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>