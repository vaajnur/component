<div class="modal hide fade" id='categor-add_component' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content edit-category-modal_cont">
          <div class="row justify-content-end">
                <div class="modal-header col-5">
                    <h4 class="modal-title">Add a component</h4>
                </div>
                <div class="modal-header col-3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <form action='' method="post" enctype="multipart/form-data">
                  
                    <div class="row pt-2">
                        <div class="col-md-6"><label>Name:</label>
                            <input type="text" name="name" class="empl-post form-control" placeholder='Name' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Partnumber</label>
                            <input type="text" name="pn" class="empl-post form-control" placeholder='Partnumber' value="">
                        </div>
                    </div>
                    <div class="row pt-2">
                        
                         <!-- attachment -->
          
                        <div class="form-group col-md-6">
                            <label class="col-form-label w-100">Add PDF:</label>
                            <input type="file" name="component_document[]" class="filter_input_for_neworder add_pdf-file">
                        </div>

                        <div class="col-md-6"><label>Comment:</label>
                            <input type="text" name="comment" class="empl-post form-control" placeholder='Comment' value="">
                        </div>
                    </div>
                    <div class="row pt-2">

                        <div class="col-md-6"><label>Body:</label>
                            <input type="text" name="body" class="empl-post form-control" placeholder='Body' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Manufacturer:</label>
                            <input type="text" name="manufacturer" class="empl-post form-control" placeholder='Manufacturer' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Marking:</label>
                            <input type="text" name="marking" class="empl-post form-control" placeholder='Marking' value="">
                        </div>
                    </div>

                    <!-- общие для категории поля -->

                    <div class="row pt-2">
                        <!-- <div class="col-md-12">Common fields <hr></div> -->
                    <? if (!empty($category->custom_f)) {
                        foreach ($category->custom_f as $key => $cf) {?>
                        <div class="col-md-6"><label><?=$cf->name;?></label>
                            <input type="text" name="custom_f[<?=$cf->id;?>]" class="empl-post form-control" placeholder='<?=$cf->name;?>' value="">
                        </div>
                        <?}
                    }?>
                    </div>

                   



                    <!-- <div class="row">
                        <div class="col-md-6">
                          <label>Add PDF:</label>
                          <button type="button" id='edit-field_contact-edit-modal' class="btn btn-block btn-dark waves-effect width-md waves-light">Add
                          </button>
                        </div>
                        <div class="col-md-5 pt-4">
                          <img src="../../public/images/google-docs.png" alt="docs" />
                          <a class="download-pdf-modal" href="#" download>Delete</a>
                        </div>
                    </div> -->

                    <!-- /////////////// new fields /////////////// -->
                    <div class='col-12 pt-4 pl-0 pr-0'>
                        <button type="submit" id='edit-field_contact-edit-modal' name="add_component" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Add
                        </button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
