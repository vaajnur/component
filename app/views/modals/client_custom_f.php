<div class="modal fade border border-dark"
     tabindex="-1"
     id='add-new-field-modal'
     role="dialog"
     aria-labelledby="myCenterModalLabel"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content border border-secondary">
      <div class="modal-header">
        <h4 class="modal-title" id="myCenterModalLabel">Add new field</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
        <div class="col-md-12">
          <div class="form-group">
            <label>field name:</label>
            <input type="text" name="" class="form-control" placeholder='enter the name of the field:'>
          </div>
        </div>
        <div class='col-12'>
          <button type="button"
                  id='add-new-field_edit-info-modal'
                  name="add-custom-f-client"
                  data-dismiss="modal"
                  class="btn btn-block btn-success waves-effect width-md waves-light">Add
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>