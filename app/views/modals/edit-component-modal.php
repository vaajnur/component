<div class="modal hide fade" id='categor-add_component' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content edit-category-modal_cont">
            <div class="row justify-content-end">
                <div class="modal-header col-5">
                    <h4 class="modal-title">Edit a component</h4>
                </div>
                <div class="modal-header col-3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <form action='' method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?=$component->id;?>">
                    
                    <div class="row pt-2">
                        <div class="col-md-6"><label>Name:</label>
                            <input type="text" name="name" class="empl-post form-control" placeholder='Name' value="<?=$component->name;?>">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Partnumber</label>
                            <input type="text" name="pn" class="empl-post form-control" placeholder='Partnumber' value="<?=$component->pn;?>">
                        </div>
                    </div>
                    <div class="row pt-2">
                         <!-- attachment -->
                        <div class="form-group col-md-6">
                            <label class="col-form-label w-100">Add PDF:</label>
                            <? 
                             // debug($component);
                            if($component->document_id==true):?>
                            <div class="">
                                <div class="jFiler-item-thumb">
                                    <i class="fas fa-file-pdf fa-2x text-black-50"></i>
                                </div>
                                <div class="doc-name"><a target="_blank" href="<?=$component->document_path;?>">
                                        <?=$component->document_name;?></a></div>
                                <a data-offer="<?=$component->id;?>" data-doc="<?=$component->document_id;?>" data-type="component_document" class="del-doc jFiler-item-trash-action text-black-50"><u>Delete</u></a>
                            </div>
                            <? endif;?>
                            <input type="file" name="component_document[]" class="filter_input_for_neworder">
                        </div>


                        <div class="col-md-6"><label>Comment:</label>
                            <input type="text" name="comment" class="empl-post form-control" placeholder='Comment' value="<?=$component->comment;?>">
                        </div>

                    </div>
                    <div class="row pt-2">
                        
                        <div class="col-md-6"><label>Body:</label>
                            <input type="text" name="body" class="empl-post form-control" placeholder='Body' value="<?=$component->body;?>">
                        </div>

                        <div class="col-md-6 ml-auto"><label>Manufacturer:</label>
                            <input type="text" name="manufacturer" class="empl-post form-control" placeholder='Manufacturer' value="<?=$component->manufacturer;?>">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Marking:</label>
                            <input type="text" name="marking" class="empl-post form-control" placeholder='Marking' value="<?=$component->manufacturer;?>">
                        </div>
                    </div>

                  <!-- common fields -->
                    <div class="row pt-3">
                      <div class="col-md-12">
                          Common fields
                          <hr>
                      </div>
                      <? if (!empty($component->custom_f)) {
                        foreach ($component->custom_f as $key => $cf) {?>
                            <div class="form-group col-md-6">
                                <label><?=$cf->name;?>:</label>
                                <input type="text" name="custom_f[<?=$cf->id;?>]" class="empl-post form-control" placeholder='<?=$cf->name;?>' value="<?=$cf->value;?>">
                            </div>
                        <?}
                      }?>
                    </div>

                   


                    <!-- <div class="row">
                        <div class="col-md-6">
                            <label>Add PDF:</label>
                            <button type="button" id='edit-field_contact-edit-modal' class="btn btn-block btn-dark waves-effect width-md waves-light">Add
                            </button>
                        </div>
                        <div class="col-md-5 pt-4">
                            <img src="../../public/images/google-docs.png" alt="docs" />
                            <a class="download-pdf-modal" href="#" download>Delete</a>
                        </div>
                    </div> -->
                    <!-- ///////////////  /////////////// -->
                    <div class='col-12 pl-0 pr-0'>
                        <button type="submit" id='edit-field_contact-edit-modal' name="edit_component" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Edit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
