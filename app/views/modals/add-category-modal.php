<div class="modal hide fade" id='categor' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content edit-category-modal_cont">
          <div class="row justify-content-end">
                <div class="modal-header col-5">
                    <h4 class="modal-title">Add a category</h4>
                </div>
                <div class="modal-header col-3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <form action='/category' method="post" enctype="multipart/form-data">
                    <input type="hidden" name="referer" value="<?=request_uri();?>">
                    <div class='row'>
                        <div class="form-group col-md-6">
                            <label>Category name:</label>
                            <input type="text" name="name" class="empl-post form-control" placeholder='Category name' value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Parent category:</label>
                            <? /*if (!empty($categories)) {?>
                                <select name="parent_id" class="empl-post form-control" id="">
                                <?foreach ($categories as $key => $categor) {?>
                                    <option value="<?=$categor->id;?>_<?=$categor->level;?>"><?=$categor->name;?></option>
                                <?}?>
                                </select>
                            <?}*/?>
                            <?//=$menu_tree_Select;?>
                            <select name="parent_id" id="select_tree" data-json='<?=$menu_tree_select2;?>'></select>
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-6"><label>Body:</label>
                            <input type="text" name="body" class="empl-post form-control" placeholder='Body' value="">
                        </div>
                        <div class="col-md-6"><label>Voltage:</label>
                            <input type="text" name="voltage" class="empl-post form-control" placeholder='Voltage' value="">
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-6 ml-auto"><label>Deviation:</label>
                            <input type="text" name="deviation" class="empl-post form-control" placeholder='Deviation' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Manufacturer:</label>
                            <input type="text" name="manufacturer" class="empl-post form-control" placeholder='Manufacturer' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Production year:</label>
                            <input type="text" name="year_of_production" class="empl-post form-control" placeholder='Production year' value="">
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-6 first-bl_custom-name">
                          <div class="row justify-content-between">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span inp-1">Delete</span>
                        </div>
                    </div>
                            <input id="inp-1" type="text" name="custom_f_name[]" class="empl-post form-control form-custom_name" placeholder='Property Name' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Unit of measurement:</label>
                            <input type="text" name="custom_f_value[]" class="empl-post form-control" placeholder='Unit of measurement' value="">
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-6 first-bl_custom-name">
                          <div class="row justify-content-between">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span inp-2">Delete</span>
                        </div>
                    </div>
                            <input id="inp-2" type="text" name="custom_f_name[]" class="empl-post form-control form-custom_name" placeholder='Property Name' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Unit of measurement:</label>
                            <input type="text" name="custom_f_value[]" class="empl-post form-control" placeholder='Unit of measurement' value="">
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-6 first-bl_custom-namen">
                          <div class="row justify-content-betwee">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span inp-3">Delete</span>
                        </div>
                    </div>
                            <input id="inp-3" type="text" name="custom_f_name[]" class="empl-post form-control form-custom_name" placeholder='Property Name' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Unit of measurement:</label>
                            <input type="text" name="custom_f_value[]" class="empl-post form-control" placeholder='Unit of measurement' value="">
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-6 first-bl_custom-name">
                          <div class="row justify-content-between">
                              <div class="col-9">
                          <label>Property Name:</label>
                          </div>
                          <div class="col">
                          <span class="add_compon-del-span inp-4">Delete</span>
                        </div>
                    </div>
                            <input id="inp-4" type="text" name="custom_f_name[]" class="empl-post form-control form-custom_name" placeholder='Property Name' value="">
                        </div>
                        <div class="col-md-6 ml-auto"><label>Unit of measurement:</label>
                            <input type="text" name="custom_f_value[]" class="empl-post form-control" placeholder='Unit of measurement' value="">
                        </div>
                    </div>
                    <div id="input0" class="row pt-2"></div>
                    <div id="input1" class="row pt-2"></div>
                    <div id="input2" class="row pt-2"></div>
                    <div id="input3" class="row pt-2"></div>
                    <div class="add" onclick="addInput()">+</div>
  
                    <!-- //////////////// current fields /////////////// -->
                    <!-- <div class='col-12 fields-current'>
                    </div> -->
                    <!-- /////////////// new fields /////////////// -->
                    <!-- <div class='col-12 fields'></div> -->
                    <div class='col-12 pt-4 pl-0 pr-0'>
                        <!-- <button type="button" id='edit-field_contact-edit-modal' class="btn btn-block btn-primary waves-effect width-md waves-light">Add a new field
                        </button> -->
                        <button type="submit" name="add_category" value="1" class="btn btn-block btn-dark waves-effect width-md waves-light">Add
                        </button>
                        <!-- <button type="button" class="btn btn-block btn-danger waves-effect width-md waves-light">Delete</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
