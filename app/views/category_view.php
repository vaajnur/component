<?php $this->load->view('layouts/header_view'); ?>
Category
<div class="wrapper-category category-comp_name" id="wrapper-category">
    <div class="row mt-4">
        

        <? require __DIR__.'/layouts/sidebar.php';?>
        

        <section class="col-lg-9 col-md-8 mt-5 pt-2 pl-5 pr-5">
            <?if(!empty($mess)){?>
            <div class="col-12">
                <div class="alert alert-<?=$mess['type'];?>">
                    <?=$mess['text'];?>
                </div>
            </div>
            <? } ?>
            <div class="heading">
                <h3>
                    <?=$category->name;?>
                </h3>
            </div>
            <div class="row justify-content-between">
                <? if(canDoOperation('add_components') ){?>
                <div class="pt-4 pl-0 col-8">
                    <button class="btn btn-lg btn-dark mb-2 btn-add__comp" data-toggle='modal' data-target='#categor-add_component' type="button">Add&nbsp;a&nbsp;component</button>
                </div>
                <? } ?>
                <!-- <div class="pt-4 col-lg-4 col-md-8">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle btn_down-text" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categories
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- <div class="row justify-content-end">
              <div class="col-lg-3 col-md-12 pt-2">
                <form name="search" action="#" method="get" class="form-inline form-search pull-right">
                    <div class="input-group">
                        <input class="form-control inp-search_categ btn-search-cat" id="searchInput" type="text" name="search" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-white"><span class="fe-search"></span></button>
                        </div>
                    </div>
                </form>
            
              </div>
            </div> -->
            <div class="row justify-content-between">
                <div class="pt-4 pl-0 pl-1 col-lg-8 col-md-6 col-sm-12">
                    <p class="value-text_search">
                    </p>
                </div>
                <div class="pt-4 col-lg-4 col-md-8 col-sm-12">
                    <form name="search" action="#" method="get" class="form-inline form-search pull-right">
                        <div class="input-group search-group_categor">
                            <!-- <input class="form-control inp-search_categ btn-search-cat" id="searchInput" type="text" name="search" placeholder="Search">
                            <span class="fe-search"></span> -->
                            <!-- <div class="input-group-btn">
                                <button type="submit" class="btn btn-white" disabled><span class="fe-search"></span></button>
                            </div> -->
                        </div>
                    </form>
                </div>
            </div>
        <? //debug($category->components);?>
            <table class="table table-borderless table-componet__categ-list  <?=canDoOperation('add_components')?'':' not_edit ';?>" id="category-components-list">
                <thead>
                    <tr class="d-flex">
                        <th class="">id</th>
                        <th class="">Component Name</th>
                        <th class="">Price</th>
                        <th class="">Partnumber</th>
                        <th class="">Comment</th>
                        <th class="">Body</th>
                        <th class="">Manufacturer</th>
                        <th class="">Marking</th>
                        <? if (!empty($category->components[0]->custom_f_name)) {
                            $cf_name = explode(',' , $category->components[0]->custom_f_name);
                            foreach ($cf_name as $key => $value) {?>
                                <th class="col-lg-1 col-sm-3"><?=$value;?></th>
                            <?}
                        }?>
                    </tr>
                </thead>
                <tbody>
                    <? 
            if (!empty($category->components)) {
                foreach ($category->components as $key => $component) {?>
                    <tr class="d-flex">
                        <td class=""><?=$component->id;?></td>
                        <td class=""><?=$component->name;?></td>
                        <td class=""><?=$component->price;?></td>
                        <td class=""><?=$component->pn;?></td>
                        <td class=""><?=$component->comment;?></td>
                        <td class=""><?=$component->body;?></td>
                        <td class=""><?=$component->manufacturer;?></td>
                        <td class=""><?=$component->marking;?></td>
                        <? if (!empty($category->components[0]->custom_f_name)) {
                            $cf_value = explode(',' , $component->custom_f_val);
                            foreach ($cf_name as $key => $value) {?>
                                <th class=""><?=$cf_value[$key];?></th>
                            <?}
                        }?>
                    </tr>
                    <?}
              }?>
                </tbody>
            </table>
        </section>
    </div>
</div>
<? require __DIR__.'/modals/add-component-modal.php';?>
<? require __DIR__.'/modals/add-category-modal.php';?>

<?php $this->load->view('layouts/footer_view'); ?>