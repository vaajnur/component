<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row align-items-center">
              <div class="col-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Account log</h4>
                </div>
              </div>
              <div class='col-md-3 offset-md-3'>
                <button
                        type="button"
                        data-target='#add-invoice-modal'
                        data-toggle='modal'
                        class="btn btn-block btn--md btn-success waves-effect waves-light add-user-btn">Add account
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-invoice-journal"
                     class="table-hover table w-100 table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>№ account</th>
                  <th>Sum total</th>
                  <th>Payment ratio</th>
                </tr>
                </thead>
                <tbody>
                <? if(!empty($providers_invoices)){
                  foreach ($providers_invoices as $key => $invoice) {?>
                    <tr class='clickable-row' data-href='invoice-page.html'>
                      <td><?=$key+1;?></td>
                      <td><?=$invoice->invoicein;?></td>
                      <td><?=$invoice->paymentout;?></td>
                      <td><?=(float)$invoice->price>0?$invoice->paymentout/$invoice->price*100:'0';?>%</td>
                    </tr>
                  <?}
                }?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ADD INVOICE MODAL -->
<div class="modal fade bs-example-modal-lg"
     id="add-invoice-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add account</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <!--        <form>-->
        <div class='row'>
          <div class="form-group col-12">
            <label class="col-form-label w-100">№ account:</label>
            <input type="number" class="form-control" placeholder="Введите № счета">
          </div>
          <div class='form-group col'>
            <button type="submit"
                    data-toggle='modal'
                    data-target='#invoice-journal-modal'
                    class="btn btn-block btn--md btn-primary waves-effect waves-light">Add component
            </button>
            <button type="submit" class="btn btn-block btn--md btn-success waves-effect waves-light">Save
            </button>
          </div>
        </div>
        <!--        </form>-->
      </div>
    </div>
  </div>
</div>

<!-- ORDERS-JOURNAL-MODAL -->
<div class="modal fade"
     id='invoice-journal-modal'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-modal="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Application log</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <table class="table-hover table w-100 table-bordered"
               id='datatable-invoice-journal2'
        >
          <thead>
          <tr>
            <th>№ n\n</th>
            <th>№ application</th>
            <th>Date</th>
            <th>Client</th>
            <th>Category</th>
          </tr>
          </thead>
          <tbody>
          <tr data-target='#invoice-modal' data-toggle='modal'>
            <td>1</td>
            <td>1</td>
            <td>10.02.2021</td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr data-target='#invoice-modal' data-toggle='modal'>
            <td>1</td>
            <td>1</td>
            <td>10.02.2021</td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr data-target='#invoice-modal' data-toggle='modal'>
            <td>1</td>
            <td>1</td>
            <td>10.02.2021</td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr data-target='#invoice-modal' data-toggle='modal'>
            <td>1</td>
            <td>1</td>
            <td>10.02.2021</td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          <tr data-target='#invoice-modal' data-toggle='modal'>
            <td>1</td>
            <td>1</td>
            <td>10.02.2021</td>
            <td>Client's name</td>
            <td>Category</td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- ORDERS-JOURNAL-MODAL2 -->
<div class="modal fade"
     id='invoice-modal'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myExtraLargeModalLabel2"
     style="display: none;"
     aria-modal="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myExtraLargeModalLabel2">Application log</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class='row mt-2'>
          <div class='col-6'>
            <h4 class='page-title'>Application №1</h4>
          </div>
          <div class='col-3 offset-3'>
            <button type="button"
                    data-toggle='modal'
                    data-target='#add-invoice-modal2'
                    class="btn btn-block btn-success waves-effect width-md waves-light">Save
            </button>
          </div>
          <div class="col-12 mt-2 mb-3">
            <div class="table-responsive">
              <table class="table table-centered mb-0">
                <thead>
                <tr>
                  <th></th>
                  <th>#</th>
                  <th>Name</th>
                  <th>Deal</th>
                  <th>Position on PCB</th>
                  <th>P/N</th>
                  <th>Comment</th>
                  <th>Substitutes</th>
                  <th>Code</th>
                  <th>QTY per 1 product</th>
                  <th>QTY per 3000 products</th>
                  <th>D/C</th>
                  <th>Body</th>
                  <th>MFG</th>
                  <th>QTY per pack</th>
                  <th>QTY quantity/piece</th>
                  <th>Price per unit($)</th>
                  <th>Sum($)</th>
                  <th>Order lead time (days)</th>
                  <th>Weight per 1 unit (kg)</th>
                  <th>T/p ($)</th>
                </tr>
                <tr>
                  <th></th>
                  <th>#</th>
                  <th>品名</th>
                  <th>路板上位号</th>
                  <th>型号</th>
                  <th>注</th>
                  <th>代用品</th>
                  <th></th>
                  <th>品的数量</th>
                  <th>数量</th>
                  <th>年份</th>
                  <th>封装</th>
                  <th>厂家/牌子</th>
                  <th>包装的数量</th>
                  <th>采数量</th>
                  <th>价 ($)</th>
                  <th>价 ($)</th>
                  <th>Sum($)</th>
                  <th>交期(日)</th>
                  <th>片重量 (公斤)</th>
                  <th>目价($)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>
                    <div class="checkbox checkbox-single">
                      <input type="checkbox" id="singleCheckbox1" value="option1" aria-label="Single checkbox One">
                      <label for="singleCheckbox1"></label>
                    </div>
                  </td>
                  <td>1</td>
                  <td>quartz</td>
                  <td></td>
                  <td>HC49SM-12.000MHZ</td>
                  <td>http://www.caltron.com.hk/Page/en..</td>
                  <td></td>
                  <td>ACA-C1040N-17A</td>
                  <td>1</td>
                  <td>3000</td>
                  <td></td>
                  <td>0603</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>0.075</td>
                  <td>225</td>
                  <td>1-2wks</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    <div class="checkbox checkbox-single">
                      <input type="checkbox" id="singleCheckbox2" value="option1" aria-label="Single checkbox One">
                      <label for="singleCheckbox2"></label>
                    </div>
                  </td>
                  <td>2</td>
                  <td>quartz</td>
                  <td></td>
                  <td>HC49SM-12.000MHZ</td>
                  <td>http://www.caltron.com.hk/Page/en..</td>
                  <td></td>
                  <td>ACA-C1040N-17A</td>
                  <td>1</td>
                  <td>3000</td>
                  <td></td>
                  <td>0603</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>0.075</td>
                  <td>225</td>
                  <td>1-2wks</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg"
     id='add-invoice-modal2'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add account</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <!-- <form>-->
        <div class="col-12">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label class="col-form-label w-100">№ account:</label>
              <input type="number" class="form-control" placeholder="Введите № счета">
            </div>
          </div>
          <div class="form-row">
            <label class="col-form-label w-100">Application №1</label>
          </div>
          <div class="row align-items-center">
            <div class='col-md-2 font-16 pt-3'>
              <span>Capacitor ceramic</span>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">QTY</label>
              <input class="form-control component-count" type='text'>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">Price:</label>
              <input class="form-control component-price" type='text'>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">Sum:</label>
              <input class="form-control component-sum" type='text' readonly>
            </div>
            <div class='col-md-1 pt-1'>
              <button class="btn btn-icon btn-xs btn-danger  mt-2">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
          </div>
          <div class="row align-items-center">
            <div class='col-md-2 font-16 pt-3'>
              <span>Capacitor ceramic</span>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">QTY</label>
              <input class="form-control component-count" type='text'>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">Price:</label>
              <input class="form-control component-price" type='text'>
            </div>
            <div class="form-group col-md-3">
              <label class="col-form-label w-100">Sum:</label>
              <input class="form-control component-sum" type='text' readonly>
            </div>
            <div class='col-md-1 pt-1'>
              <button class="btn btn-icon btn-xs btn-danger  mt-2">
                <i class="fas fa-trash-alt"></i>
              </button>
            </div>
          </div>
          <div class="row align-items-center mb-2">
            <div class='col-md-12 font-16 pt-3 text-right'>
              <span class='total-sum'></span>
            </div>
          </div>
          <div class='row row align-items-center'>
            <div class='col-6'>
              <button type="button" class="btn btn-block btn-primary waves-effect width-md waves-light">Add component
              </button>
            </div>
            <div class='col-6'>
              <button type="button" class="btn btn-block btn-primary waves-effect width-md waves-light">Save
              </button>
            </div>
          </div>
        </div>
        <!-- </form>-->
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
