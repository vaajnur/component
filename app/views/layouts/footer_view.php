<?php echo get_js('vendor'); ?>
<?php //echo get_js('moment'); ?>
<script src="/assets/libs/jquery-tabledit/jquery.tabledit.js"></script>
<script src="/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="/assets/libs/bootstrap-datepicker/bootstrap-datepicker.ru.min.js"></script>
<!-- <script src="/assets/libs/datatables/jquery.dataTables.min.js"></script> -->
<script src="/assets/libs/datatables/datatables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="/assets/libs/select2/select2.min.js"></script>

<script src="/assets/libs/yadcf/jquery.dataTables.yadcf.js"></script>
<!-- libs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>


<?php echo get_js('jquery.filer.min'); ?>
<?php echo get_js('tabledit'); ?>
<?php echo get_js('filters'); ?>
<?php echo get_js('select2totree'); ?>
<?php echo get_js('app'); ?>
<?php echo get_js('script'); ?>

</body>
</html>
