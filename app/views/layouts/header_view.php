<!DOCTYPE html>
<html lang="en">
<head>
	    <?   
  if($this->asset->get_title())
      echo $this->asset->get_title() . "\n";
  else
      echo '<title>Component</title>';
    if($this->asset->get_meta()) {
      foreach($this->asset->get_meta() as $meta_tag) {
        echo $meta_tag . "\n";
      }
    }
    // Favicon
    if($this->asset->get_favicon())
      echo $this->asset->get_favicon() . "\n";
      ?>   
  <meta charset="utf-8"/>
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
  <meta content="Coderthemes" name="author"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <link rel="shortcut icon" href="<?php echo BASE_DIR; ?>/public/images/favicon.ico">
  <link href="/assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css"/>
  <link href="/assets/libs/yadcf/jquery.dataTables.yadcf.css" rel="stylesheet" type="text/css"/>
  <link href="/assets/libs/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>
  <?php echo get_css('bootstrap'); ?>
  <?php echo get_css('icons'); ?>
  <?php echo get_css('jquery.filer'); ?>
  <?php echo get_css('app'); ?>
  <?php echo get_css('styles'); ?>
  <?php echo get_css('select2totree'); ?>
</head>
<body data-layout="horizontal" data-topbar="dark">
<div id='wrapper'>
  <header id="topnav">

    <!-- Topbar Start -->
    <div class="navbar-custom">
      <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">
          <li class="dropdown notification-list">
            <!-- Mobile menu toggle-->
            <a class="navbar-toggle nav-link">
              <div class="lines">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </a>
            <!-- End mobile menu toggle-->
          </li>
        </ul>

        <div class="clearfix"></div>
      </div>
    </div>

    <div class="topbar-menu active">
      <div class="container-fluid in">
        <div id="navigation" class="active">
          <!-- Navigation Menu-->
          <ul class="navigation-menu in">
            <li class="has-submenu">
              <a href="/category">Nomenclature
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <ul>
                    <li>
                      <a href="/categories" class="dropdown-item">Categories list</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="has-submenu">
              <a href="#"> <i class="mdi mdi-briefcase-upload"></i>Orders
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <ul>
                    <li>
                      <a href="/" class="dropdown-item">Orders</a>
                    </li>
                    <? if($this->session->get('logined')->role != '3'):?>
                    <li>
                      <a href="/create-order" class="dropdown-item">Create order</a>
                    </li>
                    <li>
                      <a href="/clients" class="dropdown-item">Clients</a>
                    </li>
                    <? endif;?>
                    <li>
                      <a href="/providers" class="dropdown-item">Providers</a>
                    </li>
                    <li>
                      <a href="/archive" class="dropdown-item">Order archive</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="has-submenu">
              <a href="#">
                <i class="mdi mdi-cube-outline"></i>Storage
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <a href="/storage" class="dropdown-item">Storage</a>
                </li>
                <li>
                  <a href="/coming-providers" class="dropdown-item">Receipt log</a>
                </li>
                <li>
                  <a href="/expense-journal" class="dropdown-item">Expense log</a>
                </li>
              </ul>
            </li>
            <? if($this->session->get('logined')->role != '3'):?>
            <li class="has-submenu">
              <a href="#"> <i class="mdi mdi-bank"></i>Bank
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <a href="/bank" class="dropdown-item">Bank</a>
                </li>
                <li>
                  <a href="/invoice-journal" class="dropdown-item">Account log</a>
                </li>
              </ul>
            </li>
            <? endif;?>
            <? if($this->session->get('logined')->role == '7'):?>
            <li class="has-submenu">
              <a href="#"><i class="mdi mdi-cog-transfer"></i>Settings
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <a href="/users" class="dropdown-item">Users</a>
                </li>
                <li>
                  <a href="/permissions" class="dropdown-item">Access permission</a>
                </li>
              </ul>
            </li>
            <? endif;?>
            <li class='has-submenu float-md-right '>
              <a href="#">
                <i class='mdi mdi-24px mdi-account text-white'></i>
                <span><?=$this->session->get('logined')->fio;?></span>
                <div class="arrow-down"></div>
              </a>
              <ul class="submenu">
                <li>
                  <a href="/logout" class="dropdown-item">Exit</a>
                </li>
              </ul>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>

  </header>