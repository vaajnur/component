<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class='row'>
          <div class='col-12 mb-3'>
            <div class="row">
              <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="page-title">Client page</h4>
                </div>
              </div>
            </div>
          </div>

          <div class='card-box col-12'>
            <div class='row'>
              <div class='col-12'>
                <div class='row'>
                  <div class='col-md-6'>
                    <h5>Basic information</h5>
                  </div>
                  <div class='col-md-3 offset-md-3'>
                    <button type="button" data-toggle='modal' data-target='#edit-client-info-modal'
                            class="btn btn-block btn-primary waves-effect width-md waves-light">Edit
                    </button>
                  </div>
                </div>
              </div>


          <?if(!empty($mess)){?>
            <div class="col-12">
              <div class="alert alert-<?=$mess['type'];?>">
                <?=$mess['text'];?>
              </div>
            </div>
          <? } ?>   

              <div class='col-12'>
                <hr class="my-3 w-100">
              </div>

              <div class="table-responsive overflow-hidden">
                <table class="table table-borderless table-striped main-client-info-table mb-0">
                  <thead>
                  <!--                  <tr>-->
                  <!--                    <th></th>-->
                  <!--                    <th></th>-->
                  <!--                    <th></th>-->
                  <!--                    <th></th>-->
                  <!--                  </tr>-->
                  </thead>
                  <tbody>
                    <tr>
                      <td>Name</td>
                      <td><?=$client->full_name;?></td>
                    </tr>
                    <?foreach ($base_fields as $key => $value) {?>
                    <tr>
                      <td><?=$value;?></td>
                      <td><?=$client->$key?></td>
                    </tr>
                    <?}?>

                  <? if (!empty($client->custom_f)) {
                    foreach ($client->custom_f as $key => $fld) {?>
                  
                        <tr>
                          <td class='font-weight-bold'><?=$fld->name;?></td>
                          <td><?=$fld->value;?></td>
                        </tr>
                  
                    <?}
                  }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>


<!-- //////////////////////////////// employers -->
          <div class='card-box col-12'>
            <div class='row'>
              <div class='col-12'>
                <div class='row'>
                  <div class='col-md-6'>
                    <h5>Contact information</h5>
                  </div>
                  <div class='col-md-3 offset-md-3'>
                    <button type="button" data-toggle='modal' data-target='#add-new-contact-modal'
                            class="btn btn-block btn-success waves-effect width-md waves-light">Add a new contact
                    </button>
                  </div>
                </div>
              </div>



              <div class='col-12'>
                <hr class="my-3 w-100">
              </div>


              <? if (!empty($employers)) {
                foreach ($employers as $key => $employee) {?>
              
              <div class='col-12'>
                <div class='row'>
                  <div class='col-6 mb-3'>
                    <h5><?=$employee->post;?></h5>
                  </div>
                  <div class='col-md-3 offset-md-3 mb-2 mb-xl-0'>
                    <button
                            type="button"
                            data-toggle='modal'
                            data-target='#edit-contact-modal'
                            data-employee='<?=json_encode($employee);?>'
                            class="edit-employee btn btn-block btn-primary waves-effect width-md waves-light">Edit a contact
                    </button>
                  </div>
                </div>
              </div>
              <div class="table-responsive overflow-hidden">
                <table class="table table-borderless table-striped contact-info-table mb-0">
                  <thead>
                  <!--                  <tr>-->
                  <!--                    <th></th>-->
                  <!--                    <th></th>-->
                  <!--                  </tr>-->
                  </thead>
                  <tbody>
                    <? if (!empty($employee->custom_fields)) {
                      foreach ($employee->custom_fields as $key => $cf) {?>
                        <tr>
                          <td class='font-weight-bold'><?=$cf->name;?></td>
                          <td><?=$cf->value;?></td>
                        </tr>
                      <?}
                    }?>

                  </tbody>
                </table>
              </div>

                <?}
              }?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<? require __DIR__.'/modals/client_edit_modal.php';?>
<!-- /////////////////////////////  ADD NEW employee -->
<? require __DIR__.'/modals/client_employee_add.php';?>
<!-- EDIT CONTACT -->
<? require __DIR__.'/modals/client_employee_edit.php';?>
<!-- ADD-NEW-FIELD-TO-CLIENT-INFO-FORM -->
<? require __DIR__.'/modals/client_custom_f.php';?>


<?php $this->load->view('layouts/footer_view'); ?>
			
