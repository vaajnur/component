<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-2 page-title-box d-flex align-items-center justify-content-between">
          <div class="col-6">
            <h4 class="page-title"><?=$storage->name;?></h4>
          </div>
          <div class='col-xl-3 offset-xl-3'>
            <button type="button" data-toggle="modal" data-target="#modal-add-storage"
                    class="btn btn-block btn--md btn-success waves-effect waves-light add-provider-btn">Add storage
            </button>
          </div>
          <div class='col col-md-3 offset-md-9 mt-3'>
            <select class="form-control choose-store">
                  <? if(!empty($stores)){
                    foreach ($stores as $key => $store) {?>
                        <option value="<?=$store->id;?>" <?=$storage->id==$store->id?' selected ':'';?> ><?=$store->name;?></option>
                    <?}
                  } ?>
            </select>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-2'>
            <h5>Filter:</h5>
          </div>
          <div class='col-md-2 mb-1 mb-dm-0 offset-md-7'>
            <span id='position-filter-select'></span>
          </div>
          <div class='col-md-1 mt-1 mt-md-0'>
            <button id='reset-all-storage' class="btn-icon btn btn-block btn-secondary waves-effect">
              Reset
            </button>
          </div>
          <div class='col-12 pt-2'>
            <div class='card-box responsive-table'>
              <table id="datatable-storage"
                     class="table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>Position</th>
                  <th>QTY</th>
                </tr>
                </thead>
                <tbody>
                  <? if(!empty($components)){
                    foreach ($components as $key => $value) {?>
                      <tr>
                        <td><?=$key+1;?></td>
                        <td><?=$value->name;?></td>
                        <td><?=$value->required_qty - $value->reached_qty;?></td>
                      </tr>
                    <?}
                  } ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ADD STORAGE MODAL -->
<div class="modal fade bs-example-modal-lg"
     id='modal-add-storage'
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     style="display: none;"
     aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add storage</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="" name="add_storage" enctype="" method="post">
          <div class="form-fields">
            <div class="form-group w-100">
              <label class="col-form-label w-100">Storage name:</label>
              <div class='d-flex align-items-center'>
                <input name="name" type="text" class="form-control" placeholder="Enter the name of the storage">
                <button class="btn btn-icon btn-xs btn-danger ml-1 delete-storage-btn invisible" disabled>
                  <i class="fas fa-trash-alt"></i>
                </button>
              </div>
            </div>
          </div>
          <div>
            <button type="submit" name="send-new-storage" value="1" class="btn btn-block btn-success waves-effect width-md waves-light mb-2 mb-sm-0">Save</button>
            <button type="button"
                    data-toggle="modal"
                    data-target='#modal-add-new-storage-field'
                    class="btn btn-primary btn-block waves-effect width-md waves-light">Add a new storage</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- ADD NEW STORAGE MODAL -->
<div class="modal fade bs-example-modal-lg"
     id='modal-add-new-storage-field'
     tabindex="-1"
     role="dialog"
     style="display: none;"
     aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-dark">
      <div class="modal-header">
        <h4 class="modal-title">Add a new storage</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group w-100">
            <label class="col-form-label w-100">Storage name:</label>
            <input name="name" type="text" class="form-control" placeholder="Enter the name of the storage">
          </div>
          <div class='form-group w-100'>
            <button
                    type="button"
                    id='add-new-field_add-storage-modal'
                    data-dismiss="modal"
                    class="btn btn-success waves-effect width-md waves-light">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/footer_view'); ?>
			
