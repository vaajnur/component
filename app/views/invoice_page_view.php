<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <div class="row">
              <div class="col-md-6">
                <h4 class="page-title">№ account: 030293</h4>
              </div>
              <div class="col-md-3 text-right">
                <h4 class="page-title">Payment ratio: 0%</h4>
              </div>
              <div class="col-md-3 text-right">
                <h4 class="page-title">Sum total: 10 000</h4>
              </div>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id=""
                     class="table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>№ application</th>
                  <th>Name component</th>
                  <th>QTY</th>
                  <th>Price</th>
                  <th>Sum</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>Capacitor ceramic</td>
                  <td>500</td>
                  <td>100</td>
                  <td>5000</td>
                  <td>Not paid</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('layouts/footer_view'); ?>
			
