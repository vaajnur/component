<?php $this->load->view('layouts/header_view'); ?>


  <div class="content-page">
    <div class="content">
      <div class="container-fluid">
        <div class="row pb-3">
          <div class="col-12">
            <h4 class="page-title">Application archive</h4>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-2'>
            <h5>Filter:</h5>
          </div>
          <div class='col-md-2 mb-1 mb-dm-0 offset-md-5'>
            <span id='client-filter-select'></span>
          </div>
          <div class='col-md-2 mb-1 mb-dm-0'>
            <span id='category-filter-select'></span>
          </div>
          <div class='col-md-1 mt-1 mt-md-0'>
            <button id='reset-all-archive' class="btn-icon btn btn-block btn-secondary waves-effect">
              reset
            </button>
          </div>
          <div class='col-12 pt-2'>
            <div class='card-box table-responsive'>
              <table id="datatable-orders-journal"
                     class="table-hover table w-100 table-bordered">
                <thead>
                <tr>
                  <th>№ n\n</th>
                  <th>№ applications</th>
                  <th>Data</th>
                  <th>Client</th>
                  <th>Category</th>
                  <th>Quotation</th>
                  <th>Payment from the client </th>
                  <th>Payment to suppliers </th>
                  <th>Delivery to the storage </th>
                  <th>Shipment to the client</th>
                </tr>
                </thead>
                <tbody>
                <? foreach($orders as $k => $order){?>
                  <tr class='clickable-row' data-href='/order/<?=$order->id;?>'>
                    <td><?=$k+1;?></td>
                    <td><?=$order->order_num;?></td>
                    <td><?=$order->date;?></td>
                    <td><?=$order->customer;?></td>
                    <td><?=$order->type=='1'?'Simple':'Contract';?></td>
                    <td><?=$order->quota_status;?></td>
                    <td><?=$order->status_payin;?></td>
                    <td><?=$order->status_payout;?></td>
                    <td><?=$order->supply_status;?></td>
                    <td><?=$order->demand_status;?></td>
                  </tr>
                <?}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!--<div class="modal fade bd-example-modal-lg modal-add-client"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content p-4 mh-100 h-100">
      <div class="float-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <h3 class="text-center">Add a client</h3>
      <br>
      <form>
        <div class='row'>
          <div class='col-12'>
            <div class="form-row">
              <div class="form-group w-100">
                <label class="col-form-label w-100">Client name:</label>
                <input name='name' type="text" class="form-control" placeholder="name">
              </div>
              <div class="form-group w-100">
                <label class="col-form-label w-100">Qty transactions:</label>
                <input name='price' type="number" class="form-control" placeholder='100'>
              </div>
              <div class="form-group w-100 pt-2">
                <button class='btn btn-block btn-primary'>Add</button>
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<div class="modal fade modal-edit-client"
     tabindex="-1"
     style="display: none;"
     aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content p-4 mh-100 h-100">
      <div class="float-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <h3 class="text-center">Edit the client</h3>
      <br>
      <form>
        <div class='row'>
          <div class='col-12'>
            <div class="form-row">
              <div class="form-group w-100">
                <label class="col-form-label w-100">Client name:</label>
                <input name='name' type="text" class="form-control" placeholder="Название">
              </div>
              <div class="form-group w-100">
                <label class="col-form-label w-100">Qty transactions:</label>
                <input name='price' type="number" class="form-control" placeholder='100'>
              </div>
              <div class="form-group w-100 pt-2">
                <button class='btn btn-block btn-primary'>Save</button>
                <button type="submit" class="mt-2 btn btn-block btn&#45;&#45;md btn-danger waves-effect waves-light">Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>-->


<?php $this->load->view('layouts/footer_view'); ?>
			
