<?php defined('DIRECT') OR exit('No direct script access allowed');

class Category_model extends Model
{
	public function getCategories()
	{
		// $this->db->dump();
		return $this->db->select('*')->from('categories')->order_by('name', 'asc')->where('parent_id', 0)->get()->results();
		// return $this->db->get('categories')->results();
	}

	public function getCategoriesAll()
	{
		// $this->db->dump();
		return $this->db->select('*')->from('categories')->order_by('name', 'asc')->get()->results();
		// return $this->db->get('categories')->results();
	}

	public function getCategory($id)
	{
		$res = $this->db->select('*')->from('categories')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function addCategory($data)
	{
		$this->db->insert('categories', $data);
		return $this->db->insert_id();
	}

	public function updateCategory($data, $id){
		$this->db->where('id', $id);
		$this->db->update('categories', $data);
	}

	public function getCategoryComponents(array $id)
	{
		// debug($id);
		$res = $this->db->select('comp.*, group_concat(cf.name) as custom_f_name, group_concat(ccf.value) as custom_f_val, doc.name as document, doc.id as document_id, doc.path as document_path')->from('components_base comp')->join('custom_fields cf', 'cf.category_id=comp.category_id', 'left')->join('component_custom_fields ccf', 'ccf.cf_id=cf.id and ccf.component_id=comp.id', 'left')->join('documents doc', 'doc.id=comp.doc_id', 'left')->group_by('comp.id')->in('comp.category_id', $id)->order_by('comp.name', 'asc')->get()->results();
		return $res;
	}

	public function deleteCategory($id){
		$this->db->where('id', $id);
		$this->db->delete('categories');
		$this->db->where('parent_id', $id);
		$this->db->delete('categories');
	}
	// --------------- CF
	public function getCategoryCustomF($cat_id)
	{
		$res = $this->db->select('*')->from('custom_fields')->where('category_id', $cat_id, '=')->get()->results();
		return $res;
	}

	public function updateCustomFieldsCategory($data, $id, $name){
		$this->db->where('category_id', $id);
		$this->db->where('name', $name);
		$this->db->update('custom_fields', $data);
	}

	public function addCustomFieds($data){
		$this->db->insert('custom_fields', $data);
		return $this->db->insert_id();
	}

	public function deleteCustomField($id){
		$this->db->where('id', $id);
		$this->db->delete('custom_fields');
	}
	// add component
	public function getCompName($id)
	{
		$res = $this->db->select('name')->from('components_base')->where('id', $id, '=')->get()->row();
		return $res;
	}
	
	public function addComponent($data){
		$this->db->insert('components_base', $data);
		return $this->db->insert_id();
	}

	public function deleteComponent($id){
		$this->db->where('id', $id);
		$this->db->delete('components_base');
	}

	public function updateComponent($data, $id){
		$this->db->where('id', $id);
		$this->db->update('components_base', $data);
	}

	public function addComponentCF($data){
		$this->db->insert('component_custom_fields', $data);
		return $this->db->insert_id();
	}

	// =========

	public function addFile($data){
		$this->db->insert('documents', $data);
		return $this->db->insert_id();
	}

}
			
