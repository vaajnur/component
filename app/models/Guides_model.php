<?php defined('DIRECT') OR exit('No direct script access allowed');

class Guides_model extends Model
{
	public function getCurrencies()
	{
		return $this->db->get('currencies')->results();
	}

	public function getDeliveryPeriods()
	{
		return $this->db->get('delivery_periods')->results();
	}

	public function getProviders()
	{
		return $this->db->get('providers')->results();
	}
}
			
