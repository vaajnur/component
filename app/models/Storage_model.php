<?php defined('DIRECT') OR exit('No direct script access allowed');

class Storage_model extends Model
{
	public function getStorage($id)
	{
		$res = $this->db->select('*')->from('storage')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function getStores()
	{
		$res = $this->db->select('*')->from('storage')->get()->results();
		return $res;
	}

	public function addStorage($data){
		$this->db->insert('storage', $data);
		return $this->db->insert_id();
	}

	public function getStoreComponents($id){
		$res = $this->db->select('po.quantity as reached_qty, com.name, com.qty as required_qty')
		->from('provider_offers po')
		->join('components com', 'po.component=com.id', 'left')
		->where('po.storage', $id, '=')
		->get()->results();
		// var_dump($this->db->last_query());
		return $res;
	}
}
			
