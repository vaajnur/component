<?php defined('DIRECT') OR exit('No direct script access allowed');

class Component_model extends Model
{
	public function getComponent($id)
	{
		// $this->db->dump();
		$res = $this->db
		->select('com.*, cat.name as category, doc.name as document_name, doc.path as document_path, doc.id as document_id')
		->from('components_base com')
		->join('categories cat', 'cat.id=com.category_id', 'left')
		->join('documents doc', 'doc.id=com.doc_id', 'left')
		->where('com.id', $id, '=')
		->get()
		->row();
		return $res;
	}

	public function getCompName($id)
	{
		$res = $this->db->select('name')->from('components_base')->where('id', $id, '=')->get()->row();
		return $res;
	}
	public function getComponentCat($id)
	{
		$res = $this->db->select('category_id')->from('components_base')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function getComponentCF($id)
	{
		$res = $this->db->select('com_cf.*, cf.name')->from('component_custom_fields com_cf')->join('custom_fields cf', 'cf.id=com_cf.cf_id', 'left')->where('com_cf.component_id', $id, '=')->get()->results();
		return $res;
	}

	public function getCategoryCF($id)
	{
		$res = $this->db
		->select('cf.id')
		->from('custom_fields cf')
		->where('cf.category_id', $id, '=')
		->get()
		->results();
		return $res;
	}


	public function addComponent($data){
		$this->db->insert('components_base', $data);
		return $this->db->insert_id();
	}

	public function deleteComponent($id){
		$this->db->where('id', $id);
		$this->db->delete('components_base');
	}

	public function updateComponent($data, $id){
		$this->db->where('id', $id);
		$this->db->update('components_base', $data);
	}

	public function editComponentCF($data, $id){
		$this->db->where('id', $id);
		$this->db->update('component_custom_fields', $data);
	}
	
	
	public function addComponentCF($data){
		$this->db->insert('component_custom_fields', $data);
		return $this->db->insert_id();
	}
	// =========

	public function addFile($data){
		$this->db->insert('documents', $data);
		return $this->db->insert_id();
	}

}
			
