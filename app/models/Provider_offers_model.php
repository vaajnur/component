<?php defined('DIRECT') OR exit('No direct script access allowed');

class Provider_offers_model extends Model
{
	public function getProviderOffers($id)
	{
		return $this->db->select('prof.*, pr.name as provider, dp.period as delivery_periods')->from('provider_offers prof')->join('providers pr', 'prof.provider=pr.id', 'left')->join('delivery_periods dp', 'prof.delivery_periods=dp.id', 'left')->where('component', $id, '=')->get()->results();
	}

	public function chooseOffers($data, $offer_id){
		$this->db->where('id', $offer_id);
		$this->db->update('provider_offers', $data);
	}
}
			
