<?php defined('DIRECT') OR exit('No direct script access allowed');

class Clients_model extends Model
{
	public function getClients()
	{
		return $this->db->get('clients')->results();
	}

	public function addClient($data){
		$this->db->insert('clients', $data);
		return $this->db->insert_id();
	}

	public function deleteClient($id){
		$this->db->where('id', $id);
		$this->db->delete('clients');
	}
}
			
