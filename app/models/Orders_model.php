<?php defined('DIRECT') OR exit('No direct script access allowed');

class Orders_model extends Model
{
	public function getOrders()
	{
		$res = $this->db->select('ord.*, cl.full_name as customer')->from('orders ord')->join('clients cl', 'cl.id=ord.customer', 'left')->order_by('order_num', 'asc')->get()->results();
		return $res;
	}

	public function getRecieptOfPayment($id){
		$res = $this->db->select('*')->from('paymentin')->where('order_id', $id, '=')->get()->results();
		return $res;
	}

	public function getPaymentOut($id)
	{
		$res = $this->db->select('po.paymentout, comp.order1, ord.order_num')->from('provider_offers po')->join('components comp', 'comp.id=po.component', 'left')->join('orders ord', 'ord.id=comp.order1')->where('ord.id', $id, '=')->get()->results();
		return $res;
	}


	public function getComponents($id)
	{
		$res = $this->db->select('*')->from('components')->where('order1', $id, '=')->get()->results();
		return $res;
	}

	public function getComponentOffers($comp_id, $selected){
		return $this->db->select('po.*, p.name as provider, doc.name as document, doc.id as document_id, doc.path as document_path, storage.id as store_id, storage.name as store_name, doc2.id as supply_document_id, doc2.name as supply_document, doc2.path as supply_document_path, doc3.id as proof_foto_id, doc3.name as proof_foto, doc3.path as proof_foto_path')
		->from('provider_offers po')
		->join('storage', 'storage.id=po.storage', 'left')
		->join('providers p', 'p.id=po.provider', 'left')
		->join('documents doc', 'doc.id=po.invoicein_doc', 'left')
		->join('documents doc2', 'doc2.id=po.supply_document', 'left')
		->join('documents doc3', 'doc3.id=po.proof_foto', 'left')
		->where('component', $comp_id, '=')
		->where('selected', $selected, '=')
		->get()->results();
	}
	
}
			
