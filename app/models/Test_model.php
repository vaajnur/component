<?php defined('DIRECT') OR exit('No direct script access allowed');

class Test_model extends Model
{
	public function getTest()
	{
		// return $this->db->get('test')->results();
	}


	public function getcomponents()
	{
		return $this->db->get('components')->results();
	}

	public function getComponent($name)
	{
		$res = $this->db->select('*')->from('components_base')
		->where('name', $name, '=')
		// ->where('order1', $order, '=')
		// ->where('pn', $pn, '=')
		// ->where('code', $code, '=')
		// ->where('body', $body, '=')
		->get()->row();
		return $res;
	}

	public function updateComp($data, $id){
		$this->db->where('id', $id);
		$this->db->update('components', $data);
	}

	public function getCategories()
	{
		return $this->db->get('categories')->results();
	}
}
			
