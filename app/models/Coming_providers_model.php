<?php defined('DIRECT') OR exit('No direct script access allowed');

class Coming_providers_model extends Model
{
	public function getComingProviders()
	{
		return $this->db->select('po.quantity, po.supply_number, po.supply_date, com.name as component, ord.order_num, s.name as storage')
		->from('provider_offers po')
		->join('components com', 'com.id=po.component', 'left')
		->join('orders ord', 'ord.id=com.order1', 'left')
		->join('storage s', 's.id=po.storage', 'left')
		->where('supply_number', '', '!=')
		->get()->results();

		// return $this->db->get('coming_providers')->results();
	}

	public function getOrders()
	{
		return $this->db->get('orders')->results();
	}

	public function getStores()
	{
		return $this->db->get('storage')->results();
	}
}
			
