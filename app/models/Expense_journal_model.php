<?php defined('DIRECT') OR exit('No direct script access allowed');

class Expense_journal_model extends Model
{
	public function getExpenseJournal()
	{
		return $this->db->select('po.quantity, po.not_brak, po.supply_number, po.supply_date, com.name as component, ord.order_num, s.name as store')
		->from('provider_offers po')
		->join('components com', 'com.id=po.component', 'left')
		->join('orders ord', 'ord.id=com.order1', 'left')
		->join('storage s', 's.id=po.storage', 'left')
		// ->where('supply_number', '', '!=')
		->where('not_brak', '', '!=')
		->get()->results();
	}
	public function getExpenseJournalBrak()
	{
		return $this->db->select('po.quantity, po.brak, po.supply_number, po.supply_date, com.name as component, ord.order_num, s.name as store')
		->from('provider_offers po')
		->join('components com', 'com.id=po.component', 'left')
		->join('orders ord', 'ord.id=com.order1', 'left')
		->join('storage s', 's.id=po.storage', 'left')
		// ->where('supply_number', '', '!=')
		->where('brak', '', '!=')
		->get()->results();
	}

	public function getOrders()
	{
		return $this->db->get('orders')->results();
	}
}
			
