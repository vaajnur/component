<?php defined('DIRECT') OR exit('No direct script access allowed');

class Order_model extends Model
{
	public function getOrder($id)
	{
		$res = $this->db->select('ord.*, doc.name as demand_doc, doc.path as demand_doc_path, doc.id as demand_doc_id, doc2.name as order_document, doc2.path as order_document_path, cl.full_name as customer')->from('orders ord')->join('documents doc', 'doc.id=ord.demand_doc', 'left')->join('documents doc2', 'doc2.id=ord.order_document', 'left')->join('clients cl', 'cl.id=ord.customer', 'left')->where('ord.id', $id, '=')->get()->row();
		return $res;
	}

	public function getOrderNum2()
	{
		$res = $this->db->select('MAX(order_num_2) as order_num_2')->from('orders')->get()->row();
		return $res;
	}

	public function updateOrder($data, $id){
		$this->db->where('id', $id);
		$this->db->update('orders', $data);
	}

	public function addRecieptOfPayment($data){
		$this->db->insert('paymentin', $data);
		return $this->db->insert_id();
	}

	public function updateRecieptOfPayment($data, $id){
		$this->db->where('id', $id);
		$this->db->update('paymentin', $data);
	}

	public function getRecieptOfPayment($id){
		$res = $this->db->select('*')->from('paymentin')->where('order_id', $id, '=')->get()->results();
		return $res;
	}

	public function getComponentsAll()
	{
		$res = $this->db->select('DISTINCT(name), id, pn')->from('components_base')->get()->results();
		return $res;
	}
	public function getCompName($id)
	{
		$res = $this->db->select('name')->from('components')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function getComponents($id)
	{
		$res = $this->db->select('com.*, doc.name as document, doc.id as document_id, doc.path as document_path')
		->from('components com')
		->join('documents doc', 'doc.id=com.doc_id', 'left')
		->where('order1', $id, '=')
		->get()->results();
		return $res;
	}

	public function addComponent($data){
		$this->db->insert('components', $data);
		return $this->db->insert_id();
	}

	public function deleteComponent($id){
		$this->db->where('id', $id);
		$this->db->delete('components');
	}

	public function updateComponent($data, $id){
		$this->db->where('id', $id);
		$this->db->update('components', $data);
	}

	public function getComponentOffers($comp_id, $selected){
		return $this->db->select('po.*, p.name as provider, doc.name as document, doc.id as document_id, doc.path as document_path, storage.id as store_id, storage.name as store_name, doc2.id as supply_document_id, doc2.name as supply_document, doc2.path as supply_document_path, doc3.id as proof_foto_id, doc3.name as proof_foto, doc3.path as proof_foto_path, doc4.id as not_brak_id, doc4.name as not_brak_name, doc4.path as not_brak_path, doc5.id as brak_id, doc5.name as brak_name, doc5.path as brak_path, d_per.period')
		->from('provider_offers po')
		->join('storage', 'storage.id=po.storage', 'left')
		->join('providers p', 'p.id=po.provider', 'left')
		->join('documents doc', 'doc.id=po.invoicein_doc', 'left')
		->join('documents doc2', 'doc2.id=po.supply_document', 'left')
		->join('documents doc3', 'doc3.id=po.proof_foto', 'left')
		->join('documents doc4', 'doc4.id=po.not_brak_doc', 'left')
		->join('documents doc5', 'doc5.id=po.brak_doc', 'left')
		->join('delivery_periods d_per', 'd_per.id=po.delivery_periods', 'left')
		->where('component', $comp_id, '=')
		->where('selected', $selected, '=')
		->get()->results();
	}

	public function getComponentScoredOffers($comp){
		$res = $this->db->select('SUM(quantity) as scored')->from('provider_offers po')->where('component', $comp, '=')->where('selected', '1')->group_by('component')->get()->row();
		return $res;
	}

	public function addProviderOffer($data){
		$this->db->insert('provider_offers', $data);
		return $this->db->insert_id();		
	}

	public function updateProviderOffer($data, $id){
		$this->db->where('id', $id);
		$this->db->update('provider_offers', $data);
	}

	public function deleteProviderOffer($id){
		$this->db->where('id', $id);
		$this->db->delete('provider_offers');
	}

	public function getProvidersByName($name)
	{
		return $this->db->select('id')->from('providers')->where('name', $name)->get()->row();
	}

	public function addProvider($data){
		$this->db->insert('providers', $data);
		return $this->db->insert_id();
	}

	public function providerInvoiceSended($id){
		$res = $this->db->select('po.id, po.invoicein, po.paymentout, po.paymentout_date , doc.name as document, doc.id as document_id,doc.path as document_path')->from('components c')->where('order1', $id, '=')->join('provider_offers po', 'po.component=c.id', 'right')->join('documents doc', 'doc.id=po.invoicein_doc', 'left')->where('po.invoicein_sended', '1', '=')->get()->results();
		return $res;
	}

	public function getStorages(){
		return $this->db->get('storage')->results();
	}

	public function getCategories()
	{
		return $this->db->select('*')->from('categories')->order_by('name', 'asc')->where('parent_id', 0)->get()->results();
	}
	
	public function getCategoryComponents(array $id)
	{
		$res = $this->db->select('comp.*, doc.name as document, doc.id as document_id, doc.path as document_path')->from('components_base comp')->join('documents doc', 'doc.id=comp.doc_id', 'left')->in('comp.category_id', $id)->order_by('comp.name', 'asc')->get()->results();
		return $res;
	}

}
			
