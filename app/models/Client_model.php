<?php defined('DIRECT') OR exit('No direct script access allowed');

class Client_model extends Model
{
	public function getClient($id)
	{
		$res = $this->db->select('*')->from('clients')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function updateClient($data, $id){
		$this->db->where('id', $id);
		$this->db->update('clients', $data);
	}

	public function deleteClient($id){
		$this->db->where('id', $id);
		$this->db->delete('clients');
	}

	public function getEmployee($id)
	{
		$res = $this->db->select('*')->from('employee em')->where('client_id', $id, '=')->get()->results();
		return $res;
	}

	public function addEmployee($data){
		$this->db->insert('employee', $data);
		return $this->db->insert_id();
	}

	public function updateEmployee($data, $id){
		$this->db->where('id', $id);
		$this->db->update('employee', $data);
	}

	public function getClientCustomF($cl_id)
	{
		$res = $this->db->select('*')->from('custom_fields')->where('client_id', $cl_id, '=')->get()->results();
		return $res;
	}

	public function getCustomFieldsEmp($emp_id)
	{
		$res = $this->db->select('*')->from('custom_fields')->where('employee_id', $emp_id, '=')->get()->results();
		return $res;
	}

	public function updateCustomFieldsClient($data, $id, $name){
		$this->db->where('client_id', $id);
		$this->db->where('name', $name);
		$this->db->update('custom_fields', $data);
	}
	
	public function updateCustomFieldsEmployer($data, $id, $name){
		$this->db->where('employee_id', $id);
		$this->db->where('name', $name);
		$this->db->update('custom_fields', $data);
	}

	public function addCustomFieds($data){
		$this->db->insert('custom_fields', $data);
		return $this->db->insert_id();
	}

	public function deleteCustomField($id){
		$this->db->where('id', $id);
		$this->db->delete('custom_fields');
	}

}
			
