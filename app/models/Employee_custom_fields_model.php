<?php defined('DIRECT') OR exit('No direct script access allowed');

class Employee_custom_fields_model extends Model
{
	public function getCustomFieldsEmp($emp_id)
	{
		$res = $this->db->select('*')->from('custom_fields')->where('employee_id', $emp_id, '=')->get()->results();
		return $res;
	}
}
			
