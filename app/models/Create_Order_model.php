<?php defined('DIRECT') OR exit('No direct script access allowed');

class Create_Order_model extends Model
{
	public function addOrder($data)
	{
		$this->db->insert('orders', $data);
		return $this->db->insert_id();
		// return $this->db->get('create-order')->results();
	}

	public function getOrder($id)
	{
		$res = $this->db->select('ord.*, doc.name as document_name, doc.path as document_path, doc.id as document_id')->from('orders ord')->join('documents doc', 'doc.id=ord.order_document', 'left')->where('ord.id', $id, '=')->get()->row();
		return $res;
	}

	public function updateOrder($data, $id){
		$this->db->where('id', $id);
		$this->db->update('orders', $data);
	}

	public function deleteOrder($id){
		$this->db->where('id', $id);
		$this->db->delete('orders');
	}

	public function getNum()
	{
		$res = $this->db->select('MAX(order_num) as order_num')->from('orders')->get()->row();
		return $res;
	}
	
	public function addFile($data){
		$this->db->insert('documents', $data);
		return $this->db->insert_id();
	}
	
	public function getClients()
	{
		return $this->db->get('clients')->results();
	}

}
			
