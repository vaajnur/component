<?php defined('DIRECT') OR exit('No direct script access allowed');

class Permissions_model extends Model
{
	public function getPermissions()
	{
		return $this->db->get('roles')->results();
	}

	public function addRole($data){
		$this->db->insert('roles', $data);
		return $this->db->insert_id();
	}

	public function updatePermissions($data, $id){
		$this->db->where('id', $id);
		$this->db->update('roles', $data);
	}
}
			
