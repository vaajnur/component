<?php defined('DIRECT') OR exit('No direct script access allowed');

class Providers_model extends Model
{
	public function getProviders()
	{
		return $this->db->select('prov.*')->from('providers prov')->get()->results();
	}

	public function addProvider($data){
		$this->db->insert('providers', $data);
		return $this->db->insert_id();
	}

	public function getSpec()
	{
		return $this->db->get('specializations')->results();
	}

	public function deleteProvider($id){
		$this->db->where('id', $id);
		$this->db->delete('providers');
	}	

	public function updateProvider($data, $id){
		$this->db->where('id', $id);
		$this->db->update('providers', $data);
	}

}
			
