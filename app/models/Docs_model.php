<?php defined('DIRECT') OR exit('No direct script access allowed');

class Docs_model extends Model
{
	
	public function updateProviderOffer($data, $id){
		$this->db->where('id', $id);
		$this->db->update('provider_offers', $data);
	}

	public function updateOrder($data, $id){
		$this->db->where('id', $id);
		$this->db->update('orders', $data);
	}

	public function addFile($data){
		$this->db->insert('documents', $data);
		return $this->db->insert_id();
	}

	public function getDoc($id)
	{
		$res = $this->db->select('*')->from('documents')->where('id', $id, '=')->get()->row();
		return $res;
	}

	public function deleteDoc($id){
		$this->db->where('id', $id);
		$this->db->delete('documents');
	}

	public function deleteOfferDoc($id, $d){
		$this->db->where('id', $id);
		$this->db->update('provider_offers', $d);
	}
	public function deleteOrderDoc($id, $d){
		$this->db->where('id', $id);
		$this->db->update('orders', $d);
	}
	public function deleteComponentDoc($id, $d){
		$this->db->where('id', $id);
		$this->db->update('components', $d);
	}
}
			
