<?php defined('DIRECT') OR exit('No direct script access allowed');

class Login_model extends Model
{
	public function getUser($data)
	{
		$res = $this->db->select('users.*, roles.code')->from('users')->where('login', $data['login'])->where('password', $data['password'])->join('roles', 'roles.id=users.role', 'left')->get()->row();
		return $res;
	}
}
			
