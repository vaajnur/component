<?php defined('DIRECT') OR exit('No direct script access allowed');

class Users_model extends Model
{
	public function getUsers()
	{
		$res = $this->db->select('u.*, r.name as role_name, r.id as role_id')->from('users u')->join('roles r', 'r.id=u.role', 'left')->get()->results();
		return $res;
	}

	public function updateUser($data, $id){
		$this->db->where('id', $id);
		$this->db->update('users', $data);
	}

	public function addUser($data){
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function deleteUser($id){
		$this->db->where('id', $id);
		$this->db->delete('users');
	}


	public function getRoles()
	{
		return $this->db->get('roles')->results();
	}
}
			
