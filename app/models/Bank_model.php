<?php defined('DIRECT') OR exit('No direct script access allowed');

class Bank_model extends Model
{
	public function getPayments()
	{
		return $this->db->select('po.paymentout, po.invoicein, po.paymentout_date, SUM(rop.sum) as receipt')->from('provider_offers po')->join('components c', 'c.id=po.component', 'left')->join('paymentin rop', 'rop.order_id=c.order1', 'left')->where('invoicein', '', '!=')->group_by('po.id')->get()->results();
	}

	public function getReceiptPayments(){
		return $this->db->get('paymentin')->results();
	}

	public function getInvoicePayments(){
		return $this->db->select('*')->from('provider_offers')->where('paymentout', '', '!=')->get()->results();
	}
}
			
