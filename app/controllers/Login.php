<?php defined('DIRECT') OR exit('No direct script access allowed');

class Login extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$this->asset->set_title('Login - Компонент');
		$this->load->model('Login_model', 'login', '');
		$data1 = [];

		if ($this->input->post('send-login') == '1' && $this->input->request_method() == 'POST' ) {
			$user = $this->login->getUser([
				'login' => $this->input->post('login'),
				'password' => md5($this->input->post('password')),
			]);
			if(!empty($user)){
				$this->session->set('logined', $user);
				redirect('/');
			}
			else{
				redirect('/login');
			}
		}

		$this->load->view('login_view', $data1);
	}

	public function logout(){
		$this->session->set('logined', null);
		redirect('/login');
	}
}
			
