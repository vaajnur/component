<?php defined('DIRECT') OR exit('No direct script access allowed');

class Guides extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Guides - Component');
		$this->load->model('Guides_model', 'guides', '');
		$data1 = [];

		if ($this->input->get('get_currencies') == '1' && $this->input->request_method() == 'GET' ) {
			echo json_encode(currencyForSelect());
			exit;
		}
		if ($this->input->get('get_currencies_select') == '1' && $this->input->request_method() == 'GET' ) {
			$select = "<select class='tabledit-input form-control input-sm' name='currency' style='width:100px;'>";
			// $select .= "<option value=''></option>";
			foreach (currencyForSelect() as $k => $d) {
				$select .= "<option value='{$k}'>$d</option>";
			}
			$select .= "</select>";
			echo $select;
			exit;
		}
		// **************
		if ($this->input->get('get_delivery_periods') == '1' && $this->input->request_method() == 'GET' ) {
			$periods = $this->guides->getDeliveryPeriods();
			foreach ($periods as $per) {
				$res[$per->id] = $per->period;
			}
			echo json_encode($res);
			exit;
		}// **************
		if ($this->input->get('get_delivery_periods_select') == '1' && $this->input->request_method() == 'GET' ) {
			$periods = $this->guides->getDeliveryPeriods();
			echo $this->createSelect($periods, 'delivery_periods');
			exit;
		}// **************
		if ($this->input->get('get_providers') == '1' && $this->input->request_method() == 'GET' ) {
			$providers = $this->guides->getProviders();
			foreach ($providers as $prov) {
				$res[$prov->id] = $prov->name;
			}
			echo json_encode($res);
			exit;
		}// **************
		if ($this->input->get('get_providers_select') == '1' && $this->input->request_method() == 'GET' ) {
			$providers = $this->guides->getProviders();
			echo $this->createSelect($providers, 'provider');
			exit;
		}


		// $data1['guides'] = $this->guides->getGuides();
		// $this->load->view('guides_view', $data1);
		var_dump('guides!!');
	}

	public function createSelect($data, $name){
		$select = "<select class='tabledit-input form-control input-sm' name='$name' style='width:100px;'>";
		// $select .= "<option value=''></option>";
		foreach ($data as $d) {
			$select .= "<option value='{$d->id}'>".($name=='provider'?$d->name:$d->period)."</option>";
		}
		$select .= "</select>";
		return $select;
	}



}
			
