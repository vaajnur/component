<?php defined('DIRECT') OR exit('No direct script access allowed');

class Bank extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		if($this->session->get('logined') == false){
			redirect('/login');
		}

		// for Control
		if($this->session->get('logined')->role == '3'){
			redirect('/');
		}
		$this->asset->set_title('Bank account statement - Component');
		$this->load->model('Bank_model', 'bank', '');
		$data1 = [];
		// счета поставщиков, поступление оплаты, расход
		$data1['paymentin'] = $this->bank->getReceiptPayments();
		$data1['paymentouts'] = $this->bank->getInvoicePayments();
		// debug($data1['paymentin']);
		// debug($data1['paymentouts']);
		$this->load->view('bank_view', $data1);
	}
}
			
