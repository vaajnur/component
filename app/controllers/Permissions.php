<?php defined('DIRECT') OR exit('No direct script access allowed');

class Permissions extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Permissions - Component');
		$this->load->model('Permissions_model', 'permissions', '');
		$data1 = [];

		if ($this->input->post('add-role') == '1' && $this->input->request_method() == 'POST' ) {
			$this->permissions->addRole(['name' => $this->input->post('name')]);
			redirect(request_uri());
		}

		if ($this->input->post('edit-roles') == '1' && $this->input->request_method() == 'POST' ) {
			$data[$this->input->post('action')] = $this->input->post('action_val');
			$this->permissions->updatePermissions($data, $this->input->post('id'));
			// print_r($data);
			if($this->input->post('ajax') == true)
				die;
		}

		$data1['permissions'] = $this->permissions->getPermissions();
		$this->load->view('permissions_view', $data1);
	}
}
			
