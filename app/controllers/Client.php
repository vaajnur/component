<?php defined('DIRECT') OR exit('No direct script access allowed');

class Client extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{


		if($this->session->get('logined') == false){
			redirect('/login');
		}
		
		if (isset($_POST)) {
			// $this->log->write('7', var_export($_POST, true));
		}		


		$this->asset->set_title('Client - Component');
		$this->load->model('Client_model', 'client', '');
		$data1 = [];
		// add client
		if ($this->input->post('edit-client') == '1' && $this->input->request_method() == 'POST') {

			$required_fields = array(
				'full_name',
				'legal_address',
				'postal_address',
				'phone_fax',
				'inn',
				'kpp',
				'ogrn',
				'payment_account',
				'correspondent_account',
				'bic_of_the_bank',
				'bank',
				'fio_director',
				'email',
			);
			$another_fields = array(
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			$errors = '';
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
								$data[$item] = $this->input->post($item, false);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){
					$this->client->updateClient($data, $id);
					// redirect(request_uri());
					// if($res == true)

				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}
			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];
				$errors = $data1['mess']['text'];
			}


			// ****************************** CF 
			$d1 = [];

			$fields = $this->input->post('custom_f', false);
			if(!empty($fields)){
				$custom_f1 = $this->client->getClientCustomF($id);
				$custom_f1 = json_decode(json_encode($custom_f1), true);
				
				array_walk($fields, function($val, $key) use(&$d1, $id, $custom_f1, &$errors){
					if($key == false){
						$errors .=  '<br>'.$key.' is empty!';
					}elseif(preg_match('@[а-яА-Я]+@mi', $key.$val) == true){
						$errors .=  '<br>'.$key.$val.' is cyrillic!';
					}else{
						$d1['value'] = $val;
						if(in_array($key, array_column($custom_f1, 'name'))){
							$this->client->updateCustomFieldsClient($d1, $id, $key);
						}else{
							$d1['name'] = $key;
							$d1['client_id'] = $id;
							$this->client->addCustomFieds($d1);
						}
					}
				});
			}
			// $this->client->updateClient($data, $id);
			if($errors == false)
				redirect(request_uri());
			else
				$data1['mess'] = ['type' => 'danger', 'text' => $errors];
		}


		// ===================== add employee
		if ($this->input->post('add-employee') == '1' && $this->input->request_method() == 'POST' ) {
			// должность
			if(preg_match('@[а-яА-Я]+@mi', $this->input->post('post', false)) != true){
				$res1 = $this->client->addEmployee([
					'post' => $this->input->post('post'),
					'client_id' => $id
				]);

				// custom fields of employee
				if(!empty($this->input->post('custom_f_name', false))){
					$emp_id = $res1;
					$custom_f = $this->client->getCustomFieldsEmp($emp_id);
					$custom_f = json_decode(json_encode($custom_f), true);
					foreach ($this->input->post('custom_f_name', false) as $key => $f_name) {
							// название поля
							// значение поля
						if($f_name == false || preg_match('@[а-яА-Я]+@mi', $f_name) == true) continue;	
						$d1['value'] = $this->input->post('custom_f_value', false)[$key];
						if(in_array($f_name, array_column($custom_f, 'name'))){
							$this->client->updateCustomFieldsEmployer($d1, $emp_id, $f_name);
						}else{
							$d1['name'] = $f_name;
							$d1['employee_id'] = $emp_id;
							$this->client->addCustomFieds($d1);
						}
					}
				}
			}else{
				$data1['mess'] = ['type' => 'danger', 'text' => 'cyrillic'];
			}
			/*if(!empty($this->input->post('custom_f_name'))){

			}*/
			if($res1 >0)
				redirect(request_uri());
		}


		// =================== edit employee
		if ($this->input->post('edit-employee') == '1' && $this->input->request_method() == 'POST' ) {
			global $emp_id;
			$emp_id = $this->input->post('empl_id');
			$this->client->updateEmployee(['post' => $this->input->post('post')], $emp_id);
			// custom fields of employee
			if(!empty($this->input->post('custom_f_name', false))){
				$custom_f = $this->client->getCustomFieldsEmp($emp_id);
				$custom_f = json_decode(json_encode($custom_f), true);
				foreach ($this->input->post('custom_f_name', false) as $key => $f_name) {
						// название поля
						// значение поля
					if($f_name == false || preg_match('@[а-яА-Я]+@mi', $f_name) == true) continue;	
					$d1['value'] = $this->input->post('custom_f_value', false)[$key];
					// field already exist
					if(in_array($key, array_column($custom_f, 'name'))){
						$d1['name'] = $f_name;
						$this->client->updateCustomFieldsEmployer($d1, $emp_id, $key);
					}else{
						$d1['name'] = $f_name;
						$d1['employee_id'] = $emp_id;
						$this->client->addCustomFieds($d1);
					}
				}
			}
		}

		// del cf
		if ($this->input->post('delete-cf') == '1' && $this->input->request_method() == 'POST' ) {
			$this->client->deleteCustomField($this->input->post('id'));
		}

		$data1['client'] = $this->client->getClient($id);
		$data1['client']->custom_f = $this->client->getClientCustomF($id);
		$data1['employers'] = $this->client->getEmployee($id);
		foreach ($data1['employers'] as $key => &$employe) {
			$employe->custom_fields = $this->client->getCustomFieldsEmp($employe->id);
		}
		$data1['base_fields'] = array(
			'full_name' => 'Client\'s name:',
			'legal_address' => 'Legal address:',
			'postal_address' => 'Postal address:',
			'phone_fax' => 'Phone / Fax number:',
			'inn' => 'INN:',
			'kpp' => 'KPP:',
			'ogrn' => 'OGRN:',
			'payment_account' => 'Payment account:',
			'correspondent_account' => 'Correspondent account:',
			'bic_of_the_bank' => 'BIC of the bank:',
			'bank' => 'Bank:',
			'fio_director' => 'Full name of the Director:',
			'email' => 'Email:',
		);
		// debug($data1['employers']);
		$this->load->view('client_view', $data1);
	}
}
			
