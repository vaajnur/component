<?php defined('DIRECT') OR exit('No direct script access allowed');

class Manager_order_page extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Manager order page - Component');
		$this->load->model('Manager_order_page_model', 'manager_order_page', '');
		$data1 = [];
		$data1['manager_order_page'] = $this->manager_order_page->getManagerOrderPage();
		$this->load->view('manager_order_page_view', $data1);
	}
}
			
