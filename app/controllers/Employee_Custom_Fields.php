<?php defined('DIRECT') OR exit('No direct script access allowed');

class Employee_custom_fields extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Employee custom fields - Component');
		$this->load->model('Employee_custom_fields_model', 'employee_custom_fields', '');
		$data1 = [];
		$data1['employee_custom_fields'] = $this->employee_custom_fields->getCustomFieldsEmp($id);
		$this->load->view('employee_custom_fields_view', $data1);
	}
}
			
