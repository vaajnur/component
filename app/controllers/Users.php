<?php defined('DIRECT') OR exit('No direct script access allowed');

class Users extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Users - Component');
		$this->load->model('Users_model', 'users', '');
		$data1 = [];

		if ( ($this->input->post('edit-user') == '1' || $this->input->post('add-user') == '1' ) && $this->input->request_method() == 'POST' ) {
			$id = $this->input->post('id');
			$fields = ['fio', 'login', 'password', 'role'];
			$data = [];
			array_walk($fields, function($item) use(&$data){
				if($this->input->post($item, false) != ''){
					if($item == 'password')
						$data[$item] = md5(trim($this->input->post($item, false)));
					else
						$data[$item] = $this->input->post($item, false);
				}
			});
			if($this->input->post('edit-user') == '1'){
				$this->users->updateUser($data, $id);			
				$res1 = 1;
			}elseif($this->input->post('add-user') == '1'){
				$res1 = $this->users->addUser($data);			
			}
			if($res1>0)
				$data1['mess'] = ['type' => 'success', 'text' => ''];	
		}

		if ($this->input->post('del-user') == '1' && $this->input->request_method() == 'POST' ) {
			$this->users->deleteUser($this->input->post('id'));
		}

		$data1['users'] = $this->users->getUsers();
		$data1['roles'] = $this->users->getRoles();
		if($data1['mess']['type'] == 'success')
			redirect(request_uri());
		$this->load->view('users_view', $data1);
	}
}
			
