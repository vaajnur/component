<?php defined('DIRECT') OR exit('No direct script access allowed');

class Invoice_page extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Account page - Component');
		$this->load->model('Invoice_page_model', 'invoice_page', '');
		$data1 = [];
		$data1['invoice_page'] = $this->invoice_page->getInvoice_page();
		$this->load->view('invoice_page_view', $data1);
	}
}
			
