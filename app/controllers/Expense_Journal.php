<?php defined('DIRECT') OR exit('No direct script access allowed');

class Expense_journal extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Expense log - Component');
		$this->load->model('Expense_journal_model', 'expense_journal', '');
		$data1 = [];


		$data1['orders'] = $this->expense_journal->getOrders();
		$data1['expense_journal'] = $this->expense_journal->getExpenseJournal();
		$data1['expense_journal_brak'] = $this->expense_journal->getExpenseJournalBrak();
		$this->load->view('expense_journal_view', $data1);
	}
}
			
