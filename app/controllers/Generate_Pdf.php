<?php defined('DIRECT') OR exit('No direct script access allowed');

class Generate_pdf extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Generate_pdf - Компонент');
		$this->load->model('Generate_pdf_model', 'generate_pdf', '');
		$data1 = [];
		$order = $this->generate_pdf->getOrder($id);		
		$components = $this->generate_pdf->getComponents($id);
		// входящий платеж
		$paymentin = $this->generate_pdf->getRecieptOfPayment($id);
		// сумма всех оплат
		$paymentin_sum = 0;

		foreach ($paymentin as $key => $payment) {
			$paymentin_sum += (float)$payment->sum;
		}

		$summary_price = 0;
		// формирование счета 
		foreach($components as &$item){
			$item->offers = $this->generate_pdf->getComponentOffers($item->id, '1');
			$item->scored = $this->generate_pdf->getComponentScoredOffers($item->id)->scored;
			// $item->offers_not_selected = $this->order->getComponentOffers($item->id, '0');
			
			foreach ($item->offers as  &$offer) {
				if($offer->profit_margin !== '')
					$margin = $offer->profit_margin;
				elseif($profit_margin !== '')
					$margin = $profit_margin;
				else
					$margin = 0;
			
				$offer->sum = ($offer->price/100*$margin*$offer->quantity) + ($offer->price*$offer->quantity);
                $summary_price += $offer->sum;
			}
		}

		$logistic_price = $order->logistic_price;
		// плюсую логистику
		$summary_price += $logistic_price;
		// оплачено %
		$payed = $paymentin_sum/$summary_price*100;
		$payed = number_format($payed, 0, '.', ' ');
		$payed_tail = 100 - $payed;
		// к оплате руб.
		$prepayment = $order->prepayment;
		$for_pay = $summary_price/100*$prepayment - $paymentin_sum;
		// if($for_pay < 0) $for_pay = 0;

		$components_html = '';
		$amount_sum = 0;
		foreach($components as $k => $comp){
			$amount = $comp->qty*$comp->unit_price;
			$amount_sum += $amount;
			$components_html .= '<tr class="row17">
            <td class="column0 style39 n">'.($k+1).'</td>
            <td class="column1 style40 s">'.$comp->name.'</td>
            <td class="column2 style41 n">'.$comp->qty.'</td>
            <td class="column3 style42 n">'.$comp->unit_price.'</td>
            <td class="column4 style43 s">EA</td>
            <td class="column5 style44 f">'.$amount.'</td>
            <td class="column6 style45 s">'.$comp->manufacturer.'</td>
            <td class="column7">&nbsp;</td>
            <td class="column8">&nbsp;</td>
          </tr>';
		}


		$mpdf = new \Mpdf\Mpdf([
				 'mode' => 'utf-8',
			    // 'format' => [190, 236],
			    'orientation' => 'L'
		]);
		// $mpdf->showImageErrors = true;
		$style = file_get_contents(ROOT_DIR.'/public/invoice/8302.css');
		$html = file_get_contents(ROOT_DIR.'/public/invoice/8302.html');
		$date = new DateTime($order->date);
		$date_f = $date->format('Y-m-d');
		$html = str_replace('#DATE#', $date_f, $html);
		$html = str_replace('#COMPONENTS#', $components_html, $html);
		$html = str_replace('#BANK#', $order->bank, $html);
		$html = str_replace('#BANK_ACCOUNT#', $order->payment_account, $html);
		$html = str_replace('#CUSTOMER_NAME#', $order->full_name, $html);
		$html = str_replace('#CUSTOMER_ADDR#', $order->legal_address, $html);
		$html = str_replace('#AMOUNT#', $amount_sum, $html);
		$html = str_replace('#PAYED#', $payed, $html);
		$html = str_replace('#PAYED_TAIL#', $payed_tail, $html);
		$html = str_replace('#DEMAND_DATE#', $order->demand_date, $html);
		// var_dump($html);
		$mpdf->WriteHTML($style, 1);
		$mpdf->WriteHTML($html, 2);
		// $mpdf->AddPage('L', '', '', '', '', 0, 0, 0, 0, 0, 0, null, null, null, null, null, null, null, null, 'second', 'A4-L');
		$mpdf->Output();


		// $this->load->view('generate_pdf_view', $data1);
	}
}
			
