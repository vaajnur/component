<?php defined('DIRECT') OR exit('No direct script access allowed');

class Coming_providers extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	

		if($this->session->get('logined') == false){
			redirect('/login');
		}
		
		$this->asset->set_title('Receipt log from provider - Component');
		$this->load->model('Coming_providers_model', 'coming_providers', '');
		$data1 = [];
		if ($this->input->post('add-new-supply') == '1' && $this->input->request_method() == 'POST' ) {
			$fields = ['supply_number', 'supply_date', 'storage', 'document'];
			$fields2 = ['component', 'order_id'];
			// $compo_id = $this->coming_providers->addComponent();
			// $supl_id = $this->coming_providers->addOffer();

		}


		$data1['coming_providers'] = $this->coming_providers->getComingProviders();
		$data1['orders'] = $this->coming_providers->getOrders();
		$data1['storages'] = $this->coming_providers->getStores();
		// debug($data1['coming_providers']);
		$this->load->view('coming_providers_view', $data1);
	}
}
			
