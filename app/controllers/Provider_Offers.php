<?php defined('DIRECT') OR exit('No direct script access allowed');

class Provider_offers extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($component_id)
	{	
		// ajax
		$this->asset->set_title('Provider offers - Component');
		$this->load->model('Provider_offers_model', 'provider_offers', '');
		$data1 = [];
		$data1['provider_offers'] = $this->provider_offers->getProviderOffers($component_id);
		$this->load->view('provider_offers_view', $data1);
	}

	public function choose_provider_offer($offer_id, $selected){
		// ajax
		$this->load->model('Provider_offers_model', 'provider_offers', '');
		$data['selected'] = $selected;
		$data1['res'] = $this->provider_offers->chooseOffers($data, $offer_id);
	}
}
			
