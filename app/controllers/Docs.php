<?php defined('DIRECT') OR exit('No direct script access allowed');

class Docs extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	

		if($this->session->get('logined') == false){
			redirect('/login');
		}
		
		$this->asset->set_title('Docs - Component');
		$this->load->model('Docs_model', 'docs', '');
		$data1 = [];

		if (isset($_FILES)) {
			// $this->log->write('7', var_export($_FILES, true));
		}

		if (isset($_POST)) {
			// $this->log->write('7', var_export($_POST, true));
		}

		$this->upload->config([
			'upload_path'  => ROOT_DIR . 'public/upload', 
			// 'allowed_types' => 'pdf'
		]);
				// docs
		if(current($_FILES['invoicein_doc']['error'])[0] === 0){
			$this->uploadDoc('invoicein_doc');
		}
		if(current($_FILES['supply_document']['error']) === 0){
			$this->uploadDoc('supply_document');
		}
		if(current($_FILES['demand_document']['error'])[0] === 0){
			$this->uploadDoc('demand_document');
		}
		if(current($_FILES['order_document']['error'])[0] === 0){
			// $this->uploadDoc('order_document');
		}
		if(current($_FILES['proof_foto']['error']) === 0){
			$this->uploadDoc('proof_foto');
		}
		if(current($_FILES['not_brak_files']['error']) === 0){
			$this->uploadDoc('not_brak_files');
		}
		if(current($_FILES['brak_files']['error']) === 0){
			$this->uploadDoc('brak_files');
		}

		// delete offer doc
		if ($this->input->post('delete-doc') == '1' && $this->input->request_method() == 'POST' ) {
			$doc = $this->docs->getDoc($this->input->post('doc_id'));
				// debug($doc);
			if($doc  != false){
				$path = ROOT_DIR.$doc->path;
				if(file_exists($path))
					unlink($path);
				$this->docs->deleteDoc($doc->id);
			}
			if($this->input->post('type') == 'supply_document' ){
				$this->docs->deleteOfferDoc($this->input->post('offer_id'), ['supply_document' => '']);
			}elseif($this->input->post('type') == 'invoicein_doc'){
				$this->docs->deleteOrderDoc($this->input->post('offer_id'), ['invoicein_doc' => '']);
			}elseif($this->input->post('type') == 'demand_doc'){
				$this->docs->deleteOrderDoc($this->input->post('offer_id'), ['demand_doc' => '']);
			}elseif($this->input->post('type') == 'order_document'){
				$this->docs->deleteOrderDoc($this->input->post('offer_id'), ['order_document' => '0']);
			}elseif($this->input->post('type') == 'proof_foto'){
				$this->docs->deleteOrderDoc($this->input->post('offer_id'), ['proof_foto' => '']);
			}elseif($this->input->post('type') == 'not_brak_doc'){
				$this->docs->deleteOfferDoc($this->input->post('offer_id'), ['not_brak_doc' => '']);
			}elseif($this->input->post('type') == 'brak_doc'){
				$this->docs->deleteOfferDoc($this->input->post('offer_id'), ['brak_doc' => '']);
			}elseif($this->input->post('type') == 'component_document'){
				$this->docs->deleteComponentDoc($this->input->post('offer_id'), ['doc_id' => '']);
			}
			echo "deleted";
			die;
		}


		// $data1['docs'] = $this->docs->getDocs();
		// $this->load->view('docs_view', $data1);
	}

	public function uploadDoc($doc){
		$file = [];
		$offer_id = key($_FILES[$doc]['name']);
		// если из таблицы то массив
		if(is_array($_FILES[$doc]['name'][$offer_id])){
			$name = $_FILES[$doc]['name'][$offer_id][0];
			$file = [
				'name' => $name,
				'type' => $_FILES[$doc]['type'][$offer_id][0],
				'error' => $_FILES[$doc]['error'][$offer_id][0],
				'size' => $_FILES[$doc]['size'][$offer_id][0],
				'tmp_name' => $_FILES[$doc]['tmp_name'][$offer_id][0]
			];
		}else{
			// из модалки
			$name = $_FILES[$doc]['name'][$offer_id];
			$file = [
				'name' => $name,
				'type' => $_FILES[$doc]['type'][$offer_id],
				'error' => $_FILES[$doc]['error'][$offer_id],
				'size' => $_FILES[$doc]['size'][$offer_id],
				'tmp_name' => $_FILES[$doc]['tmp_name'][$offer_id]
			];
		}
		$this->upload->handle($file);
		if($this->upload->getErrors())
			debug($this->upload->getErrors());
		$file2['path'] = '/public/upload/'.$name;
		$file2['type'] = $doc;
		$file2['name'] = $name;
		// $file_id[] = $this->docs->addDocs($file);
		$res = $this->docs->addFile($file2);
		if(in_array($doc, ['invoicein_doc', 'supply_document', 'proof_foto', 'not_brak_files', 'brak_files']) && $res != false){
			if($doc == 'not_brak_files')
				$doc = 'not_brak_doc';
			elseif($doc == 'brak_files')
				$doc = 'brak_doc';
			$this->docs->updateProviderOffer([$doc => $res], $offer_id);
		}elseif(in_array($doc, ['demand_document', 'order_document']) && $res != false){
			if($doc == 'demand_document')
				$doc = 'demand_doc';
			$this->docs->updateOrder([$doc => $res], $offer_id);
		}
		// debug($res);
		echo json_encode(['id' => $res, 'offer_id' => $offer_id, 'type' => $doc]);
		// debug($file);
		die;
	}
}
			
