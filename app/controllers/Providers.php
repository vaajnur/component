<?php defined('DIRECT') OR exit('No direct script access allowed');

class Providers extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	

		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Providers - Component');
		$this->load->model('Providers_model', 'providers', '');
		$data1 = [];
		if ($this->input->post('send-new-provider') == '1' && $this->input->request_method() == 'POST' ) {
			$res1 = $this->providers->addProvider([
				'name' => $this->input->post('name'), 
				// 'specialization' => $this->input->post('specialization')
			]);
			if($res1>0)
				$data1['mess'] = ['type' => 'success', 'text' => ''];
		}

		if ($this->input->post('del') == 'Y' && $this->input->post('id') != '' && $this->input->request_method() == 'POST' ) {
			$this->providers->deleteProvider($this->input->post('id'));
		}

		if ($this->input->post('edit-provider') == '1' && $this->input->post('id') && $this->input->request_method() == 'POST' ) {
			$id = $this->input->post('id');
			$this->providers->updateProvider([
				'name' => $this->input->post('name'),
				// 'specialization' => $this->input->post('specialization'),
			], $id);
		}

		$data1['providers'] = $this->providers->getProviders();
		// echo 123;
		// $data1['specialization'] = $this->providers->getSpec();
		$this->load->view('providers_view', $data1);
	}
}
			
