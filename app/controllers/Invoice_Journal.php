<?php defined('DIRECT') OR exit('No direct script access allowed');

class Invoice_Journal extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Invoice_journal - Компонент');
		$this->load->model('Invoice_journal_model', 'invoicein', '');
		$data1 = [];

		$data1['providers_invoices'] = $this->invoicein->getProvidersInvoices();
		// debug($data1['invoicein']);
		$this->load->view('invoice_journal_view', $data1);
	}
}
			
