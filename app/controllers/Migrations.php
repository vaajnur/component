<?php defined('DIRECT') OR exit('No direct script access allowed');

class Migrations extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$this->asset->set_title('Migrations - Component');
		$this->load->model('Migrations_model', 'migrations', '');
		$data1 = [];
		$data1['migrations'] = $this->migrations->getMigrations();
		$this->load->view('migrations_view', $data1);
	}
}
			
