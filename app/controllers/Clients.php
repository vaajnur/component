<?php defined('DIRECT') OR exit('No direct script access allowed');

class Clients extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	

		if($this->session->get('logined') == false){
			redirect('/login');
		}

		// for Control
		if($this->session->get('logined')->role == '3'){
			redirect('/');
		}

		$this->asset->set_title('Clients - Component');
		$this->load->model('Clients_model', 'clients', '');
		$data1 = [];

		if ($this->input->post('send-new-client') == '1' && $this->input->request_method() == 'POST') {
			$required_fields = array(
				'full_name',
				'legal_address',
				'postal_address',
				'phone_fax',
				'inn',
				'kpp',
				'ogrn',
				'payment_account',
				'correspondent_account',
				'bank',
				'bic_of_the_bank',
				'fio_director',
				'email',
			);
			$another_fields = array(
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
								$data[$item] = $this->input->post($item, false);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){
					$res = $this->clients->addClient($data);
					if($res == true)
						redirect(request_uri());

				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}
			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];
			}

				// $data1['mess'] = ['type' => 'success', 'text' => 'good'];
		}

		// del
		if ($this->input->post('del') == 'Y' && $this->input->request_method() == 'POST' ) {
			$this->clients->deleteClient($this->input->post('id'));
		}


		$data1['clients'] = $this->clients->getClients();
		$this->load->view('clients_view', $data1);
	}
}
			
