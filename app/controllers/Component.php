<?php defined('DIRECT') OR exit('No direct script access allowed');

class Component extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Component - Компонент');
		$this->load->model('Component_model', 'component', '');
		$data1 = [];


		if (!empty($_POST)) {
			// $this->log->write('7', var_export($_POST, true));
		}

		$this->upload->config([
			'upload_path'  => ROOT_DIR . 'public/upload', 
			// 'allowed_types' => 'pdf'
		]);

		if(  $this->input->post('edit_component') == '1' && $this->input->post('id') !== null && $this->input->request_method() == 'POST'){
			$required_fields = array(
				'name',
			);
			$another_fields = array(
				'qty',
				'position_on_pcb',
				'pn',
				'remark',
				'substitutes',
				'code',
				'dc',
				'body',
				'mfg',
				'qty_in_packing',
				'qty_to_buy_pcs',
				'unit_price',
				'sum',
				'lead_time_days',
				'weight_per_1_unit_kg',
				'tp',
				'voltage',
				'variation',
				'manufacturer',
				'year_of_production',
				'weight',
				'deviation',
				'marking',
				'comment',
				// 'date_arrival',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
								$data[$item] = $this->input->post($item, false);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){				
					//-/ --------/ новый компонент
					if(is_numeric($this->input->post('name', false))){
						$name = $this->component->getCompName($this->input->post('name', false));	 	
						debug($name);
						$data['name'] = $name->name;
					}
					if( $this->input->post('id') > 0){
						$this->component->updateComponent($data, $this->input->post('id'));
						// общие для всей категории кастомные поля
						$fields = $this->input->post('custom_f', false);
						if (!empty($fields) && $this->input->request_method() == 'POST' ) {
							$d1 = [];
							array_walk($fields, function($val, $key) use(&$d1, $id){
								$d1['value'] = $val;
								$this->component->editComponentCF($d1, $key);
							});
						}
						// docs
						$up_res = $this->uploadDoc();
						// echo 456;
						if($up_res == true)
							$this->component->updateComponent(['doc_id' => $up_res], $this->input->post('id'));

						redirect(request_uri());
					}
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}

			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];
			}
			// echo json_encode($data1['mess']);
			// exit;
		}

		$data1['component'] = $this->component->getComponent($id);
		$data1['component']->custom_f = $this->component->getComponentCF($id);
		// если нет кастомных полей
		if(empty($data1['component']->custom_f)){
			$cat_id = $data1['component']->category_id;
			$fields = $this->component->getCategoryCF($cat_id);;
			$d1['component_id'] = $id;
			array_walk($fields, function($val) use(&$d1){
				$d1['cf_id'] = $val->id;
				$d1['value'] = '';
				$this->component->addComponentCF($d1);
			});
		}
		$this->load->view('component_view', $data1);
	}

	public function delete($id){
		$this->load->model('Component_model', 'component', '');
		$cat_id = $this->component->getComponentCat($id);
		$this->component->deleteComponent($id);
		redirect('/category/'.$cat_id->category_id);
	}

	/**
	 * [uploadDoc description]
	 * @param  [type] 'component_document' [description]
	 * @return [type]      [description]
	 */
	public function uploadDoc(){
		$file = [];
		// если из таблицы то массив
		if(is_array($_FILES['component_document']['name'])){
			$name = $_FILES['component_document']['name'][0];
			$file = [
				'name' => $name,
				'type' => $_FILES['component_document']['type'][0],
				'error' => $_FILES['component_document']['error'][0],
				'size' => $_FILES['component_document']['size'][0],
				'tmp_name' => $_FILES['component_document']['tmp_name'][0]
			];
		}else{
			// из модалки
			$name = $_FILES['component_document']['name'];
			$file = [
				'name' => $name,
				'type' => $_FILES['component_document']['type'],
				'error' => $_FILES['component_document']['error'],
				'size' => $_FILES['component_document']['size'],
				'tmp_name' => $_FILES['component_document']['tmp_name']
			];
		}
		if($name == false) return false;
		$this->upload->handle($file);
		if($this->upload->getErrors())
			debug($this->upload->getErrors());
		$file2['path'] = '/public/upload/'.$name;
		$file2['type'] = 'component_document';
		$file2['name'] = $name;
		// $file_id[] = $this->docs->addDocs($file);
		$res = $this->component->addFile($file2);
		return $res;
	}	

}
			
