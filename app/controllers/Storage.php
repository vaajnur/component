<?php defined('DIRECT') OR exit('No direct script access allowed');

class Storage extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Storage - Component');
		$this->load->model('Storage_model', 'storage', '');
		$data1 = [];
		// debug($_POST);
		if($this->input->post('send-new-storage') == '1' && $this->input->request_method() == 'POST'){
			if(preg_match('@[а-яА-Я]+@mi', $this->input->post('name', false)) != true){
				$this->storage->addStorage(['name' => $this->input->post('name', false)]);
				redirect(request_uri());
			}
			else{
				echo "cyrillic!";
			}
		}
		// debug($id);
		$data1['storage'] = $this->storage->getStorage($id);
		$data1['stores'] = $this->storage->getStores();
		if($id != 'storage')
			$data1['components'] = $this->storage->getStoreComponents($id);
		// debug($data1['stores']);
		$this->load->view('storage_view', $data1);
	}
}
			
