<?php defined('DIRECT') OR exit('No direct script access allowed');

class Order extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{	
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Order - Component');
		$this->load->model('Order_model', 'order', '');
		// $this->session->set('logined', ['name'=>'aaa','role'=>'admin', 'id'=>1]);

		$data1 = [];

		if (!empty($_POST)) {
			$this->log->write('7', var_export($_POST, true));
		}


		// echo "string";
		// new component
		if(  $this->input->post('action') == 'edit_component' && $this->input->post('id') !== null && $this->input->post('name') != '' && $this->input->post('qty') !== null && $this->input->request_method() == 'POST'){
			$required_fields = array(
				'name',
				'component_id',
			);
			$another_fields = array(
				'qty',
				'position_on_pcb',
				'pn',
				'doc_id',
				'remark',
				'substitutes',
				'code',
				'dc',
				'body',
				'mfg',
				'qty_in_packing',
				'qty_to_buy_pcs',
				'unit_price',
				'sum',
				'lead_time_days',
				'weight_per_1_unit_kg',
				'tp',
				'voltage',
				'variation',
				'manufacturer',
				'year_of_production',
				'marking'
				// 'date_arrival',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
								$data[$item] = $this->input->post($item, false);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){				
					//-/ --------/ новый компонент
					if(is_numeric($this->input->post('name', false))){
						$name = $this->order->getCompName($this->input->post('name', false));	 	
						// debug($name);
						$data['name'] = $name->name;
					}
					if( $this->input->post('id') == ''){
						$data['order1'] = $id;
						$res = $this->order->addComponent($data);
					}elseif( trim($this->input->post('id')) > 0){
						$this->order->updateComponent($data, trim($this->input->post('id')));
						$res = 1;
					}
					if($res > 0)
						$data1['mess'] = [ 'type' => 'success', 'text' => 'insert component success!'];
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}

			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];
			}
			echo json_encode($data1['mess']);
			exit;
		}



		if ($this->input->post('action') == 'delete_component' && $this->input->request_method() == 'POST' ) {
			$this->order->deleteComponent($this->input->post('id'));
		}
		
		if ($this->input->post('action') == 'delete_prov_offer' && $this->input->request_method() == 'POST' ) {
			$this->order->deleteProviderOffer($this->input->post('id'));
		}

		// 1 comment
		if ($this->input->post('save-order') == '1' && $this->input->request_method() == 'POST' ) {
			if(preg_match('@[а-яА-Я]+@mi', $this->input->post('comment')) != true){
				$data['comment'] = $this->input->post('comment');
				$this->order->updateOrder($data, $id);
				$data1['mess']['type'] = 'success';		
			}
		}

		// 1 to engineer
		if ($this->input->post('send-to-engineer') == '1' && $this->input->request_method() == 'POST' ) {
			$data = [];
			$data['to_engineer'] = 1;
			$data['last_step'] = 2;
			$num = $this->order->getOrderNum2();
			// отдельный номер G000001 после отправки инженеру
			if( $num->order_num_2 != false )
				$data['order_num_2'] = (int)($num->order_num_2)+1;
			else
				$data['order_num_2'] = 100;
			$this->order->updateOrder($data, $id);
			$data1['mess']['type'] = 'success';		
		}
		// 2
		if($this->input->post('to-control') == '1' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['to_control' => 1, 'last_step' => 3], $id);
			$data1['mess']['type'] = 'success';	
		}

		// 3
		if($this->input->post('send-to-manager') == '1' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['to_manager' => 1, 'last_step' => 4], $id);
			$data1['mess']['type'] = 'success';	
		}
		// 4
		if($this->input->post('send-to-logist') == '1' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['to_logist' => 1, 'last_step' => 5], $id);
			$data1['mess']['type'] = 'success';	
		}
		// 5
		if($this->input->post('send-purchase-order') == '1' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['purchase_order' => 1, 'last_step' => 9], $id);
			$data1['mess']['type'] = 'success';	
		}

		//  new offer from provider ==============================
		if(  $this->input->post('action') == 'edit' && $this->input->post('provider') !== '' && $this->input->request_method() == 'POST'){
			$required_fields = array(
				'component',
				'provider',
			);
			if( $this->input->post('id') != false){
				unset($required_fields[0]);
			}
			$another_fields = array(
				'quantity',
				'price',
				'currency',
				'delivery_periods',
				'weight',
				'volume',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
							$data[$item] = $this->input->post($item);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){
					// check provider exist
					if(!is_numeric($data['provider'])){
						$prov_id = $this->order->getProvidersByName($data['provider'])->id;
						if($prov_id == false){
							$prov_id = $this->order->addProvider(['name' => $data['provider'], 'specialization' => '']);
						}
						$data['provider'] = $prov_id;
					}

					// convert currency
					if($data['currency'] != false && $data['price'] != false){
						$data['price'] = convertCurrency($data['price'] , $data['currency']);
						$data['currency'] = '$';
					}
					// $this->log->write(7, var_export($data, true));


					// add | edit
					if( $this->input->post('id') == false){
						$res = $this->order->addProviderOffer($data);
						// $data['order1'] = $id;
					}elseif( $this->input->post('id') > 0){
						unset($data['id']);
						// debug($data);
						$this->order->updateProviderOffer($data, $this->input->post('id'));
						$res = 1;
					}
					if($res > 0)
						$data1['mess'] = [ 'type' => 'success', 'text' => 'insert offer success'];
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>'];
				}
			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];	
			}
			echo json_encode($data1['mess']);
			exit;
		}

		// logist price
		if($this->input->post('send-logistics-price') == '1' && $this->input->request_method() == 'POST'){
			$data['logistic_price'] = $this->input->post('price-order-logist', false) == false ? 0 : $this->input->post('price-order-logist', false) ;
			$data['last_step'] = '6';
			$this->order->updateOrder($data, $id);
			$data1['mess']['type'] = 'success';
		}

		// маржа %
		if($this->input->post('profit_margin') != '' && $this->input->request_method() == 'POST'){
			$data['profit_margin'] = $this->input->post('profit_margin');
			$data['profit_margin_applied'] = 0;
			if($this->input->post('profit_margin_applied') == '1')
				$data['profit_margin_applied'] = 1;

			$this->order->updateOrder($data, $id);
			$data1['mess']['type'] = 'success';		
		}	

		if ($this->input->post('create_invoice') == '1' && $this->input->request_method() == 'POST' ) {
			$this->order->updateOrder(['last_step' => '7'], $id);
			
		}

		if (!empty($this->input->post('margin_arr', false)) && $this->input->request_method() == 'POST' ) {
			// echo "string111";
			foreach ($this->input->post('margin_arr', false) as $offer_id => $value) {
				$this->order->updateProviderOffer(['profit_margin' => $value], $offer_id);
			}
			exit;
		}	

		// предоплата %
		if($this->input->post('prepayment') != '' && $this->input->request_method() == 'POST'){
			$data['prepayment'] = $this->input->post('prepayment');
			$this->order->updateOrder($data, $id);
			$data1['mess']['type'] = 'success';		
		}	

		// 7 поступившие оплаты
		if($this->input->post('consumption') != '' && $this->input->request_method() == 'POST'){
			$data['order_id'] = $id;
			$data['sum'] = $this->input->post('consumption');
			if(trim($this->input->post('date')) != '')
				$data['date'] = trim($this->input->post('date'));
			if($this->input->post('id') != '')
				$this->order->updateRecieptOfPayment($data, $this->input->post('id'));
			else	
				$this->order->addRecieptOfPayment($data);
			$this->order->updateOrder(['last_step' => 8], $id);
		}


		// 9 счета поставщиков
		if( ($this->input->post('invoicein') != '' ||  $this->input->post('invoicein_doc_date')) && $this->input->request_method() == 'POST' && $this->input->post('id') > 0){
			debug([
				'invoicein' => $this->input->post('invoicein'), 
				'invoicein_doc_date' => $this->input->post('invoicein_doc_date') 
			]);
			$this->order->updateProviderOffer([
				'invoicein' => $this->input->post('invoicein'), 
				'invoicein_doc_date' => $this->input->post('invoicein_doc_date') 
			], $this->input->post('id'));
			$this->order->updateOrder(['last_step' => 10], $id);
		}

		// 9 счета поставщиков Отправить
		if($this->input->post('submit_invoicein') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateProviderOffer(['invoicein_sended' => 1], $this->input->post('submit_invoicein'));
			$this->order->updateOrder(['last_step' => 10], $id);
		}	
		// 10 оплата счетов
		if($this->input->post('paymentout') != '' && $this->input->request_method() == 'POST' && $this->input->post('id') > 0){
			$data['paymentout'] = $this->input->post('paymentout');
			if(trim($this->input->post('date')) != '')
				$data['paymentout_date'] = trim($this->input->post('date'));
			$this->order->updateProviderOffer($data, $this->input->post('id'));
			$this->order->updateOrder(['last_step' => 11], $id);
		}
		// 11  дата отправки
		if($this->input->post('shipment_date') != '' && $this->input->request_method() == 'POST' && $this->input->post('id') > 0){
			$data['shipment_date'] = trim($this->input->post('shipment_date'));
			$this->order->updateProviderOffer($data, $this->input->post('id'));
			$this->order->updateOrder(['last_step' => 12], $id);
		}
		// 11  приход / ред. приход
		if(  $this->input->post('add-edit-supply') == '1' && $this->input->request_method() == 'POST' && $this->input->post('id') > 0){
			// № приемки
			$data['supply_number'] = trim($this->input->post('supply_number'));
			$data['supply_date'] = trim($this->input->post('supply_date'));
			// склад
			$data['storage'] = trim($this->input->post('storage'));
			$this->order->updateProviderOffer($data, $this->input->post('id'));
			$data1['mess']['type'] = 'success';	
			$this->order->updateOrder(['last_step' => 12], $id);
		}

		// 12
		if($this->input->post('send_to_qc') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateProviderOffer(['send_to_qc' => 1], $this->input->post('send_to_qc'));
			$this->order->updateOrder(['last_step' => 13], $id);
		}

		// 13 QC
		if( ($this->input->post('brak') !== null ||  $this->input->post('not_brak') !== null) && $this->input->request_method() == 'POST' && $this->input->post('id') > 0){
			$data['brak'] = $this->input->post('brak');
			$data['not_brak'] = $this->input->post('not_brak');
			if(trim($this->input->post('brak_comment')) != '')
				$data['brak_comment'] = trim($this->input->post('brak_comment'));
			if(trim($this->input->post('not_brak_comment')) != '')
				$data['not_brak_comment'] = trim($this->input->post('not_brak_comment'));
			$this->order->updateProviderOffer($data, $this->input->post('id'));
			$this->order->updateOrder(['last_step' => 14], $id);
		}

		// 13
		if($this->input->post('send-to-control') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateProviderOffer(['send_to_control' => 1], $this->input->post('send-to-control'));
			$this->order->updateOrder(['last_step' => 14], $id);
		}
		// 14.7 получить остаток (предоплата 100%)
		if($this->input->post('get-balance-tail') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['prepayment' => 100], $id);
		}
		// 15 отгрузка
		/*if($this->input->post('demand') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['demand'=>1], $id);
		}*/
		// 15 отгрузка doc
		if($this->input->post('send-demand-doc') != '' && $this->input->request_method() == 'POST'){
			$this->order->updateOrder(['demand_number'=>$this->input->post('demand_number'), 'demand_date'=>$this->input->post('demand_date')], $id);
		}

		if ($this->input->request_method() == 'POST') {
			redirect(request_uri());
		}

		$data1['order'] = $this->order->getOrder($id);
		// входящий платеж
		$data1['order']->paymentin = $this->order->getRecieptOfPayment($id);
		// сумма всех оплат
		$data1['order']->paymentin_sum = 0;
		foreach ($data1['order']->paymentin as $key => $payment) {
			$data1['order']->paymentin_sum += (float)$payment->sum;
		}
		$data1['components'] = $this->order->getComponentsAll();
		// debug($data1['components']);
		$data1['order']->components = $this->order->getComponents($id);
		$data1['order']->summary_price = 0;
		// формирование счета 
		foreach($data1['order']->components as &$item){
			$item->offers = $this->order->getComponentOffers($item->id, '1');
			$item->scored = $this->order->getComponentScoredOffers($item->id)->scored;
			// $item->offers_not_selected = $this->order->getComponentOffers($item->id, '0');
			
			foreach ($item->offers as  &$offer) {
				if($offer->profit_margin !== '')
					$margin = $offer->profit_margin;
				elseif($data1['order']->profit_margin !== '')
					$margin = $data1['order']->profit_margin;
				else
					$margin = 0;
			
				$offer->sum = ($offer->price/100*$margin*$offer->quantity) + ($offer->price*$offer->quantity);
                $data1['order']->summary_price += $offer->sum;
			}
		}
		// плюсую логистику
		$data1['order']->summary_price += $data1['order']->logistic_price;
		// debug($data1['order']->paymentin_sum);
		// оплачено %
		$data1['order']->payed = $data1['order']->paymentin_sum/$data1['order']->summary_price*100;
		$data1['order']->payed = number_format($data1['order']->payed, 0, '.', ' ');
		// к оплате руб.
		$data1['order']->for_pay = $data1['order']->summary_price/100*$data1['order']->prepayment - $data1['order']->paymentin_sum;
		if($data1['order']->for_pay < 0) $data1['order']->for_pay = 0;
		//оплата счетов
		$data1['order']->providerInvoiceSended = $this->order->providerInvoiceSended($id);
		$data1['order']->paymentout_sum = 0;
		foreach ($data1['order']->providerInvoiceSended as $key => $inv) {
			$data1['order']->paymentout_sum += $inv->paymentout;
		}
		$data1['order']->payment_diff = $data1['order']->paymentin_sum - $data1['order']->paymentout_sum;

		// склады 
		$data1['order']->storages = $this->order->getStorages();
		$data1['user'] = $this->session->get('logined');
		// категории
		$data1['categories'] = $this->order->getCategories();
		// for modal
		$data1['category'] = $data1['categories'][0];



		
		// статусы
		$this->getOrderStatus($data1['order']);

				// дерево меню
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML_2'] = getTreeHTML2($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2();
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);
		$childs = getTreeChilds($menu_tree, $data1['categories'][0]->id);

		$data1['category']->components = $this->order->getCategoryComponents(!empty($childs)?$childs:[$data1['categories'][0]->id]);

		$this->load->view('order_view', $data1);
	}


	public function getOrderStatus(&$ord){
				// Статусы
		// -----------------------  in
		if($ord->paymentin_sum > $ord->paymentout_sum && $ord->paymentin_sum > 0 && $ord->paymentout_sum > 0){
			$ord->status_payout = 'Half';
		}elseif( $ord->paymentout_sum >= $ord->paymentin_sum  && $ord->paymentin_sum > 0 && $ord->paymentout_sum > 0){
			$ord->status_payout = 'Full';
		}elseif($ord->paymentout_sum <= 0){
			$ord->status_payout = 'No';
		}
		// ---------------------  out
		if($ord->paymentin_sum < $ord->summary_price && $ord->paymentin_sum > 0){
			$ord->status_payin = 'Half';
		}elseif($ord->paymentin_sum >= $ord->summary_price && $ord->paymentin_sum > 0 && $ord->summary_price > 0){
			$ord->status_payin = 'Full';
		}elseif($ord->paymentin_sum <= 0){
			$ord->status_payin = 'No';
		}
		// --------------- quotation
		$counter_quota = 0;
		foreach($ord->components as $item){
			if(!empty($item->offers)){
				$counter_quota++;
			}
		}
		$components_count = count($ord->components);
		if($components_count >0 && $counter_quota > 0 && $counter_quota >=  $components_count ){
			$ord->quota_status = 'Full';
		}elseif($components_count >0 && $counter_quota > 0 && $components_count > $counter_quota){
			$ord->quota_status = 'Half';
		}elseif($counter_quota <= 0){
			$ord->quota_status = 'No';
		}
		// ------------------ delivery 
		$counter_supply = 0;
		foreach($ord->components as $item){
			foreach($item->offers as $offer){
				if(!empty($offer->supply_number)){
					$counter_supply++;
				}
			}
		}
		if($components_count >0 && $counter_supply > 0 && $counter_supply >= $components_count){
			$ord->supply_status = 'Full';
		}elseif($components_count >0 && $counter_supply > 0 && $components_count > $counter_supply){
			$ord->supply_status = 'Half';
		}elseif($counter_supply <= 0){
			$ord->supply_status = 'No';
		}
		// ------------------ demand 
		$counter_brak = 0;
		foreach($ord->components as $item){
			foreach($item->offers as $offer){
				if(!empty($offer->brak)){
					$counter_brak++;
				}
			}
		}
		if($ord->demand_number == true && $counter_brak == 0){
			$ord->demand_status = 'Full';
		}elseif($ord->demand_number == true && $counter_brak > 0){
			$ord->demand_status = 'No';
		}elseif($ord->demand_number == false){
			$ord->demand_status = 'No';
		}

	}




}
			
