<?php defined('DIRECT') OR exit('No direct script access allowed');

class Archive extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Archive - Component');
		$this->load->model('Archive_model', 'archive', '');
		$data1 = [];
		$data1['orders'] = $this->archive->getOrders();
		foreach ($data1['orders'] as &$ord) {
			$ord->paymentin = $this->archive->getRecieptOfPayment($ord->id);
			$ord->paymentin_sum = 0;
			// debug($ord);
			if(!empty($ord->paymentin)){
				foreach ($ord->paymentin as $key => $payment) {
					$ord->paymentin_sum += (float)$payment->sum;
				}
			}
			// OUT
			$ord->payout = $this->archive->getPaymentOut($ord->id);
			// debug($ord->payout);
			$ord->paymentout_sum = 0;
			if(!empty($ord->payout)){
				foreach ($ord->payout as $key => $p_out) {
					// debug($p_out);
					$ord->paymentout_sum += (float)$p_out->paymentout;
				}
			}
		}

		// ///////////////////////// Статусы
		foreach ($data1['orders'] as &$ord) {
			// in
			if($ord->paymentin_sum > $ord->paymentout_sum && $ord->paymentin_sum > 0 && $ord->paymentout_sum > 0){
				$ord->status_payout = 'Half';
			}elseif($ord->paymentin_sum == $ord->paymentout_sum && $ord->paymentin_sum > 0 && $ord->paymentout_sum > 0){
				$ord->status_payout = 'Full';
			}elseif($ord->paymentout_sum <= 0){
				$ord->status_payout = 'No';
			}

			// вычисление суммы всех компонентов с количеством в пределах заказа
			if($ord->profit_margin <= 0 || $ord->profit_margin_applied == false)
				$margin = 0;
			else
				$margin = $ord->profit_margin;
			$ord->components = $this->archive->getComponents($ord->id);
			foreach($ord->components as &$item){
				$item->offers = $this->archive->getComponentOffers($item->id, '1');
				foreach ($item->offers as  &$offer) {
					$offer->sum = ($offer->price/100*$margin*$offer->quantity) + ($offer->price*$offer->quantity);
                	$ord->summary_price += $offer->sum;
				}
			}
			// out
			if($ord->paymentin_sum < $ord->summary_price && $ord->paymentin_sum > 0){
				$ord->status_payin = 'Half';
			}elseif($ord->paymentin_sum == $ord->summary_price && $ord->paymentin_sum > 0 && $ord->summary_price > 0){
				$ord->status_payin = 'Full';
			}elseif($ord->paymentin_sum <= 0){
				$ord->status_payin = 'No';
			}

			// quotation
			$counter_quota = 0;
			foreach($ord->components as &$item){
				if(!empty($item->offers)){
					$counter_quota++;
				}
			}
			$components_count = count($ord->components);
			if($components_count >0 && $counter_quota > 0 && $components_count == $counter_quota){
				$ord->quota_status = 'Full';
			}elseif($components_count >0 && $counter_quota > 0 && $components_count > $counter_quota){
				$ord->quota_status = 'Half';
			}elseif($counter_quota <= 0){
				$ord->quota_status = 'No';
			}
			// ------------------ delivery 
			$counter_supply = 0;
			foreach($ord->components as $item){
				foreach($item->offers as $offer){
					if(!empty($offer->supply_number)){
						$counter_supply++;
					}
				}
			}
			if($components_count >0 && $counter_supply > 0 && $components_count == $counter_supply){
				$ord->supply_status = 'Full';
			}elseif($components_count >0 && $counter_supply > 0 && $components_count > $counter_supply){
				$ord->supply_status = 'Half';
			}elseif($counter_supply <= 0){
				$ord->supply_status = 'No';
			}
					// ------------------ demand 
			$counter_brak = 0;
			foreach($ord->components as $item){
				foreach($item->offers as $offer){
					if(!empty($offer->brak)){
						$counter_brak++;
					}
				}
			}
			if($ord->demand_number == true && $counter_brak == 0){
				$ord->demand_status = 'Full';
			}elseif($ord->demand_number == true && $counter_brak > 0){
				$ord->demand_status = 'Half';
			}elseif($ord->demand_number == false){
				$ord->demand_status = 'No';
			}

		}		
		
		$this->load->view('archive_view', $data1);
	}
}
			
