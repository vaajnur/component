<?php defined('DIRECT') OR exit('No direct script access allowed');

class Category extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		
		if($this->session->get('logined') == false){
			redirect('/login');
		}
		$this->asset->set_title('Category - Компонент');
		$this->load->model('Category_model', 'category', '');
		$data1 = [];

		if (!empty($_POST)) {
			// $this->log->write('7', var_export($_POST, true)  . PHP_EOL . 'index');
		}


		if ( ($this->input->post('add_category') == '1'  || $this->input->post('edit_category') == '1')  && $this->input->request_method() == 'POST' ) {
			$required_fields = array(
				'name',
				'parent_id',
			);
			$another_fields = array(
				'body',
				'voltage',
				'deviation',
				'manufacturer',
				'year_of_production',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				if(preg_match('@[а-яА-Я]+@mi', $this->input->post('name', false)) != true)
					$data['name'] = $this->input->post('name', false);
				else
					$data['cyril'] = true;
				if($data['cyril'] === null){
					$data['parent_id'] = explode('_', $this->input->post('parent_id'))[0];
					$data['level'] = explode('_', $this->input->post('parent_id'))[1] + 1;
					if($this->input->post('id') != false){
						$cat_id = $this->input->post('id');
						$this->category->updateCategory($data, $cat_id);
						// var_dump($cat_id);
					}
					else{
						$cat_id = $this->category->addCategory($data);
					}
					// //////////////
					// add custom fields
					// ////////////////
					if(!empty($this->input->post('custom_f_name', false))){
						$custom_f = $this->category->getCategoryCustomF($cat_id);
						$custom_f = json_decode(json_encode($custom_f), true);
						foreach ($this->input->post('custom_f_name', false) as $key => $f_name) {
								// название поля
								// значение поля
							if($f_name == false || preg_match('@[а-яА-Я]+@mi', $f_name) == true) continue;	
							$d1['value'] = $this->input->post('custom_f_value', false)[$key];
							// field already exist
							if(in_array((string)$key, array_column($custom_f, 'name'))){
								// debug($key);
								$d1['name'] = $f_name;
								$this->category->updateCustomFieldsCategory($d1, $cat_id, $key);
							}else{
								$d1['name'] = $f_name;
								$d1['category_id'] = $cat_id;
								$this->category->addCustomFieds($d1);
							}
						}
					}
					if ($this->input->post('referer') == false) {
						redirect($this->input->post('referer'));
					}else{
						redirect(request_uri());
					}
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}
			}
			if ($this->input->post('referer') == false)
				redirect($this->input->post('referer'));
		}

		// -----------  DEL  cat
		if ($this->input->post('action') == 'del_categor' && $this->input->request_method() == 'POST' ) {
			if($this->input->post('id') != false){
				$this->category->deleteCategory($this->input->post('id'));
			}
			exit;
		}

		$data1['categories']  =  $this->category->getCategories();
		$first_category = $data1['categories'][0];
		// дерево меню
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML'] = getTreeHTML($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2();
		// debug($menu_tree_select2);
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);
		// debug($data1['categories']);
		$childs = getTreeChilds($menu_tree, $first_category->id);
		// debug($childs);

		$data1['category']->components = $this->category->getCategoryComponents(!empty($childs)?$childs:[$first_category->id]);
		$data1['category']->custom_f = $this->category->getCategoryCustomF($first_category->id);
		$this->load->view('categories_view', $data1);
	}

	/**
	 * [detail description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function detail($id)
	{	

		$this->asset->set_title('Category detail - Компонент');
		$this->load->model('Category_model', 'category', '');
		$data1 = [];

		if (!empty($_POST)) {
			$this->log->write('7', var_export($_POST, true) . PHP_EOL . 'detail');
		}

		if (!empty($_FILES)) {
			// $this->log->write('7', var_export($_FILES, true));
		}

		$this->upload->config([
			'upload_path'  => ROOT_DIR . 'public/upload', 
			// 'allowed_types' => 'pdf'
		]);

		if ($this->input->post('add_component') == '1' && $this->input->request_method() == 'POST' ) {
						$required_fields = array(
				'name',
			);
			$another_fields = array(
				'qty',
				'position_on_pcb',
				'pn',
				'remark',
				'substitutes',
				'code',
				'dc',
				'body',
				'mfg',
				'qty_in_packing',
				'qty_to_buy_pcs',
				'unit_price',
				'sum',
				'lead_time_days',
				'weight_per_1_unit_kg',
				'tp',
				'voltage',
				'variation',
				'manufacturer',
				'year_of_production',
				'weight',
				'comment',
				'marking'
				// 'date_arrival',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) != ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
								$data[$item] = $this->input->post($item, false);
						else
							$data['cyril'] = true;
					}
				});
				if($data['cyril'] === null){				
					//-/ --------/ новый компонент
					if(is_numeric($this->input->post('name', false))){
						$name = $this->category->getCompName($this->input->post('name', false));	 	
						debug($name);
						$data['name'] = $name->name;
					}
					$data['category_id'] = $id;
					$res = $this->category->addComponent($data);
					if($res > 0){
						// общие для всей категории кастомные поля
						$fields = $this->input->post('custom_f', false);
						if (!empty($fields) && $this->input->request_method() == 'POST' ) {
							$d1 = [];
							$d1['component_id'] = $res;
							array_walk($fields, function($val, $key) use(&$d1, $id){
								$d1['cf_id'] = $key;
								$d1['value'] = $val;
								$this->category->addComponentCF($d1);
							});
						}
						// docs
						$up_res = $this->uploadDoc();
						if($up_res == true)
							$this->category->updateComponent(['doc_id' => $up_res], $res);

						if ($this->input->post('ajax') == true) {
							echo 'ajax add comp';
							exit;
						}elseif ($this->input->post('referer') == false) {
							redirect($this->input->post('referer'));
						}else{
							redirect(request_uri());
						}
					}
						// $data1['mess'] = [ 'type' => 'success', 'text' => 'insert component success!'];
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>', 'code' => 'CYR'];
				}

			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'empty:</br>' . implode('<br>', $empty_fields)];
			}
		}


		// -------------- DEL
		if ($this->input->post('action') == 'del_categor_comp' && $this->input->request_method() == 'POST' ) {
			if($this->input->post('id') != false){
				$this->category->deleteComponent($this->input->post('id'));
			}
			exit;
		}


		$data1['category'] = $this->category->getCategory($id);

		$data1['category']->custom_f = $this->category->getCategoryCustomF($id);
		$data1['categories']  = $this->category->getCategories();

		// дерево меню
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML'] = getTreeHTML($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2();
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);
		$childs = getTreeChilds($menu_tree, $id);

		$data1['category']->components = $this->category->getCategoryComponents(!empty($childs)?$childs:[$id]);
		// debug($data1['category']->components);
		// дерево меню
		// debug($data1['menu_tree_select2']);

		$this->load->view('category_view', $data1);
	}

	/**
	 * [ajax description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function ajax($id){
		$this->load->model('Category_model', 'category', '');
		$data1 = [];
		$data1['category'] = $this->category->getCategory($id);
		
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML'] = getTreeHTML($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2();
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);
		$childs = getTreeChilds($menu_tree, $id);

		$data1['category']->components = $this->category->getCategoryComponents(!empty($childs)?$childs:[$id]);
		// var_dump(111111111);
		$data1['category']->custom_f = $this->category->getCategoryCustomF($id);
		$data1['categories'] = $this->category->getCategories();

		$this->load->view('category_ajax_view', $data1);
	}

	/**
	 * [list description]
	 * @return [type] [description]
	 */
	public function list()
	{	
		$this->asset->set_title('Category list - Компонент');
		$this->load->model('Category_model', 'category', '');
		if (!empty($_POST)) {
			// $this->log->write('7', var_export($_POST, true) . PHP_EOL . 'list');
		}
		$data1 = [];

		// -----------  DEL  cat
		if ($this->input->post('action') == 'del_categor' && $this->input->request_method() == 'POST' ) {
			if($this->input->post('id') != false){
				$this->category->deleteCategory($this->input->post('id'));
			}
			exit;
		}

		$data1['categories']  = $this->category->getCategoriesAll();
		// debug($data1['categories']);
		// exit;
		// дерево меню
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML'] = getTreeHTML($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2();
		// debug($menu_tree_select2);
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);

		$this->load->view('categories_list_view', $data1);
	}

		// ajax
	public function edit($id)
	{	
		$this->load->model('Category_model', 'category', '');
		$data1 = [];
		
		$data1['categories'] = $this->category->getCategories();
		$data1['category'] = $this->category->getCategory($id);
		$data1['category']->custom_f = $this->category->getCategoryCustomF($id);
		$cat = getCat(); 
		$menu_tree = getTree();
		$data1['menu_tree_HTML'] = getTreeHTML($menu_tree);
		$data1['menu_tree_Select'] = getTreeSelect($menu_tree);
		$menu_tree_select2 = getTreeForSelect2($id);
		$data1['menu_tree_select2'] = json_encode($menu_tree_select2);
		$childs = getTreeChilds($menu_tree, $id);

		$this->load->view('edit_category_view', $data1);
	}

	/**
	 * [uploadDoc description]
	 * @param  [type] 'component_document' [description]
	 * @return [type]      [description]
	 */
	public function uploadDoc(){
		$file = [];
		// если из таблицы то массив
		if(is_array($_FILES['component_document']['name'])){
			$name = $_FILES['component_document']['name'][0];
			$file = [
				'name' => $name,
				'type' => $_FILES['component_document']['type'][0],
				'error' => $_FILES['component_document']['error'][0],
				'size' => $_FILES['component_document']['size'][0],
				'tmp_name' => $_FILES['component_document']['tmp_name'][0]
			];
		}else{
			// из модалки
			$name = $_FILES['component_document']['name'];
			$file = [
				'name' => $name,
				'type' => $_FILES['component_document']['type'],
				'error' => $_FILES['component_document']['error'],
				'size' => $_FILES['component_document']['size'],
				'tmp_name' => $_FILES['component_document']['tmp_name']
			];
		}
		if($name == false) return false;
		$this->upload->handle($file);
		if($this->upload->getErrors())
			debug($this->upload->getErrors());
		$file2['path'] = '/public/upload/'.$name;
		$file2['type'] = 'component_document';
		$file2['name'] = $name;
		// $file_id[] = $this->docs->addDocs($file);
		$res = $this->category->addFile($file2);
		return $res;
	}




}
			
