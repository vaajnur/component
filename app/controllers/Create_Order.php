<?php defined('DIRECT') OR exit('No direct script access allowed');

class Create_Order extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	

		if($this->session->get('logined') == false){
			redirect('/login');
		}

		// for Control
		if($this->session->get('logined')->role == '3'){
			redirect('/orders');
		}
		$this->asset->set_title('Create order - Component');
		$this->load->model('Create_Order_model', 'create_order', '');
		$data1 = [];

		// var_dump($_POST);

		if (isset($_POST)) {
			// $this->log->write('7', var_export($_POST, true));
		}
		if (isset($_FILES)) {
			// $this->log->write('7', var_export($_FILES, true));
		}
		$this->upload->config([
			'upload_path'  => ROOT_DIR . 'public/upload', 
			// 'allowed_types' => 'pdf'
		]);


		if(  ( $this->input->post('send-new-order') || $this->input->post('send-edit-order')) && $this->input->request_method() == 'POST'){
			$required_fields = array(
				'date',
				'order_num',
			);
			$another_fields = array(
				'type',
				'name',
				'customer',
				'additional_cost',
				'description',
				'pcb_count',
			);
			$empty_fields = array_filter($required_fields, function($item){
				return $this->input->post($item, false) == '';
			});
			if(empty($empty_fields)){
				$data = [];
				$all_fields = array_merge($required_fields, $another_fields);
				array_walk($all_fields, function($item, $k) use(&$data){
					if($this->input->post($item, false) !== ''){
						if(preg_match('@[а-яА-Я]+@mi', $this->input->post($item, false)) != true)
							$data[$item] = $this->input->post($item);
						else
							$data['cyril'] = true;
					}
				});

				if($data['cyril'] === null){
					$data['pcb_count'] = $data['pcb_count']>0?$data['pcb_count']:1;
					if( $this->input->post('send-new-order')){
						$res = $this->create_order->addOrder($data);
						if($res > 0){
							// file
							$up_res = $this->uploadDoc();
							if($up_res == true)
								$this->create_order->updateOrder(['order_document' => $up_res], $res);

							redirect('/order/'.$res);
							// $data1['mess'] = [ 'type' => 'success', 'text' => 'Заявка добавлена!'];
						}
					}elseif( $this->input->post('send-edit-order') ){
						// debug($data);
						$up_res = $this->uploadDoc();
						if($up_res == true)
							$this->create_order->updateOrder(['order_document' => $up_res], $this->input->post('id'));

						$this->create_order->updateOrder($data, $this->input->post('id', false));
					}
				}else{
					$data1['mess'] = [ 'type' => 'danger', 'text' => 'Cyrillic detected!</br>'];
				}
			}else{
				$data1['mess'] = [ 'type' => 'danger', 'text' => 'not filled!</br>' . implode('<br>', $empty_fields)];
			}
		}
		if($this->input->post('send-edit-order'))
			redirect('/edit-order/'.$this->input->post('id'));
		if($data1['mess']['type'] == 'success')
			redirect(request_uri());

		$data1['order_num'] = $this->create_order->getNum()->order_num>0?$this->create_order->getNum()->order_num+1 : 100;
		$data1['clients'] = $this->create_order->getClients();
		// debug($data1['order_num']);

		$this->load->view('create-order_view', $data1);
	}

	/**
	 * [edit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function edit($id){
		$this->load->model('Create_Order_model', 'order', '');
		$data1 = [];
		$data1['order'] = $this->order->getOrder($id);
		$data1['edit'] = true;
		$data1['id'] = $id;
		$data1['order_num'] = $this->order->getNum()->order_num>0?$this->order->getNum()->order_num : 100;;
		$data1['clients'] = $this->order->getClients();
		$this->load->view('create-order_view', $data1);
	}

	/**
	 * [delete description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id){
		$this->load->model('Create_Order_model', 'order', '');
		$this->order->deleteOrder($id);
		redirect('/');
	}

	/**
	 * [uploadDoc description]
	 * @param  [type] 'order_document' [description]
	 * @return [type]      [description]
	 */
	public function uploadDoc(){
		$file = [];
		// если из таблицы то массив
		if(is_array($_FILES['order_document']['name'])){
			$name = $_FILES['order_document']['name'][0];
			$file = [
				'name' => $name,
				'type' => $_FILES['order_document']['type'][0],
				'error' => $_FILES['order_document']['error'][0],
				'size' => $_FILES['order_document']['size'][0],
				'tmp_name' => $_FILES['order_document']['tmp_name'][0]
			];
		}else{
			// из модалки
			$name = $_FILES['order_document']['name'];
			$file = [
				'name' => $name,
				'type' => $_FILES['order_document']['type'],
				'error' => $_FILES['order_document']['error'],
				'size' => $_FILES['order_document']['size'],
				'tmp_name' => $_FILES['order_document']['tmp_name']
			];
		}
		if($name == false) return false;
		$this->upload->handle($file);
		if($this->upload->getErrors())
			debug($this->upload->getErrors());
		$file2['path'] = '/public/upload/'.$name;
		$file2['type'] = 'order_document';
		$file2['name'] = $name;
		// $file_id[] = $this->docs->addDocs($file);
		$res = $this->create_order->addFile($file2);
		return $res;
	}
}
			
