<?php defined('DIRECT') OR exit('No direct script access allowed');

class Test extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$this->asset->set_title('Test - Component');
		$this->load->model('Test_model', 'test', '');
		$data1 = [];

		$components = $this->test->getComponents();
		$counter = 0;
		foreach($components as $comp){
			if($comp->component_id == false){
			// debug($comp->component_id);

			$base = $this->test->getComponent($comp->name);
			// if($base)
				$counter++;
			$this->test->updateComp(['component_id' => $base->id], $comp->id);
			}
		}
		debug($counter);

		// $data1['test'] = $this->test->getTest();
		// $this->load->view('test_view', $data1);
	}


	public function test2()
	{	
		$this->asset->set_title('Test - Component');
		$this->load->model('Test_model', 'test', '');
		$data1 = [];

		// $data1['test'] = $this->test->getTest();
		$categors = $this->test->getCategories();
		$cat = $this->getCat($categors); 
		// debug($cat);
		$tree = $this->getTree($cat);
		$tree_html = $this->getTreeHTML($tree);
		$tree_select = $this->getTreeSelect($tree);
		$menu_tree_select2 = $this->getTreeForSelect2($cat);
		$menu_tree_select2_clear = $this->clearParentFromTree($menu_tree_select2);

		debug($this->getTreeChilds($tree, 27));
		debug($this->getTreeChilds($tree, 40));
		debug($this->getTreeChilds($tree, 45));

		// debug($tree);
		// debug($tree_html);
		// debug($tree_select);
		// debug($menu_tree_select2);
		// debug($menu_tree_select2_clear);
		// echo $menu_tree_select2;
		// $this->load->view('test_view', $data1);
	}

	public function getCat($categors)
	{
		foreach($categors as $cat){
			$categors_list[$cat->id] = json_decode(json_encode($cat), true);
		}
		return $categors_list;
	}

/*	public function getCat2($categors)
	{
		foreach($categors as $cat){
			$c = json_decode(json_encode($cat), true);
			$categors_list[$cat->id] = ['id' => $c['id'].'_'.$c['level'], 'text' => $c['name'] ];
		}
		return $categors_list;
	}*/

	public function getTree($dataset) {
	    $tree = array();

	    foreach ($dataset as $id => &$node) {    
	        //Если нет вложений
	        if (!$node['parent_id']){
	            $tree[$id] = &$node;
	        }else{ 
	            //Если есть потомки то перебераем массив
	            $dataset[$node['parent_id']]['childs'][$id] = &$node;
	        }
	    }
	    return $tree;
	}

	function getTreeChilds($tree, $parent_id){
		$result = [];
			// debug($tree);
		foreach($tree as $id => $nodes){
			// debug($id);
			if($id == $parent_id && $nodes['childs']){
				// debug($nodes['childs']);
				array_walk_recursive($nodes['childs'], function ($item, $key) use (&$result) {
					if($key == 'id')
					    $result[] = $item;    
				});
				array_unshift($result, $parent_id);
				// debug($result);
			}elseif($id != $parent_id && $nodes['childs']){
				$result = array_merge($result, $this->getTreeChilds($nodes['childs'], $parent_id));
			}
		}
		return $result;
	}


	public function getTreeForSelect2($dataset) {
	    $tree = array();

	    foreach ($dataset as $id => &$node) {    
	    	// debug($node);
	        //Если нет вложений
        	$node = ['id' => $node['id'].'_'.$node['level'], 'text' => $node['name'], 'parent_id' => $node['parent_id'] ];
	        if (!$node['parent_id']){
	            $tree[] = &$node;
	        }else{ 
	            //Если есть потомки то перебераем массив
	            $dataset[$node['parent_id']]['inc'][] = &$node;
	        }
	    }
	    // return json_encode($tree);
	    return $tree;
	}

	function clearParentFromTree(&$tree){
		foreach ($tree as $id => &$node) { 
			unset($node['parent_id']);
			if($node['inc']){
				$this->clearParentFromTree($node['inc']);
			}
		}
		return $tree;
	}

	public function getTreeSelect($arr, $sub = false) {
		if($sub == false)
			$select = '<select name="" class="">';
		foreach ($arr as $id => $node) {
			$select .= "<option value='{$node['id']}_{$node['level']}'>".str_repeat('&nbsp;&nbsp;&nbsp;', $node['level'])."{$node['name']}</option>";
			if ($node['childs'] !== null){
	        	$select .= $this->getTreeSelect($node['childs'], true);	
	        }
		}
		if($sub == false)
			$select .= "</select>";
		return $select;
	}


	public function getTreeHTML($arr, $sub = false) {
	    $tree = '<ul '.($sub==true?' class="submenu" ':'').'>';

		    // var_dump($tree);
	    foreach ($arr as $id => $node) {    
	        //Если нет вложений
	    	$tree .= "<li class='level-{$node['level']}' data-level='{$node['level']}'>";
	            $tree .= "<a href='/category/{$node['id']}'>{$node['name']}</a>";
	        if ($node['childs'] !== null)
	        	$tree .= $this->getTreeHTML($node['childs'], true);	
	    	$tree .= "</li>";
	    }
	    	$tree .= "</ul>";
	    return $tree;
	}
}
			
