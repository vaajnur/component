-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `component_ark_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `component_ark_db`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `archive`;
CREATE TABLE `archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `categories` (`id`, `name`) VALUES
(14,	'resistors'),
(15,	'transformators'),
(16,	'Test category'),
(17,	'chips'),
(18,	'Resistor'),
(19,	'Resistor'),
(20,	'termistors '),
(21,	'sensors');

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `legal_address` varchar(255) DEFAULT NULL,
  `postal_address` varchar(255) DEFAULT NULL,
  `phone_fax` varchar(255) DEFAULT NULL,
  `inn` int(11) DEFAULT NULL,
  `kpp` int(11) DEFAULT NULL,
  `ogrn` int(11) DEFAULT NULL,
  `payment_account` int(11) DEFAULT NULL,
  `correspondent_account` int(11) DEFAULT NULL,
  `bic_of_the_bank` int(11) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `fio_director` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `clients` (`id`, `full_name`, `legal_address`, `postal_address`, `phone_fax`, `inn`, `kpp`, `ogrn`, `payment_account`, `correspondent_account`, `bic_of_the_bank`, `bank`, `fio_director`, `email`) VALUES
(8,	'POSCENTRE',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `components`;
CREATE TABLE `components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order1` int(11) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `position_on_pcb` varchar(255) NOT NULL,
  `pn` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `substitutes` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `dc` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `mfg` varchar(255) NOT NULL,
  `qty_in_packing` varchar(255) DEFAULT NULL,
  `qty_to_buy_pcs` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `sum` varchar(255) DEFAULT NULL,
  `lead_time_days` varchar(255) DEFAULT NULL,
  `weight_per_1_unit_kg` varchar(255) DEFAULT NULL,
  `tp` varchar(255) DEFAULT NULL,
  `date_arrival` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `voltage` varchar(255) DEFAULT NULL,
  `variation` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `year_of_production` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `deviation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `components` (`id`, `name`, `order1`, `qty`, `position_on_pcb`, `pn`, `remark`, `substitutes`, `code`, `dc`, `body`, `mfg`, `qty_in_packing`, `qty_to_buy_pcs`, `unit_price`, `sum`, `lead_time_days`, `weight_per_1_unit_kg`, `tp`, `date_arrival`, `category_id`, `voltage`, `variation`, `manufacturer`, `year_of_production`, `weight`, `doc_id`, `deviation`) VALUES
(83,	'new component',	71,	'                                                                                                                                                                                                                                                               ',	'                                                                                                                                                                                                                                                               ',	'                                                                                                sedfef gdf                                                                                    ',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(86,	'new component',	74,	'                                100                            ',	'                                0                            ',	'                                342r3f23f 2f                            ',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(87,	'resistor',	74,	'100',	'',	'ev dfvd f',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(88,	'Capacitor',	74,	'1000',	'',	'sedfef gdf',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(92,	'transformator 777',	0,	'',	'',	'',	'',	'',	'trf777',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	15,	NULL,	NULL,	NULL,	NULL,	'77000',	NULL,	NULL),
(93,	'transformator 555',	0,	'',	'',	'',	'',	'',	'trf555',	'',	'505',	'',	NULL,	NULL,	'5000',	NULL,	NULL,	NULL,	NULL,	NULL,	15,	NULL,	'trf-505-2',	NULL,	NULL,	'50000',	NULL,	NULL),
(94,	'Capacitor',	71,	'                                                                                                                                100000                                                                                                                ',	'                                                                                                                                                                                                                                                ',	'sedfef gdf',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(95,	'transformator 777',	71,	'                                                                                                999000                                                                                    ',	'                                                                                                                                                                                    ',	'123456',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(96,	'booster',	0,	'',	'',	'',	'',	'',	'b123',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	15,	NULL,	NULL,	NULL,	NULL,	'10',	NULL,	NULL),
(105,	'amplifier',	0,	'',	'',	'amp123',	'',	'',	'amp123123',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	15,	NULL,	NULL,	NULL,	NULL,	NULL,	110,	NULL),
(106,	'resistor new',	0,	'',	'',	'123456',	'',	'',	'',	'',	'res123',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	'1000',	NULL,	'china',	'1998',	NULL,	NULL,	NULL),
(107,	'new component',	0,	'',	'',	'123123',	'',	'',	'',	'',	'comp123',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	'220',	NULL,	'russia',	'2005',	NULL,	111,	'100'),
(108,	'transformator 777',	71,	'                                                            ',	'                                                            ',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(109,	'transformator 777',	71,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(110,	'transformator 777',	71,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(111,	'booster',	71,	'            100000                                                ',	'                                                            ',	'20000',	'comment',	'',	'code123',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(113,	'microchip',	0,	'',	'',	'123321',	'',	'',	'',	'',	'mc123',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	17,	'12',	NULL,	'singapur',	'202',	NULL,	NULL,	'bnnn'),
(114,	'transformator 777',	81,	's',	's',	'df',	'ff',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(115,	'booster',	82,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(118,	'Capacitor',	83,	'                                                                                                                                                                90                                                                                             ',	'fC7,C8,C9,C10,C13, C14,C15,C16,C19,C20,C21, C22,C23,C26,C27,C32,C33, C34,C35,C36,C37,C38,C39,C40,C41, C42,C43,C44,C45,C46,C47,C48,C49,C50, C51,C52,C53,C54,C55,C56,C58,C59,C60, C65,C69,C70,C71,C72,C73,C74,C81, C',	'm-c0603-0,1uF-10%-50V                                                                        ',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(123,	'rele',	0,	'',	'',	'rele123456',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(137,	'                                                                Capacitor ceramic SMD                                                        ',	79,	'                                                            ',	'                                                            ',	'                                                            ',	'',	'dd',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(140,	'                                new component                            ',	79,	'                                                            ',	'                                cdxxxxxxx                            ',	'                                                            ',	'xxxxxx',	'xxxx',	'ccccc',	'cccccc',	'cccccc',	'',	NULL,	'cccccccc',	'cccc',	NULL,	'cccccc',	'ccc',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(142,	'new component',	79,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(143,	'transformator 555',	73,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(144,	'transformator 777',	84,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(145,	'transformator 555',	78,	'',	'',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(151,	'                                microchip                            ',	71,	'                                    111                                                                                    ',	'ghjghgjhg222                            ',	'                                333                            ',	'444',	'55 sdfsdfsd jk;l\';lk\';lk;lk; werwerwer oijikjoijxcvxc v weffwefj',	'ldkfjsldkf  sdfjksdlfkjds  ewfioerjgiofv blvcxvmzxcv  ewefjsdlk',	'odfijsd lkfjds sdklfjdsl fksd dklfjsdlfkjroif ',	'fkdsjflksdfjs l sdfklsjdflksdjfk sdjf sdfj slkdfjsdkl fjoewirturigkjlvn',	'99',	'111',	'22',	'33',	NULL,	NULL,	NULL,	NULL,	NULL,	17,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(153,	'resistor new',	83,	'4                                                          ',	'dfagc gn11ddd111f111afffaaaaa33433434ddd21121d1xxzzx111ssdffccc3,C6,C7,C8,C9,C10,C13, C14,C15,C16,C19,C20,C21, C22,C23,C26,C27,C32,C33, C34,C35,C36,C37,C38,C39,C40,C41, C42,C43,C44,C45,C46,C47,C48,C49,C50, C51,C52,C53,C54,C55,C56,C58,C59,C60, C65,C69,C70,',	'                                                                                                                                                                                                                                                               ',	'444',	'dlfksjglsdfk dfgdfgjk sdfg df',	'dfkgjsdfh kgjdsfg ',	'xcvoergtoiwertjoe',	'[ASPDFLDPFGK',	'GJDSFKLGJ',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(156,	'rele',	83,	'11',	'1133310,C13,C14,C15, C16,C19,C20, C21,C22,C23,C26, C27,C32,C33,C34, C35,C36,C37,C38, C39,C40,C41,C42, C43,C44,C45,C46, C47,C48,C49,C50,C51, C5',	'Csm-c0603-0,47uF-10%-50V',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(159,	'new component',	83,	'44',	'C35,C, 6666',	'hhhh',	'kkk',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(162,	'resistor new',	83,	'',	'223310,C13,C14,C15, C16,C19,C20, C21,C22,C23,C26, C27,C32,C33,C34, C35,C36,C37,C38, C39,C40,C41,C42, C43,C44,C45,C46, C47,C48,C49,C50,C51, C5',	'Csm-c0603-0,47uF-10%-50V',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(165,	'Capacitor',	71,	'',	'ggggghhhhhh',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(166,	'Capacitor',	83,	'',	'123 nnm, 345 bbb',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(167,	'resistor',	83,	'',	'123 ghhgnnm',	'',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(168,	'Capacitor',	83,	'333',	'dydusyduys',	'chbxbmx',	'',	'',	'',	'',	'',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	14,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `component_custom_fields`;
CREATE TABLE `component_custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cf_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `component_custom_fields` (`id`, `cf_id`, `component_id`, `value`) VALUES
(1,	18,	92,	'7770W'),
(2,	19,	92,	'707'),
(3,	20,	92,	'7000'),
(4,	21,	92,	'705'),
(5,	18,	93,	'5000W'),
(6,	19,	93,	''),
(7,	20,	93,	''),
(8,	21,	93,	''),
(9,	18,	96,	''),
(10,	19,	96,	''),
(11,	20,	96,	''),
(12,	21,	96,	''),
(13,	18,	97,	''),
(14,	19,	97,	''),
(15,	20,	97,	''),
(16,	21,	97,	''),
(17,	18,	98,	''),
(18,	19,	98,	''),
(19,	20,	98,	''),
(20,	21,	98,	''),
(21,	18,	99,	''),
(22,	19,	99,	''),
(23,	20,	99,	''),
(24,	21,	99,	''),
(25,	18,	100,	''),
(26,	19,	100,	''),
(27,	20,	100,	''),
(28,	21,	100,	''),
(29,	18,	101,	''),
(30,	19,	101,	''),
(31,	20,	101,	''),
(32,	21,	101,	''),
(33,	18,	102,	''),
(34,	19,	102,	''),
(35,	20,	102,	''),
(36,	21,	102,	''),
(37,	18,	103,	''),
(38,	19,	103,	''),
(39,	20,	103,	''),
(40,	21,	103,	''),
(41,	18,	104,	''),
(42,	19,	104,	''),
(43,	20,	104,	''),
(44,	21,	104,	''),
(45,	18,	105,	''),
(46,	19,	105,	''),
(47,	20,	105,	''),
(48,	21,	105,	''),
(49,	10,	81,	''),
(50,	11,	81,	''),
(51,	12,	81,	''),
(52,	17,	81,	''),
(53,	22,	81,	''),
(54,	1,	79,	''),
(55,	2,	79,	''),
(56,	3,	79,	''),
(57,	4,	79,	''),
(58,	5,	79,	''),
(59,	6,	79,	''),
(60,	7,	79,	''),
(61,	8,	79,	''),
(62,	9,	79,	''),
(63,	10,	106,	'10mF'),
(64,	11,	106,	'12'),
(65,	12,	106,	'13'),
(66,	17,	106,	'15'),
(67,	22,	106,	'14'),
(68,	10,	86,	''),
(69,	11,	86,	''),
(70,	12,	86,	''),
(71,	17,	86,	''),
(72,	22,	86,	''),
(73,	10,	107,	'10mF'),
(74,	11,	107,	'120V'),
(75,	12,	107,	'22'),
(76,	17,	107,	'33'),
(77,	22,	107,	'11'),
(78,	10,	87,	''),
(79,	11,	87,	''),
(80,	12,	87,	''),
(81,	17,	87,	''),
(82,	22,	87,	''),
(83,	24,	113,	'great'),
(84,	25,	113,	'100'),
(85,	26,	113,	'200'),
(86,	10,	123,	''),
(87,	11,	123,	''),
(88,	12,	123,	''),
(89,	17,	123,	''),
(90,	22,	123,	''),
(91,	24,	151,	''),
(92,	25,	151,	''),
(93,	26,	151,	'');

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `custom_fields`;
CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `custom_fields` (`id`, `name`, `value`, `client_id`, `employee_id`, `category_id`) VALUES
(2,	'phone',	'9123123123123',	7,	0,	0),
(3,	'fio',	'name123',	0,	1,	0),
(4,	'fio',	'name22222',	0,	2,	0),
(5,	'fio',	'name3',	0,	3,	0),
(6,	'phone',	'44444',	0,	3,	0),
(7,	'fax',	'11111111111',	7,	0,	0),
(8,	'fio',	'name4',	0,	4,	0),
(9,	'fio123456',	'name4123456',	0,	4,	0),
(10,	'capacity123',	'1000123',	0,	0,	14),
(11,	'voltage',	'2000',	0,	0,	14),
(12,	'res',	'3000',	0,	0,	14),
(17,	'size2',	'm2',	0,	0,	14),
(18,	'power',	'1000W',	0,	0,	15),
(19,	'durability',	'2000',	0,	0,	15),
(20,	'weight',	'100kg',	0,	0,	15),
(21,	'volume',	'10m3',	0,	0,	15),
(22,	'height',	'meters',	0,	0,	14),
(23,	'Test property',	'Units',	0,	0,	16),
(24,	'solution',	'',	0,	0,	17),
(25,	'size',	'',	0,	0,	17),
(26,	'gabarites',	'',	0,	0,	17),
(27,	'Body',	'',	0,	0,	18),
(28,	'Voltage',	'V',	0,	0,	18),
(29,	'Body',	'',	0,	0,	19),
(30,	'Voltage',	'V',	0,	0,	19),
(31,	'skype',	'sk7777',	7,	0,	0),
(32,	'capacity',	'mF',	0,	0,	20),
(33,	'width',	'cm',	0,	0,	21);

DROP TABLE IF EXISTS `delivery_periods`;
CREATE TABLE `delivery_periods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delivery_periods` (`id`, `period`) VALUES
(1,	'1 week'),
(2,	'2 weeks'),
(3,	'3 weeks'),
(4,	'4 weeks'),
(5,	'1 month'),
(6,	'1-3 days'),
(7,	'3-7 days'),
(8,	'1 month');

DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `documents` (`id`, `name`, `path`, `type`) VALUES
(98,	'______ 0169112__54B5E__=B@0_B__;_1_aquator.pdf',	'/public/upload/______ 0169112__54B5E__=B@0_B__;_1_aquator.pdf',	'invoicein_doc'),
(99,	'______ 0169112__54B5E__=B@0_B__;_1_aquator.pdf',	'/public/upload/______ 0169112__54B5E__=B@0_B__;_1_aquator.pdf',	'supply_document'),
(100,	'N332 BOM.pdf',	'/public/upload/N332 BOM.pdf',	'order_document'),
(101,	'pdf.pdf',	'/public/upload/pdf.pdf',	'supply_document'),
(102,	'pdf (копия).pdf',	'/public/upload/pdf (копия).pdf',	'supply_document'),
(103,	'TTH___ 0490315__N_A_5E_ ___.pdf',	'/public/upload/TTH___ 0490315__N_A_5E_ ___.pdf',	'order_document'),
(104,	'pdf.pdf',	'/public/upload/pdf.pdf',	'order_document'),
(106,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(107,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(108,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(109,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(110,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(111,	'kofemashina_wmf_5000_s_plus (копия).pdf',	'/public/upload/kofemashina_wmf_5000_s_plus (копия).pdf',	'component_document'),
(112,	'kofemashina_wmf_5000_s_plus.pdf',	'/public/upload/kofemashina_wmf_5000_s_plus.pdf',	'supply_document');

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `employee` (`id`, `post`, `client_id`) VALUES
(1,	'manager',	7),
(2,	'proger',	7),
(3,	'buhgalter',	7),
(4,	'job123',	7);

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `order_num` int(11) NOT NULL COMMENT 'номер закза',
  `order_num_2` int(11) NOT NULL COMMENT 'номер после отправки инж',
  `order_document` int(11) NOT NULL COMMENT 'attachment',
  `to_engineer` int(1) NOT NULL COMMENT 'отправлено инженеру',
  `to_control` int(1) NOT NULL COMMENT 'отправлено контролу',
  `to_accountant` int(1) NOT NULL COMMENT 'бухгалтеру',
  `to_logist` int(1) NOT NULL COMMENT 'логисту',
  `to_manager` int(1) NOT NULL COMMENT 'менеджеру',
  `purchase_order` int(1) NOT NULL COMMENT 'purchase order',
  `pcb_count` int(255) NOT NULL COMMENT 'количество плат',
  `comment` varchar(255) DEFAULT NULL COMMENT 'комментарий менеджера',
  `description` varchar(255) DEFAULT NULL COMMENT 'описание',
  `customer` varchar(255) DEFAULT NULL COMMENT 'Клиент',
  `repeated` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL COMMENT 'Дата',
  `delivery_price` varchar(255) DEFAULT NULL COMMENT 'доставка',
  `logistic_price` varchar(255) DEFAULT NULL COMMENT 'логистика',
  `profit_margin` varchar(255) DEFAULT NULL COMMENT 'маржа %',
  `profit_margin_applied` int(1) DEFAULT NULL COMMENT 'маржа 1',
  `prepayment` varchar(255) DEFAULT NULL COMMENT 'предоплата %',
  `demand` int(1) DEFAULT NULL COMMENT 'отгрузка',
  `demand_number` varchar(255) DEFAULT NULL COMMENT 'отгрузка №',
  `demand_date` varchar(255) DEFAULT NULL COMMENT 'отгрузка дата',
  `demand_doc` int(11) DEFAULT NULL COMMENT 'отгрузка документ',
  `additional_cost` varchar(255) DEFAULT NULL COMMENT 'доп. расходы',
  `last_step` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `orders` (`id`, `type`, `order_num`, `order_num_2`, `order_document`, `to_engineer`, `to_control`, `to_accountant`, `to_logist`, `to_manager`, `purchase_order`, `pcb_count`, `comment`, `description`, `customer`, `repeated`, `date`, `delivery_price`, `logistic_price`, `profit_margin`, `profit_margin_applied`, `prepayment`, `demand`, `demand_number`, `demand_date`, `demand_doc`, `additional_cost`, `last_step`) VALUES
(71,	'2',	100,	100,	0,	1,	1,	0,	0,	0,	1,	0,	'',	NULL,	'POSCENTRE',	NULL,	'2021-06-02',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'14'),
(72,	'1',	101,	101,	0,	1,	1,	0,	0,	0,	0,	0,	'',	'N738 Contact Sensor BLE',	'Unknown',	NULL,	'2021-06-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(73,	'1',	102,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	'my simple order',	'client 123aaa',	NULL,	'2021-06-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'1000',	'14'),
(74,	'1',	103,	103,	103,	1,	1,	0,	0,	1,	1,	0,	'dfgdf gdfgdfg',	'faf vasdf sdfvsd fsvdfv f',	'Client',	NULL,	'2021-06-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'14'),
(76,	'1',	104,	104,	0,	1,	1,	0,	1,	1,	0,	0,	'1241 2412',	NULL,	NULL,	NULL,	'2021-06-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'14'),
(77,	'2',	105,	0,	104,	0,	0,	0,	0,	0,	0,	10000009,	NULL,	'   descr 5555',	'client 456',	NULL,	'2021-06-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	NULL),
(78,	'1',	106,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	NULL,	NULL,	NULL,	'2021-06-08',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	NULL),
(79,	'1',	107,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	NULL,	NULL,	NULL,	'2021-06-08',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	'14'),
(80,	'1',	108,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	NULL,	NULL,	NULL,	'2021-06-09',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	NULL),
(81,	'1',	109,	105,	0,	1,	1,	0,	1,	1,	1,	0,	'dddddddddd',	'trtrtr trtr tr',	'smit',	NULL,	'2021-06-09',	NULL,	'1222',	'22',	0,	'dddd',	NULL,	NULL,	NULL,	NULL,	'',	'13'),
(82,	'1',	110,	106,	0,	1,	1,	0,	1,	1,	0,	0,	NULL,	NULL,	NULL,	NULL,	'2021-06-09',	NULL,	'hjhh',	NULL,	NULL,	'345',	NULL,	NULL,	NULL,	NULL,	'',	'8'),
(83,	'2',	111,	107,	0,	1,	1,	0,	0,	0,	0,	0,	NULL,	'N44.24',	'Shtrih',	NULL,	'2021-06-10',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	'14'),
(84,	'2',	112,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	'add order 20k',	'shtrih',	NULL,	'2021-06-10',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'',	NULL);

DROP TABLE IF EXISTS `paymentin`;
CREATE TABLE `paymentin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `sum` varchar(255) NOT NULL COMMENT 'поступившие оплаты',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `paymentin` (`id`, `order_id`, `date`, `sum`) VALUES
(30,	66,	'14.05.2021',	'30000'),
(31,	66,	'22.05.2021',	'29600'),
(32,	66,	'18.06.2021',	'6000000'),
(33,	67,	'17.06.2021',	'10000'),
(34,	71,	'18.06.2021',	'10000'),
(35,	71,	'18.06.2021',	'20000'),
(36,	81,	'18.06.2021',	'fff'),
(37,	74,	'25.06.2021',	'123000'),
(38,	82,	'27.06.2021',	'hhjhj'),
(39,	82,	'09.06.2021',	'jh'),
(40,	83,	'24.06.2021',	'123456');

DROP TABLE IF EXISTS `providers`;
CREATE TABLE `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `specialization` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `providers` (`id`, `name`, `specialization`) VALUES
(35,	'Suplier - 1',	0),
(36,	'Suplier - 2 222',	0),
(37,	'Provider',	0),
(38,	'provider1',	0),
(39,	'provider1',	0),
(40,	'new provider',	0),
(41,	'fdgdfgdf',	0);

DROP TABLE IF EXISTS `provider_offers`;
CREATE TABLE `provider_offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `delivery_periods` int(11) NOT NULL COMMENT 'Дата поставки',
  `weight` varchar(255) NOT NULL,
  `volume` varchar(255) NOT NULL,
  `selected` int(1) NOT NULL,
  `invoicein` varchar(255) NOT NULL COMMENT 'счета поставщиков',
  `invoicein_sended` int(1) NOT NULL COMMENT 'отправлено на оплату',
  `invoicein_doc` int(11) NOT NULL COMMENT 'документ',
  `invoicein_doc_date` varchar(255) NOT NULL COMMENT 'дата документа',
  `paymentout` varchar(255) NOT NULL COMMENT 'оплата счетов (расход)',
  `paymentout_date` varchar(255) NOT NULL,
  `shipment_date` varchar(255) NOT NULL COMMENT 'дата отправки',
  `shipment_notify` varchar(255) NOT NULL,
  `supply_number` varchar(255) NOT NULL COMMENT 'номер прихода',
  `supply_date` varchar(255) NOT NULL COMMENT 'дата прихода',
  `storage` int(11) NOT NULL COMMENT 'склад',
  `supply_document` int(11) NOT NULL COMMENT 'документ прихода',
  `send_to_qc` int(1) NOT NULL,
  `brak` varchar(255) NOT NULL,
  `brak_comment` varchar(255) NOT NULL,
  `brak_doc` varchar(255) NOT NULL,
  `not_brak` varchar(255) NOT NULL,
  `not_brak_comment` varchar(255) NOT NULL,
  `not_brak_doc` varchar(255) NOT NULL,
  `send_to_control` int(1) NOT NULL,
  `proof_foto` int(11) NOT NULL COMMENT 'подтверждающее фото',
  `profit_margin` varchar(255) NOT NULL COMMENT 'маржа',
  PRIMARY KEY (`id`),
  KEY `component` (`component`),
  CONSTRAINT `provider_offers_ibfk_2` FOREIGN KEY (`component`) REFERENCES `components` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `provider_offers` (`id`, `component`, `provider`, `quantity`, `price`, `currency`, `delivery_periods`, `weight`, `volume`, `selected`, `invoicein`, `invoicein_sended`, `invoicein_doc`, `invoicein_doc_date`, `paymentout`, `paymentout_date`, `shipment_date`, `shipment_notify`, `supply_number`, `supply_date`, `storage`, `supply_document`, `send_to_qc`, `brak`, `brak_comment`, `brak_doc`, `not_brak`, `not_brak_comment`, `not_brak_doc`, `send_to_control`, `proof_foto`, `profit_margin`) VALUES
(79,	86,	36,	'50',	'50.00',	'$',	3,	'',	'',	1,	'1000',	1,	0,	'',	'10000',	'24.06.2021',	'01.07.2021',	'',	'123123',	'05.07.2021',	10,	112,	1,	'',	'',	'',	'',	'',	'',	1,	0,	'');

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `order_page` int(11) DEFAULT NULL,
  `add_components` int(11) DEFAULT NULL,
  `provider_offers` int(11) DEFAULT NULL,
  `offer_choose` int(11) DEFAULT NULL,
  `logistics` int(11) DEFAULT NULL,
  `account_formation` int(11) DEFAULT NULL,
  `receipt_of_payment` int(11) DEFAULT NULL,
  `purchase_order` int(11) DEFAULT NULL,
  `provider_invoices` int(11) DEFAULT NULL,
  `accountant_payment` int(11) DEFAULT NULL,
  `supply_request` int(11) DEFAULT NULL,
  `product_comings` int(11) DEFAULT NULL,
  `qc` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`id`, `name`, `code`, `order_page`, `add_components`, `provider_offers`, `offer_choose`, `logistics`, `account_formation`, `receipt_of_payment`, `purchase_order`, `provider_invoices`, `accountant_payment`, `supply_request`, `product_comings`, `qc`, `total`) VALUES
(1,	'Менеджер',	'manager',	1,	0,	0,	1,	0,	1,	0,	1,	0,	0,	0,	0,	0,	0),
(2,	'Инженер',	'engineer',	1,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0),
(3,	'Контрол',	'control',	0,	0,	1,	0,	0,	0,	0,	0,	1,	0,	1,	0,	1,	0),
(4,	'Логист',	'logistician',	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0),
(5,	'Бухгалтер',	'accountant',	0,	0,	0,	0,	0,	0,	1,	0,	0,	1,	0,	0,	0,	0),
(6,	'Контроль качества',	'qc',	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0),
(7,	'Админ',	'admin',	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1);

DROP TABLE IF EXISTS `storage`;
CREATE TABLE `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `storage` (`id`, `name`) VALUES
(9,	'Storage 1'),
(10,	'storage 2'),
(11,	'storage 3');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `users` (`id`, `fio`, `role`, `login`, `password`) VALUES
(1,	'айнур',	7,	'ainur',	'afe1aa575afd7cfbf8144d609c963f0a'),
(2,	'Менеджер',	1,	'manager',	'1d0258c2440a8d19e716292b231e3190'),
(3,	'Админ',	7,	'admin',	'21232f297a57a5a743894a0e4a801fc3'),
(5,	'ilya',	7,	'ilya',	'67e21d45f7b1fab0a3c43dd4339ee8e9'),
(6,	'Инженер',	2,	'engineer',	'c0760b36241e60897265620597031ec6'),
(7,	'контрол',	3,	'control',	'fc5364bf9dbfa34954526becad136d4b'),
(8,	'логист',	4,	'log',	'dc1d71bbb5c4d2a5e936db79ef10c19f');

-- 2021-06-16 04:43:43
