<?


function copy_dir_recursiv($src, $dest){	
	$files = scandir($src);
	// print_r($files);
	unset($files[0]);
	unset($files[1]);

	foreach ($files as $key => $value) {
		if(is_file($src.$value)){
			if(copy($src. $value, $dest.$value))
				echo "copied!";
			else
				echo "not copied!";
		}elseif(is_dir($src.$value)){
			if(is_dir($dest.$value)){
				copy_dir_recursiv($src.$value.'/', $dest.$value.'/');
			}else{
				if(mkdir($dest.$value, 0775, true)){
					echo "dir created!";
					copy_dir_recursiv($src.$value.'/', $dest.$value.'/');
				}
				else{
					echo "dir not!";
				}
			}
		}
	}
}

$src = __DIR__.'/assets/js/';
$dest = __DIR__.'/public/js/';
copy_dir_recursiv($src, $dest);

$src = __DIR__.'/assets/css/';
$dest = __DIR__.'/public/css/';
copy_dir_recursiv($src, $dest);

