#!/bin/bash

name=$1
controller='app/controllers/'${name^}'.php'
model='app/models/'${name^}'_model.php'
view='app/views/'$name'_view.php'

		if ! echo "<?php defined('DIRECT') OR exit('No direct script access allowed');

class ${name^} extends Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		\$this->asset->set_title('${name^} - Компонент');
		\$this->load->model('${name^}_model', '$name', '');
		\$data1 = [];
		\$data1['$name'] = \$this->$name->get${name^}();
		\$this->load->view('${name}_view', \$data1);
	}
}
			" > $controller
		then
			echo -e $"There is an ERROR create $controller file"
			exit;
		else
			echo -e $"\n controller Created\n"
		fi

## ################################################################ MODEL

		if ! echo "<?php defined('DIRECT') OR exit('No direct script access allowed');

class ${name^}_model extends Model
{
	public function get${name^}()
	{
		// return \$this->db->get('$name')->results();
	}
}
			" > $model
		then
			echo -e $"There is an ERROR create $model file"
			exit;
		else
			echo -e $"\n model Created\n"
		fi


# ################################################# VIEW

		if ! echo "<?php \$this->load->view('layouts/header_view'); ?>

${name^}

<?php \$this->load->view('layouts/footer_view'); ?>
			" > $view
		then
			echo -e $"There is an ERROR create $view file"
			exit;
		else
			echo -e $"\n view Created\n"
		fi


# ############################################ # ROUTE
# delete last line , array brace
sed -i '$ d' app/config/routes.php
echo 	"    '${name}'           => '${name}/index'," >> app/config/routes.php  
echo 	"];" >> app/config/routes.php  

